function subplotter(t,data,pin)
%SUBPLOTTER Summary of this function goes here
%   Detailed explanation goes here
    for i = 1:size(data,1)
        ax = subplot(size(data,1),1,i);
        hold on;
        plot(t,data(i,:));
        if isfield(pin,'xlims')
            if iscell(pin.xlims)
                xlim(pin.xlims{i});
            elseif min(size(pin.xlims)) == 1
                xlim(pin.xlims);
            else
                xlim(pin.xlims(i,:));
            end
        end
        if isfield(pin,'legends')
            legend(pin.legends);
        end 
    end
end

