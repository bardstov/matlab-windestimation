% close all
load_exp_measurements
Nantennas = 1;

do_save = 1;
fprintf('Running STATE estimation with data set number %s and gpstimeshift %.f\n',XXX,gpstimeshift(idt) );
DEG2RAD = pi/180;
RAD2DEG = 1/DEG2RAD;

% t_start = px.acc.t(1);
t_inds = find(adp.data(3,:) > 10);
t_start = adp.t(t_inds(1));
t_end = adp.t(t_inds(end));

time_vector = sort(unique([ars.t,acc.t]));
ind = find(time_vector > t_start); ind = ind(1);
time_vector = time_vector(ind:end);
t_end = min([time_vector(end),t_end]);
ind = find(time_vector <= t_end); ind = ind(end);
time_vector = time_vector(1:ind);
N = length(time_vector);
t = time_vector;
%% Filters
std_ars     = 5e-3;
std_acc     = 5e-2;
std_mag     = 1e-3;
std_gps     = 5e-2;
std_gpsvel  = 1e-1;
std_pitot   = .5;
std_dp      = 5;

pxind = find(px.ea.t > t_start); pxind = pxind(1);
gpsind = find(gps.pos.t > t_start); gpsind = gpsind(1);
% MEKF
ang                     = px.ea.data(:,pxind);
p0                      = gps.pos.data(:,1);
v0                      = px.vel.data(:,pxind);
b_acc0                  = [0;0;0];
q0                      = euler2q(ang(1),ang(2),ang(3));
b_ars0                  = [0,0,0]';
x0                      = [p0;v0;b_acc0;q0;b_ars0];

%% --- Filters ---
I3 = eye(3);
%R matrices
R_gps    = I3*std_gps^2;
R_gpsvel = I3*std_gpsvel^2;
R_mag    = I3*std_mag^2;
%Q matrices
Q_acc   = I3*std_acc^2;
Q_ars   = I3*std_ars^2;
% if Nantennas == 1
%     Q_bacc  = I3*1e-6;
%     Q_bars  = I3*1e-8;
% elseif Nantennas == 2
%     Q_bacc  = I3*1e-6;
%     Q_bars  = I3*1e-8;
% elseif Nantennas == 3
%     Q_bacc  = I3*1e-8;
%     Q_bars  = I3*1e-10;
% end
Q_bacc  = blkdiag(5e-7,5e-7,1e-7);
Q_bars  = I3*1e-9;
Q_bmag  = I3*1e-11;
Q       = blkdiag(Q_acc,Q_bacc,Q_ars,Q_bars,Q_bmag);
%P0 matrices
P0_p    = I3*20;
P0_v    = I3*2;
P0_ub   = I3*5e-1;
% if Nantennas == 1
%     P0_bars = I3*1e-4;
%     P0_bacc = I3*1e-4;
% elseif Nantennas == 2
%     P0_bars = I3*1e-3;
%     P0_bacc = I3*1e-3;
% elseif Nantennas == 3
%     P0_bars = I3*1e-7;
%     P0_bacc = I3*1e-6;
% end
P0_bars = I3*1e-9;
P0_bacc = I3*1e-9;
P0_bmag = I3*13-9;
P0      = blkdiag(P0_p,P0_v,P0_bacc,P0_ub,P0_bars,P0_bmag);
%create mekf
mekfinput.P0                    = P0;
mekfinput.x0                    = x0;
mekfinput.Q                     = Q;
mekfinput.R_gps                 = R_gps;
mekfinput.R_gpsvel              = R_gpsvel;
mekfinput.R_mag                 = R_mag;
mekfinput.Nantennas             = Nantennas;
mekfinput.max_P_update_interval = 1;
mekfinput.p_arm1                = p_arm1;
mekfinput.p_arm2                = p_arm2;
mekfinput.p_arm3                = p_arm3;
mekfinput.mag_n                 = mag_n;
mekf                            = class_mekf(mekfinput);

%%
es.mekf.data = zeros(19,N);
s='1';
tic
last_percent = 0;
mh = MeasurementHandler({acc,ars,gps1,gps2,gps3,pitot,dp,gpsvel1,gpsvel2,mag,adp},t_start);
for i = 1:N
    if i/(N/100) > last_percent
        s = num2str(last_percent);
        last_percent = last_percent + 1;
        fprintf('Done with %s%s: Time elapsed %.2f%s',s,'%',toc,char(10));
%         if i > 1
%             es.mekf_rvel.data(:,i-1)'
%         end
    end
    t_i = t(i);
    measurements = mh.get_measurements(t_i);
    %Update filter
    es.mekf.data(:,i) = mekf.update(t_i,measurements);
end

%% Store in save struct (ss)
ss.t = t;
ss.pos  = es.mekf.data(1:3,:);
ss.vel  = es.mekf.data(4:6,:);
ss.bacc = es.mekf.data(7:9,:);
ss.q    = es.mekf.data(10:13,:);
ss.ea   = zeros(3,N);
ss.bars = es.mekf.data(14:16,:);
ss.bmag = es.mekf.data(17:19,:);
for i = 1:N
    Rtemp = Rquat(ss.q(:,i));
    [a1,a2,a3] = R2euler(Rtemp);
    ss.ea(:,i) = [a1;a2;a3];
end
ss.mekf = mekf;

if do_save
    save(sprintf('../data/mekf_%s_%.2f.mat',XXX,gpstimeshift(idt)),'ss');
end 