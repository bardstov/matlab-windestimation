clearvars -except eds
addpath('lib')
folder = '~/data/ecc2019/2018-11-07--13:24:12/';
file = 'eds-%i.mat';
Nantennas = 1;
if ~exist('eds')
    eds = importdata(sprintf([folder file],Nantennas));
end

RAD2DEG = 180/pi;

Nmc = length(eds);

f = fields(eds{2});
N_fields = length(f);
t = eds{2}.pos{2}.t;
N = length(t);
N_est = length(eds{2}.pos);

ss_start = 1200;
tr_end = 500;
ss_inds = find(t >= ss_start);
tr_inds = find(t <= tr_end);
%% --- Create MAE ---
% for i_f = 1:N_fields
%     fname = f{i_f};
%     N_dim = size(eds{2}.(fname){2}.data,1);
%     mae.(fname) = cell(1,N_est-1);
%     for i_est = 2:N_est
%         mae.(fname){i_est-1}.t = t;
%         if ~isstruct(eds{2}.(fname){i_est})
%             mae.(fname){i_est-1}.data = 0;
%             continue
%         end
%         mae.(fname){i_est-1}.data = zeros(N_dim,N);
%         for i_mc = 1:Nmc
%             ed = eds{i_mc};
%             mae.(fname){i_est-1}.data = mae.(fname){i_est-1}.data + abs(ed.(fname){i_est}.data)/Nmc;
%         end
%     end
% end

%% --- Produce MAE point for each i_mc ---
for i_f = 1:N_fields
    fname = f{i_f};
    N_dim = size(eds{2}.(fname){2}.data,1);
    maes_ss.(fname) = cell(1,N_est-1);
    maes_tr.(fname) = cell(1,N_est-1);
    for i_est = 2:N_est
        if ~isstruct(eds{2}.(fname){i_est})
            mae.(fname){i_est-1}.data = 0;
            continue
        end
        maes_ss.(fname){i_est-1} = zeros(N_dim,Nmc);
        maes_tr.(fname){i_est-1} = zeros(N_dim,Nmc);
        for i_mc = 1:Nmc
            ed = eds{i_mc};
            maes_ss.(fname){i_est-1}(:,i_mc) = sum(abs(ed.(fname){i_est}.data(:,ss_inds))/Nmc,2);
            maes_tr.(fname){i_est-1}(:,i_mc) = sum(abs(ed.(fname){i_est}.data(:,tr_inds))/Nmc,2);
        end
    end
end


%% --- Plot MAE data points
% figure;
% for i = 1:3
%     subplot(3,1,i)
%     hold on
%     plot(maes_ss.ea{1}(i,:),'g*')
%     plot(maes_ss.ea{2}(i,:),'b*')
%     plot(maes_ss.ea{3}(i,:),'k*')
% end

%% --- Counter number of failed convergences
if Nantennas == 1
    L = [];
    fail_inds = cell(3,1);
    for i_est = 1:3
        fail_inds{i_est} = [];
        for i_dim = 1:3
            fail_inds{i_est} = [fail_inds{i_est},find(maes_ss.ea{i_est}(i_dim,:) > 100)];
        end
        fail_inds{i_est} = sort(unique(fail_inds{i_est}));
        L = [L,length(fail_inds{i_est})];
    end
    L
end


%% --- Plot MAE trajectories --- 
if Nantennas == 1
    fail_inds_all = unique(sort([fail_inds{1},fail_inds{2},fail_inds{3}]));
else
    fail_inds_all = [];
end
clearvars mae
for i_f = 1:N_fields
    fname = f{i_f};
    N_dim = size(eds{2}.(fname){2}.data,1);
    mae.(fname) = cell(1,N_est);
    mae.(fname){1} = 0;
    for i_est = 2:N_est
        if ~isstruct(eds{2}.(fname){i_est})
            mae.(fname){i_est} = 0;
            continue
        end
        mae.(fname){i_est}.t = t;
        mae.(fname){i_est}.data = zeros(N_dim,N);
        for i_mc = 1:Nmc
            if find(fail_inds_all == i_mc)
                continue
            end
            ed = eds{i_mc};
            mae.(fname){i_est}.data = mae.(fname){i_est}.data + abs(ed.(fname){i_est}.data)/(Nmc-length(fail_inds_all));
        end
    end
    disp(sprintf('Done with: %s',fname));
end
plotter(mae,make_plotinput(Nantennas))

%% --- Produce Tables --- 
N_ss = length(ss_inds);
N_tr = length(tr_inds);
for i_f = 1:N_fields
    fname = f{i_f};
    mae_ss.(fname) = [];
    mae_tr.(fname) = [];
    for i_est = 2:N_est
        if ~isstruct(mae.(fname){i_est})
            N_dim = size(mae.(fname){2}.data,1);
            mae_ss.(fname) = [mae_ss.(fname),zeros(N_dim,1)];
            mae_tr.(fname) = [mae_tr.(fname),zeros(N_dim,1)];
            continue
        end
        mae_ss.(fname) = [mae_ss.(fname), sum(mae.(fname){i_est}.data(:,ss_inds),2)/N_ss];
        mae_tr.(fname) = [mae_tr.(fname), sum(mae.(fname){i_est}.data(:,tr_inds),2)/N_tr];
    end
end
mae_ss.ea = mae_ss.ea*RAD2DEG;
mae_tr.ea = mae_tr.ea*RAD2DEG;
mae_ss.ea_ba = mae_ss.ea_ba*RAD2DEG;
mae_tr.ea_ba = mae_tr.ea_ba*RAD2DEG;
%% --- Write to file

savefolder = '~/work/postdoc/latex/papers/ecc2019/';
% file = sprintf([savefolder, 'tabels/ss-%i.txt'],Nantennas);
% fid = fopen(file,'w');
% for i_f = 1:N_fields
%     fname = f{i_f};
%     fprintf(fid,sprintf([fname,'%c'],char(10))); 
%     for i = 1:size(mae_ss.(fname),1)
%         fprintf(fid,' %.6f & %.6f & %.6f',mae_ss.(fname)(i,:));
%         fprintf(fid,sprintf('%c',char(10)));
%     end
%     fprintf(fid,sprintf('%c',char(10)));
% end
% fclose(fid);
% 
% file = sprintf([savefolder, 'tabels/tr-%i.txt'],Nantennas);
% fid = fopen(file,'w');
% for i_f = 1:N_fields
%     fname = f{i_f};
%     fprintf(fid,sprintf([fname,'%c'],char(10))); 
%     for i = 1:size(mae_tr.(fname),1)
%         fprintf(fid,' %.6f & %.6f & %.6f',mae_tr.(fname)(i,:));
%         fprintf(fid,sprintf('%c',char(10)));
%     end
%     fprintf(fid,sprintf('%c',char(10)));
% end
% fclose(fid);

scale = {1000,1000,1000,1,1,1,1000,1000,1000};
w = {'%.1f','%.1f','%.1f','%.2f','%.2f','%.2f','%.1f','%.1f','%.1f'};
file = sprintf([savefolder, 'tabels/all-%i.txt'],Nantennas);
fid = fopen(file,'w');
for i_f = 1:N_fields
    fname = f{i_f};
    fprintf(fid,sprintf([fname,'%c'],char(10))); 
    str = [' ',w{i_f},' & ',w{i_f},' & ',w{i_f},' & ',w{i_f},' & ',w{i_f},' & ',w{i_f},' & '];
    for i = 1:size(mae_tr.(fname),1)
        fprintf(fid,str,mae_tr.(fname)(i,:)*scale{i_f},mae_ss.(fname)(i,:)*scale{i_f});
        fprintf(fid,sprintf('%c',char(10)));
    end
    fprintf(fid,sprintf('%c',char(10)));
end
fprintf('armnorms %s',char(10));
norm_tr = zeros(1,3);
norm_ss = zeros(1,3);
for i_ant = 1:Nantennas
    fname = f{6+i_ant};
    for i_est = 1:3
        norm_tr(i_est) = 0;
        norm_ss(i_est) = 0;
        if ~isstruct(mae.(fname){i_est+1})
            continue
        end
        for i = tr_inds
            norm_tr(i_est) = norm_tr(i_est) + norm(mae.(fname){i_est+1}.data(:,i))/N_tr;
        end
        for i = ss_inds
            norm_ss(i_est) = norm_ss(i_est) + norm(mae.(fname){i_est+1}.data(:,i))/N_ss;
        end
    end
    fprintf(fid,' %.1f & %.1f & %.1f & %.1f & %.1f & %.1f %s',norm_tr*1e3,norm_ss*1e3,char(10));
end
fclose(fid);