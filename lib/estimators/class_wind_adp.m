classdef class_wind_adp < handle
    %EKF Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        %states
        alpha;
        beta;
        V_wb;
        gamma;
        
        %air density
        rho;
        
        %Covariance matrices
        Q;
        P;
        R;
        %Arms
        Nantennas;
        p_arm1;
        p_arm2;
        p_arm3;
        
        %Sets maximal update interval for P
        max_P_update_interval;
        
        %Stores previous P update time
        P_prev_time = 0;
        
        %Stores previous time instance
        prev_time = 0;
        
        %Stores previous acc and ars measururements
        prev_acc=zeros(3,1);
        prev_ars=zeros(3,1);
        
        %Identity matrix of n x n
        I;
        
        %gravity
        g = [0;0;9.81];
        
        %time step
        time_step;
    end
    
    methods
        function kf = class_wind_adp(input)
            x0                          = input.x0;
            kf.alpha                    = x0(1);
            kf.beta                     = x0(2);
            kf.V_wb                     = x0(3); 
            kf.gamma                    = x0(4);
            kf.P                        = input.P0;
            kf.Q                        = input.Q;
            kf.I                        = eye(size(input.P0));
            kf.R                        = input.R;
            if isfield(input,'g')
                kf.g = input.g;
            end
            if isfield(input,'time_step')
                kf.time_step = input.time_step;
            end
            Rnb = Rquat(input.q0);
            kf.prev_acc = -[0;0;9.81];
            kf.rho = input.rho;
        end
%         function x = update(kf,measurements,input)
        function x = update(kf,time,measurements)
            new_pitot = ~isempty(measurements.pitot.data);
%             new_pitot = ~isempty(measurements.dp.data);
            q = measurements.quaternion.data;
            
            %find dt
            if kf.time_step > 0
                dt = kf.time_step;
            else
                dt = min([.1,time-kf.prev_time]);
                kf.prev_time=time;
            end
            %cov time update
            Rnb = Rquat(q);
            ars_hat = kf.prev_ars;
            fb_hat = kf.prev_acc;
            
            p = ars_hat(1);
            q = ars_hat(2);
            r = ars_hat(3);
            
            u = fb_hat + Rnb'*[0;0;9.81];
            u1 = u(1); u2 = u(2); u3 = u(3);
            
            a = kf.alpha;
            b = kf.beta;
            V = kf.V_wb;
            %find f,F
            f = ...
                                                      [(cos(a)*(u3 - V*p*sin(b) + V*q*cos(a)*cos(b)))/(V*cos(b)) - (sin(a)*(u1 + V*r*sin(b) - V*q*cos(b)*sin(a)))/(V*cos(b))
 (cos(b)*(u2 - V*r*cos(a)*cos(b) + V*p*cos(b)*sin(a)))/V - (sin(a)*sin(b)*(u3 - V*p*sin(b) + V*q*cos(a)*cos(b)))/V - (cos(a)*sin(b)*(u1 + V*r*sin(b) - V*q*cos(b)*sin(a)))/V
             sin(b)*(u2 - V*r*cos(a)*cos(b) + V*p*cos(b)*sin(a)) + cos(b)*sin(a)*(u3 - V*p*sin(b) + V*q*cos(a)*cos(b)) + cos(a)*cos(b)*(u1 + V*r*sin(b) - V*q*cos(b)*sin(a))];
            F = ...
[ -(u1*cos(a) + u3*sin(a) + V*r*cos(a)*sin(b) - V*p*sin(a)*sin(b))/(V*cos(b)), -(u1*sin(a)*sin(b) - u3*cos(a)*sin(b) + V*p*cos(a) + V*r*sin(a))/(V*cos(b)^2),                 -(u3*cos(a) - u1*sin(a))/(V^2*cos(b));
            (u1*sin(a)*sin(b) - u3*cos(a)*sin(b) + V*p*cos(a) + V*r*sin(a))/V,                          -(u2*sin(b) + u1*cos(a)*cos(b) + u3*cos(b)*sin(a))/V, (u1*cos(a)*sin(b) - u2*cos(b) + u3*sin(a)*sin(b))/V^2;
                                               cos(b)*(u3*cos(a) - u1*sin(a)),                               u2*cos(b) - u1*cos(a)*sin(b) - u3*sin(a)*sin(b),                                                     0];
            F = blkdiag(F,0);
            %find G
            G = [ -sin(a)/(V*cos(b)),        0,  cos(a)/(V*cos(b));
                  -(cos(a)*sin(b))/V, cos(b)/V, -(sin(a)*sin(b))/V;
                       cos(a)*cos(b),   sin(b),      cos(b)*sin(a)];
            G = blkdiag(G,1);
%             size(F)
%             size(G)
%             size(kf.Q)
            [Phi,Qd] = van_loan(F,G*kf.Q*G',dt);
            kf.P = Phi*kf.P*Phi'+Qd;
            kf.P = .5*(kf.P+kf.P');
            
            %state time update
            kf.alpha = kf.alpha + dt*f(1);
            kf.beta  = kf.beta  + dt*f(2);
            kf.V_wb  = kf.V_wb  + dt*f(3);
            
            %measurement update
            if ~isempty(measurements.acc.data)
                kf.prev_acc = measurements.acc.data-measurements.b_acc_hat.data;
            end
            if ~isempty(measurements.ars.data)
                kf.prev_ars = measurements.ars.data-measurements.b_ars_hat.data;
            end
            if new_pitot
%                 y = measurements.pitot.data;
%                 y_hat = kf.gamma*V*cos(a)*cos(b);
%                 H = [ -kf.gamma*V*cos(b)*sin(a), -kf.gamma*V*cos(a)*sin(b), kf.gamma*cos(a)*cos(b),V*cos(a)*cos(b)];
%                 y_hat = kf.gamma*V;
%                 H = [0,0,kf.gamma,V];
                y = measurements.dp.data;
                y_hat = .5*kf.rho*V^2*kf.gamma;
                H = .5*kf.rho*[0,0,2*V*kf.gamma,V^2];
                
                % --- KF update ---
%                 size(kf.P)
%                 size(H)
%                 size(R)
                K = kf.P*H'/(H*kf.P*H'+kf.R);
                kf.P = (kf.I-K*H)*kf.P*(kf.I-K*H)'+K*kf.R*K';
                kf.P = (kf.P+kf.P')/2;
                ytilde = y-y_hat;
                xtilde = K*ytilde;

%                 kf.alpha = kf.alpha + xtilde(1);
%                 kf.beta  = kf.beta  + xtilde(2);
                kf.V_wb  = kf.V_wb  + xtilde(3);
                kf.gamma = kf.gamma + xtilde(4);
            end
            x = [kf.alpha;kf.beta;kf.V_wb;kf.gamma];
        end
    end
end

