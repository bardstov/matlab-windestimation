addlib
%%
mistuning = 40;
if ~exist('loaded')
%     pd = importdata('~/data/scitech/2019-07-25--12:54:26/pds.mat');
%     pd = importdata('~/data/scitech/2019-11-21--10:05:15/pds.mat');
    if mistuning == 1
        pd = importdata('~/data/scitech/2019-11-28--12:13:17/pds.mat');
    elseif mistuning == 10
        pd = importdata('~/data/scitech/2019-11-28--12:26:10/pds.mat');
    elseif mistuning == 40
        pd = importdata('~/data/scitech/2019-11-28--12:41:43/pds.mat');
    else
        error('invalid mistuning value')
    end
    loaded=1;
end

N_mc = length(pd);
N_filters = length(pd{1}.v_nw);
t = pd{1}.v_nw{2}.t;
N_samples = length(t);
%% Plot one single run
n = 1;
pd_n = pd{n};
plotinput = make_plotinput();
plotinput.colors = plotinput.colors([1,3,7,4,5,6]);
plotinput.legends = {'True', 'EKF','KF[4]','3SF1','3SF2','3SF3'};
plotinput.ylabels = {{'N [$m/s$]','E [$m/s$]','D [$m/s$]'},{'Correction factor'},{'AOA [deg]'},{'SSA [deg]'},{'$\|V_wb\|_2$'},{'X [$m/s$]','Y [$m/s$]','Z [$m/s$]'}};
plotinput.labels  = {'Wind Velocity','Gamma','AOA','SSA','Airspeed','Relative Velocity'};
plotinput.figpos = [0,0,1000,600];
plotinput.do_save = 0;
plotinput.savefolder = sprintf('~/work/postdoc/latex/papers/windestimation/simplots/');
plotinput.savetype = '.eps';
% plotinput.savesuffix = '-Qlarge';
% plotinput.selected_fields = [1,1,1,0,0,0];
plotinput.selected_estimators = [1,1,1,0,1,1,1,1,1];
plotter(pd_n,plotinput);

%% Calculate all errors
ed.v_nw = cell(1,6);
ed.gamma = cell(1,6);    
    
for i = 2:N_filters
    ed.v_nw{i}.data = zeros(3,N_samples);
    ed.gamma{i}.data = zeros(1,N_samples);
    ed.v_nw{i}.t = t;
    ed.gamma{i}.t = t;
    for j = 1:N_mc
        error_vnw = pd{j}.v_nw{i}.data - pd{j}.v_nw{1}.data(:,1);
        ed.v_nw{i}.data = ed.v_nw{i}.data + abs(error_vnw)/N_mc;
        
        error_gamma = pd{j}.gamma{i}.data - pd{j}.gamma{1}.data(1);
        ed.gamma{i}.data = ed.gamma{i}.data + abs(error_gamma)/N_mc;
    end
end

%% MAE tables
T_divide = 300;
inds_tr = find(t < T_divide);
inds_ss = find(t >= T_divide);
mae_tr = zeros(4,N_filters-1);
mae_ss = zeros(4,N_filters-1);
for i = 2:N_filters
    mae_tr(1:3,i-1) = sum(ed.v_nw{i}.data(:,inds_tr),2)/length(inds_tr);
    mae_tr(4,i-1) = sum(ed.gamma{i}.data(inds_tr))/length(inds_tr);
    mae_ss(1:3,i-1) = sum(ed.v_nw{i}.data(:,inds_ss),2)/length(inds_ss);
    mae_ss(4,i-1) = sum(ed.gamma{i}.data(inds_ss))/length(inds_ss);
end

%% Calculate AEs
ed1 = ed;
ed2 = ed;
for i = 2:N_filters
    ed1.v_nw{i}.t = ed1.v_nw{i}.t(inds_tr);
    ed1.v_nw{i}.data = ed1.v_nw{i}.data(:,inds_tr);
    
    ed2.v_nw{i}.t = ed2.v_nw{i}.t(inds_ss);
    ed2.v_nw{i}.data = ed2.v_nw{i}.data(:,inds_ss);
    
    ed1.gamma{i}.t = ed1.gamma{i}.t(inds_tr);
    ed1.gamma{i}.data = ed1.gamma{i}.data(inds_tr);
    
    ed2.gamma{i}.t = ed2.gamma{i}.t(inds_ss);
    ed2.gamma{i}.data = ed2.gamma{i}.data(inds_ss);
end

%% Plot AE whole both
clearvars gwh;
gwh.gamma = ed.gamma;
plotinput_gwh = plotinput;
plotinput_gwh.do_save = 1;
plotinput_gwh.savesuffix = sprintf('-ae-whole-%i',mistuning);
plotinput_gwh.ylabels = {{'Correction factor'}};
plotinput_gwh.labels  = {'Gamma'};
plotinput_gwh.figpos = [0,0,700,300];
plotter(gwh,plotinput_gwh);

clearvars vwh;
vwh.v_nw = ed.v_nw;

plotinput_vwh = plotinput_gwh;
plotinput_vwh.figpos = [0,0,700,600];
plotinput_vwh.ylabels = {{'N [$m/s$]','E [$m/s$]','D [$m/s$]'}};
plotinput_vwh.labels  = {'Wind Velocity'};
plotinput_vwh.where_legends = {[0,1,0]};
plotter(vwh,plotinput_vwh);

if mistuning == 1
    %% Plot AE gamma
    clearvars gtr
    gtr.gamma = ed1.gamma;

    plotinput_gtr = plotinput;
    plotinput_gtr.do_save = 0;
    plotinput_gtr.savesuffix = '-ae-transient';
    plotinput_gtr.ylabels = {{'Correction factor'}};
    plotinput_gtr.labels  = {'Gamma'};
    plotinput_gtr.figpos = [0,0,700,300];
    plotter(gtr,plotinput_gtr);

    %% Plot AE v_nw
    clearvars vtr
    vtr.v_nw = ed1.v_nw;

    plotinput_vtr = plotinput_gtr;
    plotinput_vtr.figpos = [0,0,700,600];
    plotinput_vtr.ylabels = {{'N [$m/s$]','E [$m/s$]','D [$m/s$]'}};
    plotinput_vtr.labels  = {'Wind Velocity'};
    plotinput_vtr.where_legends = {[0,1,0]};
    plotter(vtr,plotinput_vtr);
end