syms a b V p q r u1 u2 u3 'real'

u = [u1;u2;u3];
v_wb = [cos(a)*cos(b);
        sin(b);
        sin(a)*cos(b)]*V;
    
dv_a = diff(v_wb,a);
dv_b = diff(v_wb,b);
dv_V = diff(v_wb,V);

dv = [dv_a,dv_b,dv_V];

T = simplify(inv(dv));

S = Smtrx([p,q,r]);

f = T*(-S*v_wb + u);

F = simplify([diff(f,a),diff(f,b),diff(f,V)]);

h = cos(a)*cos(b)*V;
H = [diff(h,a),diff(h,b),diff(h,V)];


F = ...
[ -(u1*cos(a) + u3*sin(a) + V*r*cos(a)*sin(b) - V*p*sin(a)*sin(b))/(V*cos(b)), -(u1*sin(a)*sin(b) - u3*cos(a)*sin(b) + V*p*cos(a) + V*r*sin(a))/(V*cos(b)^2),                 -(u3*cos(a) - u1*sin(a))/(V^2*cos(b));
            (u1*sin(a)*sin(b) - u3*cos(a)*sin(b) + V*p*cos(a) + V*r*sin(a))/V,                          -(u2*sin(b) + u1*cos(a)*cos(b) + u3*cos(b)*sin(a))/V, (u1*cos(a)*sin(b) - u2*cos(b) + u3*sin(a)*sin(b))/V^2;
                                               cos(b)*(u3*cos(a) - u1*sin(a)),                               u2*cos(b) - u1*cos(a)*sin(b) - u3*sin(a)*sin(b),                                                     0];