function [Phi,Gamma] = van_loan(A,Q,h)
    n = size(A,1);
    H = [A, Q;
         zeros(n),-A'];
    dH = eye(2*n) + h*H + h^2*H^2/2 + h^3*H^3/6 + h^4*H^4/24 + h^5*H^5/120;
    Phi = dH(1:n,1:n);
    Gamma = dH(1:n,n+1:2*n)*Phi';
end
