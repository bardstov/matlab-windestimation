function v2 = interpolateNd(t1,v1,t2)
%INTERPOLATE3D Summary of this function goes here
%   Detailed explanation goes here
    v2 = zeros(size(v1,1),length(t2));
    for i = 1:size(v1,1)
        v2(i,:) = interp1(t1,v1(i,:),t2);
    end
end

