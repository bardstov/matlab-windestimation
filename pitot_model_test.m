
close all

a = 1;
m = 1.5;

inds = 2000:20000;
airspeed = rpx.ARSP_Airspeed;
rho = 1.225;
dp = rpx.ARSP_DiffPress;



fun = @(x)(dp(inds)*2/rho-airspeed(inds).*airspeed(inds) - x(1)*airspeed(inds) - x(2));

x = lsqnonlin(fun,[0,1])

figure
plot(airspeed(inds),'r')
hold on
plot(sqrt(dp(inds)*2/rho-x(1)*airspeed(inds)-x(2)),'b')


%%

figure
hold on
plot(rpx.ARSP_DiffPress,'r')
plot(rpx.ARSP_RawPress,'b--')
