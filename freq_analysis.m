imu = adis;
% imu.ars.data(2,:) = sin(2*pi*imu.ars.t*20);
Fs = 1/mean(diff(imu.ars.t));
imu.ars.data = lowpass(imu.ars.data',1e-9,Fs)';
N = length(imu.ars.t);
data = imu.ars.data(2,:);
NFFT = 2^nextpow2(10*N);
Y = fft(data-mean(data),NFFT)/N;
amp = 2*abs(Y(1:NFFT/2+1));
f = Fs/2*linspace(0,1,NFFT/2+1);
figure;
plot(f,amp);

plot(imu.ars.t,imu.ars.data(2,:))
