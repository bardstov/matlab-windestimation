classdef class_wind_ekf < handle
    %EKF Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        %states
        v_nw;
        gamma;
        
        %air density
        rho;
        
        %Covariance matrices
        Q;
        P;
        R;
        
        %Identity matrix of appropriate size
        I;
        
        %in order to calculate time step
        prev_time;
        
        %enables delayed starting
        start_time = 0;
        
        O = zeros(4,4);
        O_counter = 0;
    end
    
    methods
        function kf = class_wind_ekf(input)
            x0                          = input.x0;
            kf.v_nw                     = x0(1:3); 
            kf.gamma                    = x0(4);
            kf.rho                      = input.rho;
            kf.P                        = input.P0;
            kf.Q                        = input.Q;
            kf.I                        = eye(size(input.P0));
            kf.R                        = input.R;
            if isfield(input,'start_time')
                kf.start_time = input.start_time;
            end
        end
        function x = update(kf,time,measurements)
%             v_abs   = measurements.pitot.data;
            v_nb    = measurements.velocity.data;
            dp      = measurements.dp.data;
            q       = measurements.quaternion.data; 
            P_ret = diag(kf.P);
            if isempty(dp)
                x = [kf.v_nw;kf.gamma;eig(kf.O/max([1,kf.O_counter]));P_ret];
                return
            end
            
            if kf.start_time ~= -Inf && time < kf.start_time
                x = [kf.v_nw;kf.gamma];
                return
            else
                kf.start_time = -Inf;
            end
            
            %find dt
            dt = min([.1,time-kf.prev_time]);
            kf.prev_time=time;
            
            %cov time update
            [Phi,Qd] = van_loan(zeros(4),kf.Q,dt);
%             Phi
%             Gamma
            kf.P = Phi*kf.P*Phi' + Qd;
            kf.P = .5*(kf.P+kf.P');
            
            %no state update
            
            % --- KF update ---
            %<<< OLD
%             y = v_abs;
%             relvel = (v_nb-kf.v_nw);
%             relvel_n = norm(relvel);
%             y_hat = kf.gamma*relvel_n;
%             H = [-kf.gamma*(relvel)'/relvel_n, relvel_n];
            % OLD >>>
            %<<< NEW
            y = dp;
            relvel = v_nb-kf.v_nw;
            y_hat = .5*kf.rho*kf.gamma*(relvel'*relvel);
            H = kf.rho*[-kf.gamma*(relvel)', .5*(relvel'*relvel)];
            % NEW >>>
            K = kf.P*H'/(H*kf.P*H'+kf.R);
            kf.P = (kf.I-K*H)*kf.P*(kf.I-K*H)'+K*kf.R*K';
            kf.P = (kf.P+kf.P')/2;
            ytilde = y-y_hat;
            xtilde = K*ytilde;

            kf.v_nw = kf.v_nw + xtilde(1:3);
            kf.gamma = kf.gamma + xtilde(4);
            
            kf.O_counter  = kf.O_counter +1;
            kf.O = kf.O + H'*H;
            x = [kf.v_nw;kf.gamma;eig(kf.O/kf.O_counter);P_ret];
            kf.O = zeros(4);
            kf.O_counter = 0;
        end
    end
end

