classdef class_model_based < handle
    %EKF Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        %states
        x;
        
        %Covariance matrices
        Q;
        P;
        R_f;
        R_aoa_hf;
        R_aoa_lf;
        
        %Model matrices
        G = eye(5);%,9);
        F = zeros(5);
        
        %acceleration of gravity
        g = [0;0;9.81];
        
        %air density
        rho;
        
        %UAV parameters
        c; %chord line?
        m; %mass
        S; %wing area
        
        %Identity matrix of appropriate size
        I;
        
        %in order to calculate time step
        prev_time;
        
        %enables delayed starting
        start_time = 0;
    end
    
    methods
        function kf = class_model_based(input)
            kf.x        = input.x0;
            kf.P        = input.P0;
            kf.Q        = input.Q;
            kf.I        = eye(size(input.P0));
            kf.R_f      = input.R_f;
            kf.R_aoa_lf = input.R_aoa_lf;
            kf.c        = input.c;
            kf.S        = input.S;
            kf.m        = input.mass;
            kf.rho      = input.rho;
            
            if isfield(input,'start_time')
                kf.start_time = input.start_time;
            end
            if isfield(input,'g')
                kf.g = input.g;
            end
            
        end
        function x = update(kf,time,measurements)
            aoa_hf  = [];%measurements.aeroprobe.data;
            aoa_lf  = measurements.alpha_hat.data;
            ars     = measurements.ars.data - measurements.b_ars_hat.data;
            f       = measurements.acc.data - measurements.b_acc_hat.data; ...
            acc     = f + Rquat(measurements.quaternion.data)'*kf.g;
            ax = acc(1); ay = acc(2); az = acc(3);
            fx = f(1); fy = f(2); fz = f(3);
            p = ars(1); q = ars(2); r = ars(3);
            V       = measurements.airspeed_hat.data;
            beta    = measurements.beta_hat.data;
            e       = measurements.elevator.data;
            
            if V  < 5
                x = kf.x;
                return
            end
            alpha       = kf.x(1);
            C_L_0       = kf.x(2);
            C_L_alpha   = kf.x(3);
            C_L_q       = kf.x(4);
            C_L_e       = kf.x(5);
            if kf.start_time ~= -Inf && time < kf.start_time
                x = [kf.v_nw;kf.gamma];
                return
            else
                kf.start_time = -Inf;
            end
            
            %find dt
            dt = min([.1,time-kf.prev_time]);
            kf.prev_time=time;
            
            ca = cos(alpha);
            sa = sin(alpha);
            cb = cos(beta);
            sb = sin(beta);
            tb = sb/cb;
            
            % --- KF covariance time update --- 
            kf.F(1) = [sa*tb,0,-ca*tb]*ars - [sa, 0, ca]/(V*cb)*acc;
            
%             G_ars = [-ca*tb,1,-sa*tb];
%             G_acc = [ca,0,-sa]/(V*cb);
%             kf.G(1,1:6) = [G_ars,G_acc];
            
            [Phi,Qd] = van_loan(kf.F,kf.G*kf.Q*kf.G',dt);
            kf.P = Phi*kf.P*Phi'+Qd;
            kf.P = .5*(kf.P+kf.P');
            
            % --- KF state time update ---
            da = (az*ca-ax*sa)/(V*cb)*0 + q - p*ca*tb - r*sa*tb;
            alpha = alpha + dt*da;
            
            % --- KF covariance measurement update
%             H_alpha = -sa*(C_L_0 + C_L_alpha*alpha + C_L_q*q*kf.c/(2*V) + C_L_e*e) + ca*C_L_alpha;
%             H_CL0 = 1;
%             H_CLalpha = alpha;
%             H_CLq = q*kf.c/(2*V);
%             H_CLe = e;
            H_alpha = C_L_alpha;
            H_CL0 = 1;
            H_CLalpha = alpha;
            H_CLq = q*kf.c/(2*V);
            H_CLe = e;
            H = .5*kf.rho*V^2*kf.S*[H_alpha, H_CL0, H_CLalpha, H_CLq, H_CLe].*[1,0,1,0,0];
            R = kf.R_f;
            if ~isempty(aoa_lf)
                H = [H;1,0,0,0,0];
                R = blkdiag(R,kf.R_aoa_lf);
            end
%             H_alpha
%             H_CL0
%             H_CLalpha
%             H_CLq
%             size(kf.P)
%             size(H)
%             size(kf.R)
            K = kf.P*H'/(H*kf.P*H'+R);
            kf.P = (kf.I-K*H)*kf.P*(kf.I-K*H)'+K*R*K';
            kf.P = (kf.P+kf.P')/2;
            
            % --- KF state measurement update ---
            y = -kf.m*fz;
            y_hat = .5*kf.rho*V^2*kf.S*(C_L_0 + C_L_alpha*alpha);% + C_L_q*q*kf.c/(2*V));
%             y_hat = .5*kf.rho*V^2*kf.S*(C_L_0 + C_L_alpha*alpha + C_L_q*q*kf.c/(2*V));
            if ~isempty(aoa_hf)
                y = [y;aoa_hf(1)];
                y_hat = [y_hat;alpha];
            end
            if ~isempty(aoa_lf)
                y = [y;aoa_lf];
                y_hat = [y_hat;alpha];
            end
            ytilde = y-y_hat;
            xtilde = K*ytilde;
            kf.x = [alpha;
                    C_L_0;
                    C_L_alpha;
                    C_L_q;
                    C_L_e] + xtilde;
            x = kf.x;
        end
    end
end

