classdef Measurement < handle
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        t;
        data;
        tag = '';
        add_info;
    end
    
    methods
        function m = Measurement(input)
            if isfield(input,'t_resample')
                m.t = input.t_resample;
                for i = 1:size(input.data,1)
                    m.data(i,:) = interp1(input.t,input.data(i,:),m.t);
                end
                
                while isnan(sum(m.data(:,1)))
                    m.data = m.data(:,2:end);
                    m.t = m.t(2:end);
                end
                while isnan(sum(m.data(:,end)))
                    m.data = m.data(:,1:end-1);
                    m.t = m.t(1:end-1);
                end
            else 
                m.t = input.t;
                m.data = input.data;
            end
            if isfield(input,'tag')
                m.tag = input.tag;
            else
                error('Provide input.tag!');
            end
            if isfield(input,'add_info')
                m.add_info = input.add_info;
            end
        end
    end
    
end

