classdef class_wind_ukf < handle
    %EKF Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        %state
        x;
        
        %sigma points
        X;
        
        %air density
        rho;
        
        %tuning parameters
        alpha=1e-3;                                 %default, tunable
        ki=0;                                       %default, tunable
        beta=2;                                     %default, tunable
        
        %derived parameters
        lambda;
        Lx; %number of states
        Ls; %number of sigma points
        c;
        c2;
        Wc;
        Wm;
        
        Nsigmas;
        
        %Covariance matrices
        Q;
        P;
        R;
        
        %Stores previous time instance
        prev_time = 0;
        
        %Stores previous acc and ars measururements
        prev_acc = zeros(3,1);
        prev_ars = zeros(3,1);
        prev_ea  = zeros(3,1);
        
        %Identity matrix of n x n
        I;
        
        %gravity
        g = [0;0;9.81];
        
        %time step
        time_step;
    end
    
    methods
        function kf = class_wind_ukf(input)
            kf.x                        = input.x0;
            kf.P                        = input.P0;
            kf.Q                        = input.Q;
            kf.R                        = input.R;
            if isfield(input,'g')
                kf.g = input.g;
            end
            if isfield(input,'time_step')
                kf.time_step = input.time_step;
            end
            kf.prev_acc = -[0;0;9.81];
            kf.prev_ars = zeros(3,1);
            kf.prev_ea = zeros(3,1);
            
            if isfield(input,'alpha')
                kf.alpha                    = input.alpha;
            end
            if isfield(input,'beta')
                kf.beta                     = input.beta;
            end
            if isfield(input,'ki')
                kf.ki                       = input.ki;
            end
            kf.Lx = length(kf.x);
            kf.lambda = kf.alpha^2*(kf.Lx+kf.ki) - kf.Lx;           %scaling factor
            kf.c = kf.Lx + kf.lambda;                               %scaling factor
            kf.Wm = [kf.lambda/kf.c, 0.5/kf.c+zeros(1,2*kf.Lx)];    %weights for means
            kf.Wc = kf.Wm;
            kf.Wc(1) = kf.Wc(1) + (1-kf.alpha^2+kf.beta);           %weights for covariance
            kf.c2 = sqrt(kf.c);            
            kf.X = kf.sigmas;
            kf.Ls = size(kf.X,2);
        end
        function [xret,Pret] = update(kf,time,measurements)
            %create input struct
            inps.acc = kf.prev_acc;
            inps.omg = kf.prev_ars;
            inps.ea  = kf.prev_ea;
            inps.dt = min([0,time-kf.prev_time]);
            inps.g = kf.g;
            
            %handle new measurements
            if ~isempty(measurements.acc.data)
                kf.prev_acc = measurements.acc.data-measurements.b_acc_hat.data;
            end
            if ~isempty(measurements.ars.data)
                kf.prev_ars = measurements.ars.data-measurements.b_ars_hat.data;
            end
            if ~isempty(measurements.quaternion.data)
                [a1,a2,a3] = q2euler(measurements.quaternion.data);
                kf.prev_ea = [a1;a2;a3];
            end
            
            %time update sigma points
            for k = 1:kf.Nsigmas
                kf.X(k) = time_update(kf.X(k),inps);
            end
            
            %measurement update
            if ~isempty(measurements.aeroprobe.data)
                z = [measurements.aeroprobe.data];
                m = numel(z);                           %number of measurements
                Y = kf.X;                               %propagate sigma points through time update function (already done)
                [x1,X1,P1,X2] = kf.ut(Y,kf.Lx,kf.Q);    %unscented transformation of process
                % propagate each sigma point through measurement function.
                % here it is identity and therefore not necessary.
                [z1,~,P2,Z2] = kf.ut(X1,m,kf.R);        %unscented transformation of measurments
                P12 = X2*diag(kf.Wc)*Z2';               %transformed cross-covariance
                K = P12*pinv(P2);
                kf.x = x1+K*(z-z1);                     %state update
                kf.P = P1-K*P12';                       %covariance update
                
                kf.X = kf.sigmas();
            end
            kf.prev_time = time;
            xret = kf.x;
            Pret = kf.P;
        end
        function [x,P] = time_update(kf)
            L = numel(kf.x);                                 %numer of states
            m = numel(z);                                 %numer of measurements
            lambda = kf.alpha^2*(L + kf.ki) - L;                    %scaling factor
            c = L + lambda;                                 %scaling factor
            Wm = [lambda/c, 0.5/c+zeros(1,2*L)];           %weights for means
            Wc = Wm;
            Wc(1) = Wc(1) + (1-kf.alpha^2+kf.beta);               %weights for covariance
            c = sqrt(c);
            X = sigmas(kf.x,kf.P,c);                            %sigma points around x
            [x1, X1, P1, X2] = ut(fstate,X,Wm,Wc,L,Q);          %unscented transformation of process
        end
%         function measurement_update()
%             L = numel(x);                                 %numer of states
%             m = numel(z);                                 %numer of measurements
%             lambda = kf.alpha^2 * (L + kf.ki) - L;        %scaling factor
%             c = L + lambda;                               %scaling factor
%             Wm = [lambda/c, 0.5/c+zeros(1,2*L)];          %weights for means
%             Wc = Wm;
%             Wc(1) = Wc(1) + (1-kf.alpha^2+kf.beta);       %weights for covariance
%             c = sqrt(c);
%             X = sigmas(x,P,c);                            %sigma points around x
%             [z1, Z1, P2, Z2] = ut(hmeas,X1,Wm,Wc,m,kf.R); %unscented transformation of measurments
%             P12 = X2*diag(Wc)*Z2';                        %transformed cross-covariance
%             K = P12*pinv(P2);
%             x = x1 + K*(z-z1);                              %state update
%             P = P1 - K*P12';                                %covariance update
%         end
        function [y, Y, P, Y1] = ut(kf, Y, n, Cov) 
            %Unscented Transformation
            %Input:
            %        f: nonlinear map
            %        X: sigma points
            %       Wm: weights for mean
            %       Wc: weights for covraiance
            %        n: numer of outputs of f
            %        R: additive covariance
            %Output:
            %        y: transformed mean
            %        Y: transformed smapling points
            %        P: transformed covariance
            %       Y1: transformed deviations
            y = zeros(n,1);
            for k = 1 : kf.Ls                   
                y = y + kf.Wm(k)*Y(:,k);       
            end
            Y1 = Y - y(:,ones(1,kf.Ls));
            P = Y1*diag(kf.Wc)*Y1' + Cov;
        end
        function X = sigmas(kf)
            %Sigma points around reference point
            %Inputs:
            %Output:
            %       X: Sigma points

            A = kf.c2*chol(kf.P)';
            Y = kf.x(:,ones(1,numel(kf.x)));
            X = [kf.x Y+A Y-A]; 
        end
    end
end

