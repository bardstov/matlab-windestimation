function diff = cal_ap(x,c)
    N = size(c.y,2);
    diff = zeros(3*N,1);
    for i = 1:N
        ca = cos(c.alpha(i)+x(1));
        sa = sin(c.alpha(i)+x(1));
        cb = cos(c.beta(i)+x(2));
        sb = sin(c.beta(i)+x(2));
        vwb = c.V(i)*[ca*cb; sb; sa*cb];
        diff(3*i-2:3*i) = c.y(:,i)-vwb;
    end
end

