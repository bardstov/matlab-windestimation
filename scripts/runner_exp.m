close all
load_exp_measurements
Nantennas = 1;

do_save = 0;

addpath('lib')

DEG2RAD = pi/180;
RAD2DEG = 1/DEG2RAD;

t_start = px.acc.t(1);
t_end = 2700;

time_vector = sort(unique([ars.t,acc.t]));
ind = find(time_vector > t_start); ind = ind(1);
time_vector = time_vector(ind:end);
t_end = min([time_vector(end),t_end]);
ind = find(time_vector <= t_end); ind = ind(end);
time_vector = time_vector(1:ind);
N = length(time_vector);
t = time_vector;
%% Filters
std_ars     = 5e-3;
std_acc     = 5e-2;
std_mag     = 1e-3;
std_gps     = 5e-2;
std_gpsvel  = 1e-1;
std_pitot   = .5;
std_dp      = 5;

pxind = find(px.ea.t > t_start); pxind = pxind(1);
gpsind = find(gps.pos.t > t_start); gpsind = gpsind(1);
% MEKF
ang                     = px.ea.data(:,pxind);
p0                      = gps.pos.data(:,1);
v0                      = px.vel.data(:,pxind);
b_acc0                  = [0;0;0];
q0                      = euler2q(ang(1),ang(2),ang(3));
b_ars0                  = [0,0,0]';
x0                      = [p0;v0;b_acc0;q0;b_ars0];

%% --- Filters ---
I3 = eye(3);
%R matrices
R_gps    = I3*std_gps^2;
R_gpsvel = I3*std_gpsvel^2;
R_mag    = I3*std_mag^2;
%Q matrices
Q_acc   = I3*std_acc^2;
Q_ars   = I3*std_ars^2;
% if Nantennas == 1
%     Q_bacc  = I3*1e-6;
%     Q_bars  = I3*1e-8;
% elseif Nantennas == 2
%     Q_bacc  = I3*1e-6;
%     Q_bars  = I3*1e-8;
% elseif Nantennas == 3
%     Q_bacc  = I3*1e-8;
%     Q_bars  = I3*1e-10;
% end
Q_bacc  = I3*1e-7;
Q_bars  = I3*1e-9;
Q_bmag   = I3*1e-11;
Q       = blkdiag(Q_acc,Q_bacc,Q_ars,Q_bars,Q_bmag);
%P0 matrices
P0_p    = I3*20;
P0_v    = I3*2;
P0_ub   = I3*5e-1;
% if Nantennas == 1
%     P0_bars = I3*1e-4;
%     P0_bacc = I3*1e-4;
% elseif Nantennas == 2
%     P0_bars = I3*1e-3;
%     P0_bacc = I3*1e-3;
% elseif Nantennas == 3
%     P0_bars = I3*1e-7;
%     P0_bacc = I3*1e-6;
% end
P0_bars = I3*1e-9;
P0_bacc = I3*1e-9;
P0_bmag = I3*13-9;
P0      = blkdiag(P0_p,P0_v,P0_bacc,P0_ub,P0_bars,P0_bmag);
%create mekf
mekfinput.P0                    = P0;
mekfinput.x0                    = x0;
mekfinput.Q                     = Q;
mekfinput.R_gps                 = R_gps;
mekfinput.R_gpsvel              = R_gpsvel;
mekfinput.R_mag                 = R_mag;
mekfinput.Nantennas             = Nantennas;
mekfinput.max_P_update_interval = 1;
mekfinput.p_arm1                = p_arm1;
mekfinput.p_arm2                = p_arm2;
mekfinput.p_arm3                = p_arm3;
mekfinput.mag_n                 = mag_n;
mekf                            = class_mekf(mekfinput);

%wind ekf
wind_input.x0    = [0;0;0;1];
wind_input.P0    = blkdiag(1e-4,1e-4,1e-5,1e-7);

Q_v             = blkdiag(1e-5,1e-5,1e-7);
Q_gamma         = 1e-7;
Q_wind          = blkdiag(Q_v,Q_gamma);
wind_input.Q    = Q_wind;

R_pitot         = std_pitot^2;
R_dp            = std_dp^2;
wind_input.R            = R_dp;
wind_input.start_time   = 0;
wind_input.rho          = rho;
wind                    = class_wind_ekf(wind_input);

%mekf with wind
mekf_wwind_input                = mekfinput;
mekf_wwind_input.Q              = mekf_wwind_input.Q(1:12,1:12);
mekf_wwind_input.x0             = [mekf_wwind_input.x0;wind_input.x0];
mekf_wwind_input.P0             = blkdiag(mekf_wwind_input.P0(1:15,1:15),wind_input.P0);
mekf_wwind_input.Q              = blkdiag(mekf_wwind_input.Q,wind_input.Q);
mekf_wwind_input.R_pitot        = wind_input.R;
mekf_wwind_input.rho            = rho;
mekf_wwind                      = class_mekf_wwind(mekf_wwind_input);

%mekf with rvel_wind
mekf_rvel_input                = mekfinput;
mekf_rvel_input.Q              = mekf_rvel_input.Q(1:12,1:12);
mekf_rvel_input.x0             = [mekf_rvel_input.x0;wind_input.x0];
mekf_rvel_input.P0             = blkdiag(mekf_rvel_input.P0(1:15,1:15),wind_input.P0,P0_bmag);
mekf_rvel_input.Q              = blkdiag(mekf_rvel_input.Q,wind_input.Q,Q_bmag);
mekf_rvel_input.R_pitot        = wind_input.R;
mekf_rvel_input.R_mag          = R_mag;
mekf_rvel_input.rho            = rho;
mekf_rvel                      = class_mekf_rvel_wind(mekf_rvel_input);

%wind ta
wind_ta_input   = wind_input;
wind_ta         = class_wind_ta(wind_ta_input);

%wind opt
wind_opt_input.x0               = wind_input.x0;
wind_opt_input.window_length    = 1000;
wind_opt_input.rho              = rho;
wind_opt                        = class_wind_opt(wind_opt_input);

%wind aug
wind_aug_input      = wind_input;
wind_aug_input.P0   = blkdiag(I3*3,1e-2,9);
wind_aug_input.Q    = blkdiag(Q_v,Q_gamma,1000);
wind_aug_input.R    = blkdiag((2/rho)^2*R_dp,1e-8);
wind_aug            = class_wind_aug(wind_aug_input);

%wind 3sf1
wind_3sf1_input                 = wind_input;
wind_3sf1_input.window_length   = 1000;
wind_3sf1_input.rho             = rho;
wind_3sf1                       = class_wind_3sf1(wind_3sf1_input);

%wind 3sf2
wind_3sf2_input                 = wind_input;
wind_3sf2_input.rho             = rho;
wind_3sf2_input.R               = R_dp/10;
wind_3sf2_input.Q               = 10*Q_wind;
wind_3sf2                       = class_wind_3sf2(wind_3sf2_input);

%wind 3sf3
wind_3sf3_input                 = wind_input;
wind_3sf3_input.R               = R_dp;
wind_3sf3_input.rho             = rho;
wind_3sf3                       = class_wind_3sf3(wind_3sf3_input);

%wind adp
wind_adp_input.x0               = [0;0;18;1];
wind_adp_input.P0               = blkdiag(.1,.1,2,.01);
wind_adp_input.Q                = blkdiag(.05,.05,1,1e-6);
wind_adp_input.R                = R_pitot;
wind_adp_input.q0               = q0;
wind_adp                        = class_wind_adp(wind_adp_input);

%ukf adp 
ukf_input.x0                = [0;0;18];
ukf_input.P0                = blkdiag(.1,.1,1);
ukf_input.Q                 = blkdiag(1e-4,1e-4,.1);
ukf_input.R                 = blkdiag(1e-1,1e-1,.3);
ukf                         = class_wind_ukf(ukf_input);

%% --- Run Simulation ---
run_all         = 0;
run_mekf        = 1 || run_all;
run_wind        = 0 || run_all;
run_wind_ta     = 0 || run_all;
run_wind_aug    = 0 || run_all;
run_wind_opt    = 0 || run_all;
run_wind_3sf    = 0 || run_all;
run_mekf_wwind  = 0 || run_all;
run_wind_adp    = 0 || run_all;
% run_mekf_rvel   = 0 || run_all;
run_ukf         = 0 || run_all;

if run_mekf
    es.mekf.data = zeros(19,N);
end
if run_wind
    es.wind.data = zeros(4,N);
end
if run_mekf_wwind
    es.mekf_wwind.data = zeros(20,N);
end
if run_wind_ta
    es.wind_ta.data = zeros(4,N);
end
if run_wind_aug
    es.wind_aug.data = zeros(5,N);
end
if run_wind_opt
    es.wind_opt.data = zeros(4,N);
    es.wind_opt.received = zeros(1,N);
end
if run_wind_3sf
    es.wind_3sf1.data = zeros(4,N);
    es.wind_3sf2.data = zeros(4,N);
    es.wind_3sf3.data = zeros(4,N);
end
if run_wind_adp
    es.wind_adp.data = zeros(4,N);
end
% if run_mekf_rvel
%     es.mekf_rvel.data = zeros(23,N);
% end
if run_ukf
    es.ukf.data = zeros(3,N);
    es.ukf.P    = zeros(3,3,N);
end

es.t = t;

s='1';
tic
last_percent = 0;
mh = MeasurementHandler({acc,ars,gps1,gps2,gps3,pitot,dp,gpsvel1,gpsvel2,mag,adp},t_start);
for i = 1:N
    if i/(N/100) > last_percent
        s = num2str(last_percent);
        last_percent = last_percent + 1;
        fprintf('Done with %s%s: Time elapsed %.2f%s',s,'%',toc,char(10));
%         if i > 1
%             es.mekf_rvel.data(:,i-1)'
%         end
    end
    t_i = t(i);
    measurements = mh.get_measurements(t_i);
    %Filters
    if run_mekf
        es.mekf.data(:,i) = mekf.update(t_i,measurements);
    end
    measurements.b_acc_hat.data = es.mekf.data(7:9,i);
    measurements.b_ars_hat.data = es.mekf.data(14:16,i);
    measurements.velocity.data = es.mekf.data(4:6,i);
    measurements.quaternion.data = es.mekf.data(10:13,i);
    if run_wind
        es.wind.data(:,i) = wind.update(t_i,measurements);
    end
    if run_mekf_wwind
        es.mekf_wwind.data(:,i) = mekf_wwind.update(t_i,measurements);
    end
    if run_wind_ta
        es.wind_ta.data(:,i) = wind_ta.update(t_i,measurements);
    end
    if run_wind_aug
        es.wind_aug.data(:,i) = wind_aug.update(t_i,measurements);
    end
    if run_wind_opt
        wind_opt_ret = wind_opt.update(t_i,measurements);
        if ~isempty(wind_opt_ret)
    %         wind_opt_ret
            es.wind_opt.data(:,i) = wind_opt_ret;
            es.wind_opt.received(i) = 1;
        end
    end
    if run_wind_3sf
        ret_3sf1 = wind_3sf1.update(t_i,measurements);
        es.wind_3sf1.data(:,i) = ret_3sf1.x;
        measurements.x_3sf1 = ret_3sf1.x;
        measurements.C_3sf1 = ret_3sf1.C;
        measurements.y_3sf1 = ret_3sf1.y;
        x_3sf2 = wind_3sf2.update(t_i,measurements);
        es.wind_3sf2.data(:,i) = x_3sf2;
        measurements.x_3sf2 = x_3sf2;
        es.wind_3sf3.data(:,i) = wind_3sf3.update(t_i,measurements);
    end
    if run_wind_adp && ~isempty(measurements.acc.data) && ~isempty(measurements.ars.data)
        es.wind_adp.data(:,i) = wind_adp.update(t_i,measurements);
    end
%     if run_mekf_rvel
%         es.mekf_rvel.data(:,i) = mekf_rvel.update(t_i,measurements);
%     end
    if run_ukf
        [es.ukf.data(:,i),es.ukf.P(:,:,i)] = ukf.update(t_i,measurements);
    end
end
if do_save
    save(sprintf('es%s.mat',XXX),'es');
end 
%% --- Saving ---
t = es.t;
N = length(t);
%MEKF
mekf_pos.t = t;
mekf_pos.data = es.mekf.data(1:3,:);
mekf_vel.t = t;
mekf_vel.data = es.mekf.data(4:6,:);
mekf_bacc.t = t;
mekf_bacc.data = es.mekf.data(7:9,:);
mekf_ea.t       = t;
mekf_ea.data    = zeros(3,N);

% gps_vel1.t = gpsvel1.t;
% gps_vel1.data = gpsvel1.data;

rvel = zeros(3,N);
aoa = zeros(1,N);
ssa = zeros(1,N);
Vwb = zeros(1,N);
for i = 1:N
    Rtemp = Rquat(es.mekf.data(10:13,i));
    [a1,a2,a3] = R2euler(Rtemp);
    mekf_ea.data(:,i) = [a1;a2;a3];
    
    rvel(:,i) = Rtemp'*(mekf_vel.data(:,i)-es.wind.data(1:3,i));
    Vwb(i) = norm(rvel(:,i));
    aoa(i) = atan2(rvel(3,i),rvel(1,i));
    ssa(i) = asin(rvel(2,i)/norm(rvel(:,i)));
end
mekf_bars.t = t;
mekf_bars.data = es.mekf.data(14:16,:);

%Wind EKF
wind_vnw.t = t;
wind_vnw.data = es.wind.data(1:3,:);
wind_gamma.t = t;
wind_gamma.data = es.wind.data(4,:); 
wind_aoa.t = t;
wind_aoa.data = aoa*RAD2DEG;
wind_ssa.t = t;
wind_ssa.data = ssa*RAD2DEG;
wind_vwb.t = t;
wind_vwb.data = rvel;
wind_Vwb.t = t;
wind_Vwb.data = Vwb;

%MEKF with wind
mekf_wwind_pos.t = t;
mekf_wwind_pos.data = es.mekf_wwind.data(1:3,:);
mekf_wwind_vel.t = t;
mekf_wwind_vel.data = es.mekf_wwind.data(4:6,:);
mekf_wwind_bacc.t = t;
mekf_wwind_bacc.data = es.mekf_wwind.data(7:9,:);
mekf_wwind_ea.t       = t;
mekf_wwind_ea.data    = zeros(3,N);
mekf_wwind_bars.t = t;
mekf_wwind_bars.data = es.mekf_wwind.data(14:16,:);
mekf_wwind_vnw.t = t;
mekf_wwind_vnw.data = es.mekf_wwind.data(17:19,:);
mekf_wwind_gamma.t = t;
mekf_wwind_gamma.data = es.mekf_wwind.data(20,:);
rvel_wwind = zeros(3,N);
aoa_wwind = zeros(1,N);
ssa_wwind = zeros(1,N);
Vwb_wwind = zeros(1,N);
for i = 1:N
    Rtemp = Rquat(es.mekf_wwind.data(10:13,i));
    [a1,a2,a3] = R2euler(Rtemp);
    mekf_wwind_ea.data(:,i) = [a1;a2;a3];
    
    rvel_wwind(:,i) = Rtemp'*(mekf_wwind_vel.data(:,i)-mekf_wwind_vnw.data(1:3,i));
    Vwb_wwind(i) = norm(rvel_wwind(:,i));
    aoa_wwind(i) = atan2(rvel_wwind(3,i),rvel_wwind(1,i));
    ssa_wwind(i) = asin(rvel_wwind(2,i)/norm(rvel_wwind(:,i)));
end
mekf_wwind_aoa.t = t;
mekf_wwind_aoa.data = aoa_wwind*RAD2DEG;
mekf_wwind_ssa.t = t;
mekf_wwind_ssa.data = ssa_wwind*RAD2DEG;
mekf_wwind_vwb.t = t;
mekf_wwind_vwb.data = rvel_wwind;

%Wind OPT
inds = find(es.wind_opt.received);
wind_opt_vnw.t = t(inds);
wind_opt_vnw.data = es.wind_opt.data(1:3,inds);
wind_opt_gamma.t = t(inds);
wind_opt_gamma.data = es.wind_opt.data(4,inds);

%Wind TA
wind_ta_vnw.t = t;
wind_ta_vnw.data = es.wind_ta.data(1:3,:);
wind_ta_gamma.t = t;
wind_ta_gamma.data = 1./es.wind_ta.data(4,:);

%Wind AUG
wind_aug_vnw.t = t;
wind_aug_vnw.data = es.wind_aug.data(1:3,:);
wind_aug_gamma.t = t;
wind_aug_gamma.data = es.wind_aug.data(4,:);
wind_aug_vnorm.t = t;
wind_aug_vnorm.data = es.wind_aug.data(5,:);

%Wind 3SF1
wind_3sf1_vnw.t = t;
wind_3sf1_vnw.data = es.wind_3sf1.data(1:3,:);
wind_3sf1_gamma.t = t;
wind_3sf1_gamma.data = es.wind_3sf1.data(4,:);

%Wind 3SF2
wind_3sf2_vnw.t = t;
wind_3sf2_vnw.data = es.wind_3sf2.data(1:3,:);
wind_3sf2_gamma.t = t;
wind_3sf2_gamma.data = es.wind_3sf2.data(4,:);

%Wind 3SF3
wind_3sf3_vnw.t = t;
wind_3sf3_vnw.data = es.wind_3sf3.data(1:3,:);
wind_3sf3_gamma.t = t;
wind_3sf3_gamma.data = es.wind_3sf3.data(4,:);
wind_3sf3_vwb.data = zeros(3,N);   wind_3sf3_vwb.t = t;
wind_3sf3_aoa.data = zeros(1,N);    wind_3sf3_aoa.t = t;
wind_3sf3_ssa.data = zeros(1,N);    wind_3sf3_ssa.t = t;
wind_3sf3_Vwb.data = zeros(1,N);    wind_3sf3_Vwb.t = t;
for i = 1:N
    Rtemp = Rquat(es.mekf.data(10:13,i));
    [a1,a2,a3] = R2euler(Rtemp);
    mekf_ea.data(:,i) = [a1;a2;a3];
    
    wind_3sf3_vwb.data(:,i) = Rtemp'*(mekf_vel.data(:,i)-wind_3sf3_vnw.data(1:3,i));
    wind_3sf3_Vwb.data(i) = norm(wind_3sf3_vwb.data(:,i));
    wind_3sf3_aoa.data(i) = atan2(wind_3sf3_vwb.data(3,i),wind_3sf3_vwb.data(1,i))*RAD2DEG;
    wind_3sf3_ssa.data(i) = asin(wind_3sf3_vwb.data(2,i)/norm(wind_3sf3_vwb.data(:,i)))*RAD2DEG;
end

%Wind ADP
wind_adp_aoa.t = t;
wind_adp_aoa.data = es.wind_adp.data(1,:);
wind_adp_ssa.t = t;
wind_adp_ssa.data = es.wind_adp.data(2,:);
wind_adp_airspeed.t = t;
wind_adp_airspeed.data = es.wind_adp.data(3,:);
% wind_adp_aoa.t = t;
% wind_adp_aoa.data = es.wind_adp.data(1:3,:);
wind_adp_gamma.t = t;
wind_adp_gamma.data = es.wind_adp.data(4,:);

% %Wind RVEL
% mekf_rvel_pos.t = t;
% mekf_rvel_pos.data = es.mekf_rvel.data(1:3,:);
% mekf_rvel_rvel.t = t;
% mekf_rvel_rvel.data = es.mekf_rvel.data(4:6,:);
% mekf_rvel_bacc.t = t;
% mekf_rvel_bacc.data = es.mekf_rvel.data(7:9,:);
% mekf_rvel_ea.t       = t;
% mekf_rvel_ea.data    = zeros(3,N);
% mekf_rvel_bars.t = t;
% mekf_rvel_bars.data = es.mekf_rvel.data(14:16,:);
% mekf_rvel_vnw.t = t;
% mekf_rvel_vnw.data = es.mekf_rvel.data(17:19,:);
% mekf_rvel_gamma.t = t;
% mekf_rvel_gamma.data = es.mekf_rvel.data(20,:);
% rvel_vel = zeros(3,N);
% rvel_aoa = zeros(1,N);
% rvel_ssa = zeros(1,N);
% for i = 1:N
%     Rtemp = Rquat(es.mekf_rvel.data(10:13,i));
%     [a1,a2,a3] = R2euler(Rtemp);
%     mekf_rvel_ea.data(:,i) = [a1;a2;a3];
%     
%     rvel_vel(:,i) = mekf_rvel_vnw.data(:,i) + Rtemp*mekf_rvel_rvel.data(:,i);
%     rvel_aoa(i) = atan2(mekf_rvel_rvel.data(3,i),mekf_rvel_rvel.data(1,i));
%     rvel_ssa(i) = asin(mekf_rvel_rvel.data(2,i)/norm(mekf_rvel_rvel.data(:,i)));
% end
% mekf_rvel_aoa.t = t;
% mekf_rvel_aoa.data = rvel_aoa*RAD2DEG;
% mekf_rvel_ssa.t = t;
% mekf_rvel_ssa.data = rvel_ssa*RAD2DEG;
% mekf_rvel_vel.t = t;
% mekf_rvel_vel.data = rvel_vel;

%UKF
ukf_aoa.t = t;
ukf_aoa.data = es.ukf.data(1,:)*RAD2DEG;
ukf_ssa.t = t;
ukf_ssa.data = es.ukf.data(2,:)*RAD2DEG;
ukf_Vwb.t = t;
ukf_Vwb.data = es.ukf.data(3,:)*RAD2DEG;

%AOA APPROXIMATION pitch - flight path angle
fpa = zeros(1,N);
for i = 1:N
    fpa(i) = atan2(mekf_vel.data(3,i),norm(mekf_vel.data(1:2,i)));
end
aoa_approx.t = mekf_ea.t;
aoa_approx.data = (mekf_ea.data(2,:)+fpa)*RAD2DEG;

%SSA APPROXIMATION course - heading
course = zeros(1,N);
for i = 1:N
    course(i) = atan2(mekf_vel.data(2,i),mekf_vel.data(1,i));
end
ssa_approx.t = mekf_vel.t;
ssa_approx.data = wrapToPi(course - mekf_ea.data(3,:))*RAD2DEG;

%Aeroprobe
cal = [-0.0594;-0.0276];
ap_aoa.t = ap.aoa.t;
ap_aoa.data = (ap.aoa.data+cal(1))*RAD2DEG;
ap_ssa.t = ap.ssa.t;
ap_ssa.data = (ap.ssa.data+cal(2))*RAD2DEG;
ap_rvel.t = ap.rvel.t;
ap_rvel.data = zeros(3,length(ap_rvel.t));
for i = 1:length(ap_rvel.t)
    a = ap.aoa.data(i)+cal(1);
    b = ap.ssa.data(i)+cal(2);
    ap_rvel.data(:,i) = ap.airspeed.data(i)*[...
                            cos(a)*cos(b);
                                   sin(b);
                            sin(a)*cos(b)];
end

%RTK Ground Truth
rtk_ea.t = gps1.t;
rtk_pos.t = gps1.t;
pos1 = gps1.data;
pos2 = zeros(size(pos1));
pos3 = zeros(size(pos1));
for i = 1:3
    pos2(i,:) = interp1(gps2.t,gps2.data(i,:),gps1.t);
    pos3(i,:) = interp1(gps3.t,gps3.data(i,:),gps1.t);
end
p12_b = p_arm1-p_arm2; p12_b = p12_b/norm(p12_b);
p23_b = p_arm2-p_arm3; p23_b = p23_b/norm(p23_b);
p13_b = p_arm1-p_arm3; p13_b = p13_b/norm(p13_b);
Pb = [p12_b,Smtrx(p12_b)*p23_b,Smtrx(p12_b)^2*p23_b];
for i = 1:length(gps1.t)
    p12_n = pos1(:,i)-pos2(:,i); p12_n = p12_n/norm(p12_n);
    p23_n = pos2(:,i)-pos3(:,i); p23_n = p23_n/norm(p23_n);
    p13_n = pos1(:,i)-pos3(:,i); p13_n = p13_n/norm(p13_n);
    Pn = [p12_n,Smtrx(p12_n)*p23_n,Smtrx(p12_n)^2*p23_n];
    Rnb = Pn*pinv(Pb);
    [a1,a2,a3] = R2euler(Rnb);
    rtk_ea.data(:,i) = [a1;a2;a3];
    p1 = pos1(:,i) - Rnb*p_arm1;
    p2 = pos2(:,i) - Rnb*p_arm2;
    p3 = pos3(:,i) - Rnb*p_arm3;
    rtk_pos.data(:,i) = (p1+p2+p3)/3;
end

pd_wind2.v_nw = {px.wvel,wind_vnw,wind_ta_vnw,wind_aug_vnw,wind_opt_vnw,wind_3sf1_vnw,wind_3sf2_vnw,wind_3sf3_vnw};
pd_wind2.gamma = {0,wind_gamma,wind_ta_gamma,wind_aug_gamma,wind_opt_gamma,wind_3sf1_gamma,wind_3sf2_gamma,wind_3sf3_gamma};
%% --- Plotting states of MEKF ---
pd.pos = {px.pos,mekf_pos,rtk_pos,mekf_wwind_pos};
pd.vel = {px.vel,mekf_vel,0,mekf_wwind_vel};
pd.b_acc = {0,mekf_bacc,0,mekf_wwind_bacc};
pd.ea = {px.ea,mekf_ea,rtk_ea,mekf_wwind_ea};
pd.b_ars = {0,mekf_bars,0,mekf_wwind_bars};

plotinput = make_plotinput();
plotinput.legends = {'PX','MEKF','RTK','MEKF W/ WIND'};
plotinput.savesuffix = sprintf('-%i',Nantennas);
plotter(pd,plotinput);

%% --- Plotting states of selected wind estimators ---
pd_wind.v_nw =  {0,           px.wvel,      wind_vnw  ,wind_3sf3_vnw};
pd_wind.gamma = {0,           0,            wind_gamma,wind_3sf3_gamma};
pd_wind.aoa =   {ap_aoa,      px.aoa,       wind_aoa  ,wind_3sf3_aoa};
pd_wind.ssa =   {ap_ssa,      px.ssa,       wind_ssa  ,wind_3sf3_ssa};
pd_wind.Vwb =   {ap.airspeed, px.airspeed,  wind_Vwb  ,wind_3sf3_Vwb};
pd_wind.vwb =   {ap_rvel,     px.rvel,      wind_vwb  ,wind_3sf3_vwb};


plotinput = make_plotinput();
plotinput.legends = {'Aeroprobe', 'Pixhawk','Wind EKF','Wind 3SF3'};
plotinput.ylabels = {{'N [$m/s$]','E [$m/s$]','D [$m/s$]'},{'Gamma'},{'AOA [deg]'},{'SSA [deg]'},{'$\|V_wb\|_2$'},{'X [$m/s$]','Y [$m/s$]','Z [$m/s$]'}};
plotinput.labels  = {'Wind Velocity','Gamma','AOA','SSA','Airspeed','Relative Velocity'};
% plotinput.figpos = [0,0,800,500];
plotinput.figpos = [0,0,1000,600];
if strcmp(XXX,'005')
    plotinput.xlims = {[300,2400],[300,2400],[2200,2400],[2200,2400],[2000,2400],[0,2400]};
elseif strcmp(XXX,'010')
    plotinput.xlims = {[450,2400],[450,1960],[1760,1960],[1760,1960],[1760,1960],[500,1960]};
end
plotinput.do_save = 0;
plotinput.savefolder = sprintf('~/work/postdoc/latex/papers/windestimation/plots%s/',XXX);
plotinput.savetype = '.eps';
% plotinput.savesuffix = sprintf('-%i',Nantennas);
plotter(pd_wind,plotinput);



%% --- Plotting states of all wind estimators
plotinput = make_plotinput();
plotinput.legends = {'Pixhawk', 'Wind EKF','Wind TA','Wind AUG','Wind OPT','Wind 3SF1','Wind 3SF2','Wind 3SF3','MEKF /W WIND'};
plotinput.ylabels = {{'N [$m/s$]','E [$m/s$]','D [$m/s$]'},{'Gamma'}};
plotinput.labels  = {'Wind Velocity','Gamma'};
plotinput.figpos = [0,0,800,500];
plotinput.do_save = 0;
plotinput.linewidth = 2;
plotinput.savesuffix = sprintf('-%i',Nantennas);
plotter(pd_wind2,plotinput);

%%
t_divide = 1300;
inds1 = find(t < t_divide);
inds2 = find(t > t_divide);
figure; hold on; plot3(es.mekf.data(1,inds1),es.mekf.data(2,inds1),es.mekf.data(3,inds1),'r')
hold on; plot3(es.mekf.data(1,inds2),es.mekf.data(2,inds2),es.mekf.data(3,inds2),'g')

