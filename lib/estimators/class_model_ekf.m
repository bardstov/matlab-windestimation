classdef class_model_ekf < handle
    %EKF Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        %states
        x;
        
        %air density
        rho;
        %Wing span
        S;
        %chord line
        c;
        %mass
        m
        
        %Covariance matrices
        Q;
        P;
        R_dp;
        R_f;
        
        %Identity matrix of appropriate size
        I;
        
        %in order to calculate time step
        prev_time;
        
        %enables delayed starting
        start_time = 0;
        
        O = zeros(4,4);
        O_counter = 0;
        
    end
    
    methods
        function kf = class_model_ekf(input)
            kf.x                           = input.x0;
            kf.rho                      = input.rho;
            kf.S                        = input.S_wing;
            kf.c                        = input.c;
            kf.m                        = input.mass;
            kf.P                        = input.P0;
            kf.Q                        = input.Q;
            kf.I                        = eye(size(input.P0));
            kf.R_dp                     = input.R_dp;
            kf.R_f                      = input.R_f;
            if isfield(input,'start_time')
                kf.start_time = input.start_time;
            end
        end
        function x_ret = update(kf,time,measurements)
            acc     = measurements.acc.data;
            v_nb    = measurements.velocity.data;
            dp      = measurements.dp.data;
            q       = measurements.quaternion.data; 
            P_ret = diag(kf.P);
            x_ret = kf.x;
            if isempty(dp) && isempty(acc)
                return
            end
            
            if kf.start_time ~= -Inf && time < kf.start_time
                return
            else
                kf.start_time = -Inf;
            end
            
            %find dt
            dt = min([.1,time-kf.prev_time]);
            kf.prev_time=time;

            %cov time update
            kf.P = kf.P + kf.Q*dt;
            kf.P = .5*(kf.P+kf.P');
            
            %no state update
            
            % --- KF update ---
            %<<< OLD
%             y = v_abs;
%             relvel = (v_nb-kf.v_nw);
%             relvel_n = norm(relvel);
%             y_hat = kf.gamma*relvel_n;
%             H = [-kf.gamma*(relvel)'/relvel_n, relvel_n];
            % OLD >>>
            %<<< NEW
            v_nw = kf.x(1:3);
            gamma = kf.x(4);
            C_L_0 = kf.x(5);
            C_L_alpha = kf.x(6);
            rvel_n = v_nb-v_nw;
            y = [];
            y_hat = [];
            H = [];
            R = [];
            if ~isempty(dp)
                y = [y;dp];
                y_hat = [y_hat;.5*kf.rho*gamma*(rvel_n'*rvel_n)];
                H = [H;-kf.rho*gamma*rvel_n',.5*kf.rho*(rvel_n'*rvel_n),0,0];
                R = blkdiag(R,kf.R_dp);
            end
            if ~isempty(acc)
                Rbn = Rquat(q)';
                R1 = Rbn(1,:);
                R3 = Rbn(3,:);
                y = [y;-kf.m*acc(3)];
                y_hat = [y_hat;
                         .5*kf.rho*kf.S*kf.c*(rvel_n'*rvel_n)*(C_L_0 + C_L_alpha*atan2(R3*rvel_n,R1*rvel_n))];
                H_v = .5*kf.rho*kf.S*kf.c*((rvel_n'*rvel_n)*C_L_alpha*rvel_n'*(R3'*R1-R1'*R3)/((R1*rvel_n)^2+(R3*rvel_n)^2) ...
                      -2*rvel_n'*(C_L_0 + C_L_alpha*atan2(R3*rvel_n,R1*rvel_n)));
                H_CL0 = .5*kf.rho*kf.S*kf.c*((rvel_n'*rvel_n));
                H_CLalpha = .5*kf.rho*kf.S*kf.c*(rvel_n'*rvel_n)*atan2(R3*rvel_n,R1*rvel_n);
                H = [H; H_v, 0, H_CL0, H_CLalpha];
                R = blkdiag(R,kf.R_f);
            end
            H = H.*[1,1,1,1,1,0];
%             size(kf.P)
%             size(H)
%             size(R)
            K = kf.P*H'/(H*kf.P*H'+R);
            kf.P = (kf.I-K*H)*kf.P*(kf.I-K*H)'+K*R*K';
            kf.P = (kf.P+kf.P')/2;
            ytilde = y-y_hat;
            xtilde = K*ytilde;

            kf.x = kf.x + xtilde;
            x_ret = kf.x;
        end
    end
end

