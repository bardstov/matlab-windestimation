classdef Input < handle
    %INPUT Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        data;
        output_names;
        timevarying = false;
        t;
        counter = int64(1);
    end
    
    methods
        function obj = Input(input)
            obj.data = input.data;
            obj.output_names = input.output_names;
            if isfield(input,'t')
                obj.timevarying = true;
                obj.t = input.t;
            end
        end
        
        function x = update(obj,sim)
            if ~obj.timevarying
                x = {obj.data};
            else
                while obj.counter < length(obj.t) && obj.t(obj.counter) < sim.time
                    obj.counter = obj.counter + 1;
                end
                x = {obj.data(:,obj.counter)};
            end
        end
    end
end

