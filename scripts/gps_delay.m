%This scripts attempts to find the time delay of gps-measurements relative
%to the imu.
%This is done by comparing ADIS.t with PX.t and GPS.t with PX.t. Then, we
%can find the difference between ADIS.t and GPS.t through PX.t

id = 1;

XXXs = {'005','007','010'};
XXX = XXXs{id};

load_exp_measurements

%% find biases

%% Find timedelay between ADIS and PX

%% Find PX GPS
spheroid = wgs84Ellipsoid('meters');
llh0 = [63.319194188;10.272568042;43.0727];
pxllh = [rpx.GPS_Lat;rpx.GPS_Lng;rpx.GPS_Alt];
px.gps.t = double(rpx.GPS_TimeUS)*1e-6;
for i = 1:size(pxllh,2)
        %llh to ecef
        [x,y,z] = geodetic2ecef(spheroid,pxllh(1,i),pxllh(2,i),pxllh(3,i));        
        %ecef to ned
        [x,y,z] = ecef2ned(x,y,z,llh0(1),llh0(2),llh0(3),spheroid);
        px.gps.data(:,i) = [x;y;z];
end

%% Find timedelay between PX and GPS
f = 'pos3'
fun = @(x)(lsqfun(x,px.gps.t,px.gps.data(1:2,:),gps.(f).t,gps.(f).data(1:2,:)));
x = lsqnonlin(fun,[100,1]);
x(1)
x(2)-1

pin.legends = {'gps','px old','px new'};
figure;
subplotter(gps1.t,gps1.data,pin)
subplotter(px.gps.t/t_timescale-t_offset,px.gps.data,pin);
subplotter(px.gps.t*x(2)-x(1),px.gps.data,pin);

%% Find timedelay between ADIS and PX
imu = adis;
f = 'ars';
myv.t = imu.(f).t; myv.data = lowpass(imu.(f).data',25,250)';
pxv.t = double(rpx.IMU_TimeUS)*1e-6; pxv.data = lowpass([rpx.IMU_GyrX;rpx.IMU_GyrY;rpx.IMU_GyrZ]',5,50)';
fun = @(x)(lsqfun(x,pxv.t,pxv.data,myv.t,myv.data));
x = lsqnonlin(fun,[86,1]);

pin.legends = {'my','px old','px new'};
figure;
subplotter(myv.t,myv.data,pin)
subplotter(pxv.t/t_timescale-t_offset,pxv.data,pin);
subplotter(pxv.t*x(2)-x(1),pxv.data,pin);

%% LSQ func
function y = lsqfun(x,t1,v1,t2,v2)
%     fprintf('x: %f\n',x);
    v2_inp = interpolateNd(t2,v2,t1*x(2)-x(1));
    y = sum((v1-v2_inp).^2,1);
%     fprintf('y: %f\n',y)
end