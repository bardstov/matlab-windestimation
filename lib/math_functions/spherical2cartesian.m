function cart = spherical2cartesian(r,el,az)
%     cart = r*[sin(el)*cos(az);
%               sin(el)*sin(az);
%               cos(el)];
    cart = r*[ cos(el)*cos(az);
               cos(el)*sin(az);
              -sin(el)];
end

