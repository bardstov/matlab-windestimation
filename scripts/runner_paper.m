close all
addlib

DEG2RAD = pi/180;
RAD2DEG = 1/DEG2RAD;

init_noise = 1;
meas_noise = 1;
do_bias = 1;

t_end = Inf; % Inf == all
tstart = 0;

%% --- LOAD DATA ---
tic
ss = importdata('data.mat');

t_end = min([ss.pos.t(end),t_end]);
lol = find(ss.pos.t >= t_end);
N = lol(1);
t = ss.pos.t(1:N);
h = t(2)-t(1);

%create wind velocity
v_nw.t = t;
v_nw.data = [5;-5;2]*ones(1,N);

%% --- NOISE AND BIAS ---
std_gps     = meas_noise*1e-2;
std_gpsvel  = meas_noise*1e-2;
std_acc     = meas_noise*1e-3;
std_ars     = meas_noise*1e-3;
std_dp      = meas_noise*3;
std_pitot   = meas_noise*.06;
std_mag     = meas_noise*.05;

bias_acc = [.05,-.04,.06]'*do_bias;
bias_ars = [.08,-.06,-.1]'*do_bias;

%% --- SENSORS ---
%ARS
ars_input = ss.ars;
ars_input.data = ss.ars.data + bias_ars*ones(1,size(ss.ars.data,2)) + std_ars*randn(size(ss.ars.data));
ars_input.tag = 'ars';
ars = Measurement(ars_input);
%ACC
acc_input = ss.acc;
acc_input.data = acc_input.data + bias_acc*ones(1,size(acc_input.data,2)) + std_acc*randn(size(acc_input.data));
acc_input.tag = 'acc';
acc = Measurement(acc_input);
%MAG
mag_n = [.134, 9e-3,.478]';
bias_mag = [.1,.2,.3]';
mag_input.t = t;
mag_input.data = zeros(3,N);
mag_input.tag = 'mag';
for i = 1:N
    Rnb = Rzyx(ss.ea.data(1,i),ss.ea.data(2,i),ss.ea.data(3,i));
    mag_input.data(:,i) = Rnb'*mag_n + bias_mag + std_mag*randn(3,1);
end
mag = Measurement(mag_input);
%GPS
f_gps = 1;
t_gps = 0:1/f_gps:t(end)-1/f_gps;

Nantennas = 2;
p_arm1 = [  0.347; -0.010; -0.045];
p_arm2 = [ -0.226;  0.904;  0.000];
p_arm3 = [ -0.285; -0.894;  0.005];
gps1_input.t = t_gps;
gps2_input.t = t_gps;
gps3_input.t = t_gps;
gps1_input.data = zeros(3,length(t_gps));
gps2_input.data = zeros(3,length(t_gps));
gps3_input.data = zeros(3,length(t_gps));

j = 1;
for i = 1:length(t_gps)
    while t(j) < t_gps(i)
        j = j+1;
    end
    Rnb = Rzyx(ss.ea.data(1,j),ss.ea.data(2,j),ss.ea.data(3,j));
    gps1_input.data(:,i) = ss.pos.data(:,j) + Rnb*p_arm1 + std_gps*randn(3,1);
    gps2_input.data(:,i) = ss.pos.data(:,j) + Rnb*p_arm2 + std_gps*randn(3,1);
    gps3_input.data(:,i) = ss.pos.data(:,j) + Rnb*p_arm3 + std_gps*randn(3,1);
end

gps1_input.tag  = 'gps1';
gps1            = Measurement(gps1_input);

gps2_input.tag  = 'gps2';
gps2            = Measurement(gps2_input);

gps3_input.tag  = 'gps3';
gps3    = Measurement(gps3_input);

%Pitot tube
gamma = 1.1;
f_pitot = 5;
t_pitot = 0:1/f_pitot:t(end)-1/f_pitot;
pitot_input.t = t_pitot;
pitot_input.data = zeros(1,length(t_pitot));
dp_input.data = zeros(1,length(t_pitot));
dp_input.t = t_pitot;
j = 1;
rho = 1.225;
for i = 1:length(t_pitot)
    while t(j) < t_pitot(i)
        j = j+1;
    end
    dp_input.data(i) = gamma*(norm(ss.vel.data(:,j)-v_nw.data(:,j)))^2*rho/2 + std_dp*randn;
    truedp(i) = gamma*(norm(ss.vel.data(:,j)-v_nw.data(:,j)))^2*rho/2;
    truepitot(i) = sqrt(truedp(i)*2/rho);
    pitot_input.data(i) = sqrt(dp_input.data(i)*2/rho);
end
pitot_input.tag = 'pitot';
pitot = Measurement(pitot_input);

% figure;
% hist(dp_input.data-truedp,20);
% figure
% hist(pitot_input.data-truepitot,20);

dp_input.tag = 'dp';
dp = Measurement(dp_input);

%% --- Inputs ---
%G_N
g_input.output_names = {'g_n'};
g_input.data = [0;0;9.81];
g_n = Input(g_input);
%Time
time = Time();
%ea
ea_input = ss.ea;
ea_input.output_names = {'ea'};
ea = Input(ea_input);
% %vn
% vn_input = ss.vel;
% vn_input.output_names = {'vn'};
% vn = Input(vn_input);
% %pos
% pn_input = ss.pos;
% pn_input.output_names = {'pn'};
% pn = Input(pn_input);

%% --- Filters ---
ars_std = max([std_ars,1e-4]);
acc_std = max([std_acc,1e-4]);
gps_std = max([std_gps,1e-2]);
gpsvel_std = max([std_gpsvel,1e-2]);
dp_std = max([std_dp,1e-4]);
pitot_std = max([std_pitot,1e-4]);
mag_std = max([std_mag,1e-4]);
I3 = eye(3);
%states
p0      = ss.pos.data(:,1) + randn(3,1)*10*init_noise;
v0      = ss.vel.data(:,1) + randn(3,1)*1*init_noise;
b_acc0  = bias_acc + randn(3,1)*.005*init_noise;
ea0     = ss.ea.data(:,1) + randn(3,1)*.2*init_noise;
q0      = euler2q(ea0(1),ea0(2),ea0(3));
b_ars0  = bias_ars + randn(3,1)*.005*init_noise;
x0      = [p0;v0;b_acc0;q0;b_ars0];
%R matrices
R_gps   = I3*gps_std^2;
R_gpsvel= I3*gpsvel_std^2;
R_mag   = I3*mag_std^2;
%Q matrices
Q_acc   = I3*acc_std^2;
Q_ars   = I3*ars_std^2;
if Nantennas == 1
    Q_bacc  = I3*1e-8;
    Q_bars  = I3*1e-9;
elseif Nantennas == 2
    Q_bacc  = I3*1e-9;
    Q_bars  = I3*1e-12;
elseif Nantennas == 3
    Q_bacc  = I3*1e-8;
    Q_bars  = I3*1e-12;
end
Q_bmag   = I3*1e-3;
Q       = blkdiag(Q_acc,Q_bacc,Q_ars,Q_bars,Q_bmag);

%P0 matrices
P0_p    = I3*20;
P0_v    = I3*2;
P0_ub   = I3*5e-1;
if Nantennas == 1
    P0_bars = I3*1e-8;
    P0_bacc = I3*1e-7;
elseif Nantennas == 2
    P0_bars = I3*1e-7;
    P0_bacc = I3*1e-5;
elseif Nantennas == 3
    P0_bars = I3*1e-7;
    P0_bacc = I3*1e-5;
end
P0_bmag = I3*1e-1;
P0      = blkdiag(P0_p,P0_v,P0_bacc,P0_ub,P0_bars,P0_bmag);
%create mekf
mekfinput.P0                    = P0;
mekfinput.x0                    = x0;
mekfinput.Q                     = Q;
mekfinput.R_gps                 = R_gps;
mekfinput.R_gpsvel              = R_gpsvel;
mekfinput.R_mag                 = R_mag;
mekfinput.mag_n                 = mag_n;
mekfinput.Nantennas             = Nantennas;
mekfinput.max_P_update_interval = 1;
mekfinput.time_step             = h;
mekfinput.p_arm1                = p_arm1;
mekfinput.p_arm2                = p_arm2;
mekfinput.p_arm3                = p_arm3;
mekf                            = class_mekf(mekfinput);

%wind ekf
R_airspeed      = pitot_std^2;
R_dp            = dp_std^2;

wind_input.x0           = [0;0;0;1];
wind_input.P0           = blkdiag(I3*5,1e-6);
Q_v                     = I3*1e-4;
Q_gamma                 = 1e-8;
Q_wind                  = blkdiag(Q_v,Q_gamma);
wind_input.Q            = Q_wind;
wind_input.R            = R_dp;
wind_input.start_time   = 0;
wind_input.rho          = rho;
wind                    = class_wind_ekf(wind_input);

%wind opt
wind_opt_input.x0               = wind_input.x0;
wind_opt_input.window_length    = 500;
wind_opt_input.rho              = rho;
wind_opt                        = class_wind_opt(wind_opt_input);

%wind ta
wind_ta_input   = wind_input;
% wind_ta_input.Q = blkdiag(I3*1e-300,1e-400);
wind_ta         = class_wind_ta(wind_ta_input);

%wind aug
wind_aug_input      = wind_input;
wind_aug_input.P0   = blkdiag(I3*50,1e-2,250);
wind_aug_input.Q    = blkdiag(Q_v,Q_gamma,5);
wind_aug_input.R    = blkdiag((2/rho)^2*R_dp,1e-2);
wind_aug            = class_wind_aug(wind_aug_input);

%wind 3sf1
wind_3sf1_input                 = wind_input;
wind_3sf1_input.window_length   = 500;
wind_3sf1_input.rho             = rho;
wind_3sf1                       = class_wind_3sf1(wind_3sf1_input);

%wind 3sf2
wind_3sf2_input                 = wind_input;
wind_3sf2_input.rho             = rho;
wind_3sf2_input.R               = 5*R_dp;
wind_3sf2                       = class_wind_3sf2(wind_3sf2_input);

%wind 3sf3
wind_3sf3_input                 = wind_input;
wind_3sf3_input.R               = R_dp;
wind_3sf3_input.rho             = rho;
wind_3sf3                       = class_wind_3sf3(wind_3sf3_input);

% %wind adp
% wind_adp_input.x0               = [0;0;18;1];
% wind_adp_input.R                = R_airspeed;
% wind_adp_input.q0               = q0;
% wind_adp_input.P0               = blkdiag(;
% wind_adp                        = class_wind_adp(wind_adp_input);

%mekf with rvel_wind
mekf_rvel_input                = mekfinput;
mekf_rvel_input.Q              = mekf_rvel_input.Q(1:12,1:12);
mekf_rvel_input.x0             = [mekf_rvel_input.x0;wind_input.x0];
mekf_rvel_input.P0             = blkdiag(mekf_rvel_input.P0(1:15,1:15),wind_input.P0,P0_bmag);
mekf_rvel_input.Q              = blkdiag(mekf_rvel_input.Q,wind_input.Q,Q_bmag);
mekf_rvel_input.R_pitot        = wind_input.R;
mekf_rvel_input.rho            = rho;
mekf_rvel                      = class_mekf_rvel_wind(mekf_rvel_input);

%% --- Run Simulation ---
run_all     = 1;
run_mekf    = 0;            run_mekf = run_all || run_mekf;
run_wind    = 1;            run_wind = run_all || run_wind;
run_wind_ta = 0;         run_wind_ta = run_all || run_wind_ta;
run_wind_aug = 0;       run_wind_aug = run_all || run_wind_aug;
run_wind_opt = 0;       run_wind_opt = run_all || run_wind_opt;
run_wind_3sf = 1;       run_wind_3sf = run_all || run_wind_3sf;
% run_wind_adp = 1;       run_wind_adp = run_all || run_wind_adp;
run_mekf_rvel= 1;      run_mekf_rvel = run_all || run_mekf_rvel;

if run_mekf
    es.mekf.data = zeros(19,N);
end
if run_wind
    es.wind.data = zeros(4,N);
end
if run_wind_ta
    es.wind_ta.data = zeros(4,N);
end
if run_wind_aug
    es.wind_aug.data = zeros(5,N);
end
if run_wind_opt
    es.wind_opt.data = zeros(4,N);
    es.wind_opt.received = zeros(1,N);
end
if run_wind_3sf
    es.wind_3sf1.data = zeros(4,N);
    es.wind_3sf2.data = zeros(4,N);
    es.wind_3sf3.data = zeros(4,N);
end
% if run_wind_adp
%     es.wind_adp.data = zeros(4,N);
% end
if run_mekf_rvel
    es.mekf_rvel.data = zeros(23,N);
end
s='1';
tic
last_percent = 0;
mh = MeasurementHandler({acc,ars,gps1,gps2,gps3,pitot,dp,mag},tstart);
for i = 1:N
    if i/(N/100) > last_percent
        s = num2str(last_percent);
        last_percent = last_percent + 1;
        fprintf('Done with %s%s: Time elapsed %.2f%s',s,'%',toc,char(10));
    end
    t_i = t(i);
    measurements = mh.get_measurements(t_i);
    
    %Filters
    if run_mekf
        es.mekf.data(:,i) = mekf.update(t_i,measurements);
    end
    measurements.velocity.data = es.mekf.data(4:6,i);
    measurements.quaternion.data = es.mekf.data(10:13,i);
    if run_wind
        es.wind.data(:,i) = wind.update(t_i,measurements);
    end
    if run_wind_ta
        es.wind_ta.data(:,i) = wind_ta.update(t_i,measurements);
    end
    if run_wind_aug
        x_ta = wind_aug.update(t_i,measurements);
        es.wind_aug.data(:,i) = x_ta;
    end
    if run_wind_opt
        wind_opt_ret = wind_opt.update(t_i,measurements);
        if ~isempty(wind_opt_ret)
            es.wind_opt.data(:,i) = wind_opt_ret;
            es.wind_opt.received(i) = 1;
        end
    end
    if run_wind_3sf
        ret_3sf1 = wind_3sf1.update(t_i,measurements);
        es.wind_3sf1.data(:,i) = ret_3sf1.x;
        measurements.x_3sf1 = ret_3sf1.x;
        measurements.C_3sf1 = ret_3sf1.C;
        measurements.y_3sf1 = ret_3sf1.y;
        x_3sf2 = wind_3sf2.update(t_i,measurements);
        es.wind_3sf2.data(:,i) = x_3sf2;
        measurements.x_3sf2 = x_3sf2;
        es.wind_3sf3.data(:,i) = wind_3sf3.update(t_i,measurements);
    end
%     if run_wind_adp && ~isempty(measurements.acc.data) && ~isempty(measurements.ars.data)
%         measurements.acc_hat.data = measurements.acc.data - es.mekf.data(7:9,i);
%         measurements.ars_hat.data = measurements.ars.data - es.mekf.data(14:16,i);
%         es.wind_adp.data(:,i) = wind_adp.update(t_i,measurements);
%     end
    if run_mekf_rvel
        es.mekf_rvel.data(:,i) = mekf_rvel.update(t_i,measurements);
    end
end

%% --- Saving ---
t = ss.pos.t(1:N);
ss_bacc.t       = t;
ss_bacc.data    = bias_acc*ones(1,N);
ss_bars.t       = t;
ss_bars.data    = bias_ars*ones(1,N);

ss_gamma.t = t;
ss_gamma.data = gamma*ones(1,N);

%MEKF
mekf_pos.t = t;
mekf_pos.data = es.mekf.data(1:3,:);
mekf_vel.t = t;
mekf_vel.data = es.mekf.data(4:6,:);
mekf_bacc.t = t;
mekf_bacc.data = es.mekf.data(7:9,:);
mekf_ea.t       = t;
mekf_ea.data    = zeros(3,N);
for i = 1:N
    Rtemp = Rquat(es.mekf.data(10:13,i));
    [a1,a2,a3] = R2euler(Rtemp);
    mekf_ea.data(:,i) = [a1;a2;a3];
end
mekf_bars.t = t;
mekf_bars.data = es.mekf.data(14:16,:);

%Wind EKF
wind_vnw.t = t;
wind_vnw.data = es.wind.data(1:3,:);
wind_gamma.t = t;
wind_gamma.data = es.wind.data(4,:);

%Wind OPT
wind_opt_vnw.t = t;
wind_opt_vnw.data = es.wind_opt.data(1:3,:);
wind_opt_gamma.t = t;
wind_opt_gamma.data = es.wind_opt.data(4,:);

%Wind TA
wind_ta_vnw = 0;
wind_ta_gamma = 0;
% wind_ta_vnw.t = t;
% wind_ta_vnw.data = es.wind_ta.data(1:3,:);
% wind_ta_gamma.t = t;
% wind_ta_gamma.data = 1./es.wind_ta.data(4,:);

%Wind AUG
wind_aug_vnw.t = t;
wind_aug_vnw.data = es.wind_aug.data(1:3,:);
wind_aug_gamma.t = t;
wind_aug_gamma.data = es.wind_aug.data(4,:);
wind_aug_vnorm.t = t;
wind_aug_vnorm.data = es.wind_aug.data(5,:);

%Wind 3SF1
wind_3sf1_vnw.t = t;
wind_3sf1_vnw.data = es.wind_3sf1.data(1:3,:);
wind_3sf1_gamma.t = t;
wind_3sf1_gamma.data = es.wind_3sf1.data(4,:);

%Wind 3SF2
wind_3sf2_vnw.t = t;
wind_3sf2_vnw.data = es.wind_3sf2.data(1:3,:);
wind_3sf2_gamma.t = t;
wind_3sf2_gamma.data = es.wind_3sf2.data(4,:);

%Wind 3SF3
wind_3sf3_vnw.t = t;
wind_3sf3_vnw.data = es.wind_3sf3.data(1:3,:);
wind_3sf3_gamma.t = t;
wind_3sf3_gamma.data = es.wind_3sf3.data(4,:);

% %Wind ADP
% wind_adp_aoa.t = t;
% wind_adp_aoa.data = es.wind_adp.data(1,:);
% wind_adp_ssa.t = t;
% wind_adp_ssa.data = es.wind_adp.data(2,:);
% wind_adp_airspeed.t = t;
% wind_adp_airspeed.data = es.wind_adp.data(3,:);
% wind_adp_aoa.t = t;
% wind_adp_aoa.data = es.wind_adp.data(1:3,:);
% wind_adp_gamma.t = t;
% wind_adp_gamma.data = es.wind_adp.data(4,:);

%Wind RVEL
mekf_rvel_pos.t = t;
mekf_rvel_pos.data = es.mekf_rvel.data(1:3,:);
mekf_rvel_rvel.t = t;
mekf_rvel_rvel.data = es.mekf_rvel.data(4:6,:);
mekf_rvel_bacc.t = t;
mekf_rvel_bacc.data = es.mekf_rvel.data(7:9,:);
mekf_rvel_ea.t       = t;
mekf_rvel_ea.data    = zeros(3,N);
mekf_rvel_bars.t = t;
mekf_rvel_bars.data = es.mekf_rvel.data(14:16,:);
mekf_rvel_vnw.t = t;
mekf_rvel_vnw.data = es.mekf_rvel.data(17:19,:);
mekf_rvel_gamma.t = t;
mekf_rvel_gamma.data = es.mekf_rvel.data(20,:);
rvel_vel = zeros(3,N);
rvel_aoa = zeros(1,N);
rvel_ssa = zeros(1,N);
for i = 1:N
    Rtemp = Rquat(es.mekf_rvel.data(10:13,i));
    [a1,a2,a3] = R2euler(Rtemp);
    mekf_rvel_ea.data(:,i) = [a1;a2;a3];
    
    rvel_vel(:,i) = mekf_rvel_vnw.data(:,i) + Rtemp*mekf_rvel_rvel.data(:,i);
    rvel_aoa(i) = atan2(mekf_rvel_rvel.data(3,i),mekf_rvel_rvel.data(1,i));
    rvel_ssa(i) = asin(mekf_rvel_rvel.data(2,i)/norm(mekf_rvel_rvel.data(:,i)));
end
mekf_rvel_aoa.t = t;
mekf_rvel_aoa.data = rvel_aoa*RAD2DEG;
mekf_rvel_ssa.t = t;
mekf_rvel_ssa.data = rvel_ssa*RAD2DEG;
mekf_rvel_vel.t = t;
mekf_rvel_vel.data = rvel_vel;

%MAKE PDs
pd.pos = {ss.pos,mekf_pos,mekf_rvel_pos};
pd.vel = {ss.vel,mekf_vel,mekf_rvel_vel};
pd.b_acc = {ss_bacc,mekf_bacc,mekf_rvel_bacc};
pd.ea = {ss.ea,mekf_ea,mekf_rvel_ea};
pd.b_ars = {ss_bars,mekf_bars,mekf_rvel_bars};
ed = pd2ed(pd);

pd_wind.v_nw = {v_nw,wind_vnw,wind_ta_vnw,wind_aug_vnw,wind_opt_vnw,wind_3sf1_vnw,wind_3sf2_vnw,wind_3sf3_vnw,mekf_rvel_vel};
pd_wind.gamma = {ss_gamma,wind_gamma,wind_ta_gamma,wind_aug_gamma,wind_opt_gamma,wind_3sf1_gamma,wind_3sf2_gamma,wind_3sf3_gamma,mekf_rvel_vel};

%% --- Plotting states of MEKF ---
plotinput = make_plotinput();
plotinput.savesuffix = sprintf('-%i',Nantennas);
plotter(pd,plotinput);

%% --- Plotting errors of MEKF ---
plotinput.figpos = [810,1440,800,500];
% plotter(ed,plotinput);

%% --- Plotting states of wind estimator ---
plotinput = make_plotinput();
plotinput.legends = {'True', 'Wind EKF','Wind TA','Wind AUG','Wind OPT','Wind 3SF1','Wind 3SF2','Wind 3SF3'};
plotinput.ylabels = {{'N [$m/s$]','E [$m/s$]','D [$m/s$]'},{'Gamma'}};
plotinput.labels  = {'Wind Velocity','Gamma'};
plotinput.figpos = [0,0,800,500];
plotinput.do_save = 0;
plotinput.linewidth = 2;
plotinput.savesuffix = sprintf('-%i',Nantennas);
plotter(pd_wind,plotinput);


%%
velsqr = zeros(1,N);
for i = 1:N
    velsqr(i) = norm(wind_aug_vnw.data(:,i))^2;
end

figure;plot(es.wind_aug.data(5,:),'r'); hold on;plot(velsqr,'b')
legend('est norm','calc norm')




