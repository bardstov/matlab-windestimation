classdef class_wind_3sf3 < handle
    %EKF Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        %states
        x;
        
        %air density
        rho
        
        %Covariance matrices
        Q;
        P;
        R;
        
        %Identity matrix of appropriate size
        I;
        
        %in order to calculate time step
        prev_time;
        
        %enables delayed starting
        start_time = 0;
        begun = 0;
    end
    
    methods
        function kf = class_wind_3sf3(input)
            kf.x                        = input.x0;
            kf.P                        = input.P0;
            kf.Q                        = input.Q;
            kf.I                        = eye(size(input.P0));
            kf.R                        = input.R;
            kf.rho                      = input.rho;
            if isfield(input,'start_time')
                kf.start_time = input.start_time;
            end
        end
        function x = update(kf,time,measurements)
            dp   = measurements.dp.data;
            v_nb = measurements.velocity.data;
            if isempty(dp)
                x = kf.x;
                return
            end
%             if ~kf.begun && sum(measurements.x_3sf2 == [0;0;0;1]) == 4
%                 x = [kf.x(1:3);sqrt(kf.x(4))];
%                 return
%             else
%                 kf.begun = 1;
%             end
            
%             if kf.start_time ~= -Inf && time < kf.start_time
%                 x = [kf.x(1:3);sqrt(kf.x(4))];
%                 return
%             else
%                 kf.start_time = -Inf;
%             end
            
            %find dt
            dt = min([1,time-kf.prev_time]);
            kf.prev_time=time;
            
            %cov time update
            [Phi,Qd] = van_loan(zeros(4),kf.Q,dt);
%             Phi
%             Gamma
            kf.P = Phi*kf.P*Phi' + Qd;
            kf.P = .5*(kf.P+kf.P');
            
            %no state update
            
            % --- KF update ---
            y = dp;
            %retrieve exogenous estimates
            v_nw_bar = measurements.x_3sf2(1:3);
            gamma_bar = measurements.x_3sf2(4);
            x_bar = [v_nw_bar;gamma_bar];
            %linearize
            rvel_bar = v_nb-v_nw_bar;
            
            h = .5*kf.rho*gamma_bar*(rvel_bar'*rvel_bar);
            
            H_v = -kf.rho*gamma_bar*rvel_bar';
            H_gamma = .5*kf.rho*(rvel_bar'*rvel_bar); %x2 = gamma
            
            H = [H_v,H_gamma];
            %estimated measurement
            y_hat = h + H*(kf.x-x_bar);
            %KF update
            K = kf.P*H'/(H*kf.P*H'+kf.R);
            kf.P = (kf.I-K*H)*kf.P*(kf.I-K*H)'+K*kf.R*K';
            kf.P = (kf.P+kf.P')/2;
            ytilde = y-y_hat;
            xtilde = K*ytilde;

            kf.x = kf.x + xtilde;
            x = kf.x;
        end
    end
end

