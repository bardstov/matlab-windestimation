classdef Master_offline < handle
    %MASTER Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        h;
        M;
        N;
        filters;
        n_filters;
        sim_outputs;
        sim_outputs_val;
        n_sim_outputs;
        outputs;
        io_map;
        sim;
        sensor_select;
        save_estimate;
        current_filter_n;
        save_names;
        n_save;
        saved_data;
        save_map;
        t;
        counter;
    end
    
    methods
        function m = Master_offline(input)
            m.filters = input.filters;
            m.sim = input.sim;
            m.t = input.t;
            m.N = length(input.t);
            m.n_filters = length(m.filters);
            m.sensor_select = cell(m.n_filters,1);
            for j = 1:m.n_filters
                for k = 1:length(m.filters{j}.sensors)
                    for l = 1:m.sim.n_sensors
                        s1 = m.sim.sensors{l}.tag;
                        s2 = m.filters{j}.sensors{k};
                        if length(s1)==length(s2) && strcmp(s1,s2)
                            m.sensor_select{j} = [m.sensor_select{j} l];
                        end
                    end
                end
            end
            
            m.sim_outputs = input.sim_outputs;
            m.n_sim_outputs = length(m.sim_outputs);
                
%             if(isfield(input,'monte_carlo'))
%                 if(input.monte_carlo)
%                     m.sim.measurements_available = [];
%                     m.sim.measurements_value = [];
%                 else
%                     m.gen_output_vals();
%                 end
%             else
%                 m.gen_output_vals();
%             end

            m.outputs = cell(m.n_sim_outputs+m.n_filters,1);
            m.io_map = cell(m.n_filters,1);
            output_map = cell(m.n_sim_outputs+m.n_filters,1);
            for j = 1:m.n_filters
                output_map{j} = m.filters{j}.output_names;
            end
            for k = 1:m.n_sim_outputs
                output_map{m.n_filters+k} = m.sim_outputs{k}.output_names;
            end
            for j = 1:m.n_filters % For all filters j
                for i = 1:m.filters{j}.n_inputs % for all inputs i to filter j
                    for k = 1:m.n_filters+m.n_sim_outputs % for all possible "outputters" k
                        for l = 1:length(output_map{k}) % for all outputs l from outputter k
                            s1 = m.filters{j}.input_names{i};
                            s2 = output_map{k}{l};
                            if length(s1)==length(s2) && strcmp(s1,s2) % relate output l from outputter k to input i of filter j
%                                 if j == 7
%                                     s1=s1
%                                     s2=s2
%                                 end
                                m.io_map{j} = [m.io_map{j};[k,l]];
                            end
                        end
                    end
                end
            end
            
            m.n_save = length(input.save_names);
            for l = 1:m.n_save
                m.save_names{l} = input.save_names{l}{2};
            end
            
            m.saved_data = cell(m.n_save,1);
            for l = 1:m.n_save
                m.saved_data{l}.t = m.t;
                m.saved_data{l}.data = zeros(input.save_names{l}{1},m.N);
            end
            m.save_map = zeros(m.n_save,2);
            for i = 1:m.n_save
                for k = 1:m.n_filters+m.n_sim_outputs
                    for l = 1:length(output_map{k})
                        s1 = m.save_names{i};
                        s2 = output_map{k}{l};
                        if length(s1)==length(s2) && strcmp(s1,s2)
                            m.save_map(i,:) = [k,l];
                        end
                    end
                end
            end
%             m.save_map
%             for j = 1:m.n_filters
%                 for l = 1:length(m.filters{j}.
%             end
            
            %gather all inputs
            %map outputs to inputs
            
%             for j = 1:m.n_filters
%                 m.save_estimate{j}.data = zeros(length(m.filters{j}.x0),m.filters{j}.N+1);
%                 m.save_estimate{j}.data(:,1) = m.filters{j}.x0;
%                 m.save_estimate{j}.t = zeros(1,m.filters{j}.N+1);
%                 m.filters{j}.update_interval = m.filters{j}.h/m.h;
%             end
            m.current_filter_n = ones(m.n_filters,1);
%             m.save_true = zeros(length(m.sim.x0)+1,m.N);

        end
        
        function run(m)
            timer_id = tic;
%             start_time = toc(timer_id);
            % Save initial values
            for j = 1:m.n_filters
                m.save_estimate{j}.data(:,1) =  m.filters{j}.x0;
                m.save_estimate{j}.t(:,1) = 0;%toc(timer_id)-start_time;
            end
            
            
            % Run
            prev_percentage = -1;
            L = length(m.t);
            for i = 1:L
                m.counter = i;
%                 ['-----------',num2str(i),'--------------']
                [measurement_available, measurement_value] = m.sim.get_available_measurements(m.t(i));
%                 measurement_available = m.sim.measurements_available(:,i);
%                 measurement_value = m.sim.measurements_value(:,i);
%                 measurement_available
%                 measurement_value{1}
                for k = 1:m.n_sim_outputs
%                     m.outputs{m.n_filters+k} = m.sim_outputs_val{k}{i};%(:,i);%
                    m.outputs{m.n_filters+k} = m.sim_outputs{k}.update(m.sim);%
                end
                for j = 1:m.n_filters
                    measurements = cell(m.filters{j}.n_sensors,2);
                    for k = 1:m.filters{j}.n_sensors
                        index = m.sensor_select{j}(k);
                        measurements{k,1} = measurement_available(index);
                        measurements{k,2} = measurement_value{index};
                    end
%                     if ~mod(i,m.filters{j}.update_interval)
%                         m.save_estimate{j}.data(:,m.current_filter_n(j)+1) = m.filters{j}.update(measurements);
%                         m.save_estimate{j}.t(m.current_filter_n(j)+1) = i*m.h;%toc(timer_id)-start_time;
%                         m.current_filter_n(j) = m.current_filter_n(j) + 1;
%                     end
                    input = cell(m.filters{j}.n_inputs,1);
                    for l = 1:m.filters{j}.n_inputs
%                         filter=m.filters{j}.tag
%                         j
%                         m.io_map
%                         'before'
%                         l
%                         inputname = m.filters{j}.input_names{l}
%                         'mid'
%                         iomapj = m.io_map{j}
%                         outputs=m.outputs
%                         l1 = m.io_map{j}(l,1)
%                         l2 = m.io_map{j}(l,2)
%                         m.outputs{3}
%                         m.outputs{m.io_map{j}(l,1)}{m.io_map{j}(l,2)}
%                         'after'
%                         input{l}
                        input{l} = m.outputs{m.io_map{j}(l,1)}{m.io_map{j}(l,2)};
                    end
                    m.outputs{j} = m.filters{j}.update(measurements,input);
                end
                for l = 1:m.n_save
                    m.saved_data{l}.t(i) = m.t(i);
%                     m.saved_data{l}
%                     size(m.saved_data{l}.data(:,i))
%                     l
%                     m.outputs{m.save_map(l,1)}
%                     m.outputs{m.save_map(l,1)}{m.save_map(l,2)}
%                     size(m.outputs{m.save_map(l,1)}{m.save_map(l,2)})
                    m.saved_data{l}.data(:,i) = m.outputs{m.save_map(l,1)}{m.save_map(l,2)};
                end
                
                m.sim.update(i);
                if(floor((i-1)/L*100)> prev_percentage)
                    prev_percentage = floor(i/L*100);
                    disp(['---- ',num2str(prev_percentage),'% ---- Real time: ', num2str(toc(timer_id)),' ---- Sim time: ', num2str(m.t(i)),' ----']);
                end
            end
            toc(timer_id)
        end
        
%         function data = get_save_struct(m)
%             data = m.sim.get_save_struct();
%             data.filter_data = m.save_estimate;
%         end
        
%         function sim_data = sim_init(m)
%         end

        function gen_output_vals(m)
            m.sim_outputs_val = cell(m.n_sim_outputs,1);
            for i=1:m.n_sim_outputs
                m.sim_outputs_val{i} = m.sim_outputs{i}.get_full_run(m.sim);
            end
        end
    end
end
