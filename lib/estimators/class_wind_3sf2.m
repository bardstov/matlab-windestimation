classdef class_wind_3sf2 < handle
    %EKF Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        %states
        x
        
        %air density
        rho
        
        %Covariance matrices
        Q;
        P;
        R;
        
        %Identity matrix of appropriate size
        I;
        
        %in order to calculate time step
        prev_time;
        
        %enables delayed starting
        start_time = 0;
    end
    
    methods
        function kf = class_wind_3sf2(input)
            x0                          = input.x0;
            kf.x                        = x0;
            kf.P                        = input.P0;
            kf.Q                        = input.Q;
            kf.I                        = eye(size(input.P0));
            kf.R                        = input.R;
            kf.rho                      = input.rho;
            if isfield(input,'start_time')
                kf.start_time = input.start_time;
            end
        end
        function x = update(kf,time,measurements)
            C = measurements.C_3sf1;
            y = measurements.y_3sf1;
            if isempty(C)
                x = [kf.x(1:3);1/kf.x(4)];
                return
            end
            
            if kf.start_time ~= -Inf && time < kf.start_time
                x = [kf.x(1:3);1/kf.x(4)];
                return
            else
                kf.start_time = -Inf;
            end
            
            %find dt
            dt = min([.1,time-kf.prev_time]);
            kf.prev_time=time;
            
            %cov time update
            [Phi,Qd] = van_loan(zeros(4),kf.Q,dt);
%             Phi
%             Gamma
            kf.P = Phi*kf.P*Phi' + Qd;
            kf.P = .5*(kf.P+kf.P');
            
%             %cov time update
%             kf.P = kf.P + kf.Q*dt;
%             kf.P = .5*(kf.P+kf.P');
            
            %no state update
            
            % --- KF update ---
            Ri = kf.R*eye(length(y));
            y_hat = C*kf.x;
            K = kf.P*C'/(C*kf.P*C'+Ri);
            kf.P = (kf.I-K*C)*kf.P*(kf.I-K*C)'+K*Ri*K';
            kf.P = (kf.P+kf.P')/2;
            ytilde = y-y_hat;
            xtilde = K*ytilde;

            kf.x = kf.x + xtilde;

            x = [kf.x(1:3);1/kf.x(4)]; 
        end
    end
end

