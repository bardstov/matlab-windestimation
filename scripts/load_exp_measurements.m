% if ~exist('data_loaded') || data_loaded == 0
    load_data
% end
addlib
%% Measurement
do_plot_measurements = 0;

%IMUs
fs_pass = 20;

mag_n = [.134, 9e-3,.478]';

use_stim = 0;
use_adis = 1;
use_px = 0;
F_resample = 0;
clearvars ars_input acc_input mag_input;
dt = 0.01;
imu_t = max([adis.ars.t(1),adis.acc.t(1)]):dt:min([adis.ars.t(end),adis.acc.t(end)]);
if use_stim
    fs = 500;
    ars_input.data = lowpass(stim.ars.data',fs_pass,fs)';
    ars_input.t = stim.ars.t;
    ars_input.tag = 'ars';
    ars_input.t_resample = imu_t;
    ars = Measurement(ars_input);
    %ACC
%     acc_input.data = px.acc.data;%lowpass(adis.acc.values',fs_pass,fs);
%     acc_input.t = px.acc.t;%adis.acc.tot;
    acc_input.data = lowpass(stim.acc.data',fs_pass,fs)';
    acc_input.t = stim.acc.t;
    acc_input.tag = 'acc';
    acc_input.t_resample = imu_t;
    acc = Measurement(acc_input);
elseif use_adis
    fs = 250;
%     magcorr = [.06;.05;-.125];
    %ARS
%     ars_input.data = px.ars.data;%lowpass(adis.ars.values',fs_pass,fs);
%     ars_input.t = px.ars.t;%adis.ars.tot;
    ars_input.data = lowpass(adis.ars.data',fs_pass,fs)';
    ars_input.t = adis.ars.t;
    ars_input.tag = 'ars';
    ars_input.t_resample = imu_t;
    ars = Measurement(ars_input);
    %ACC
%     acc_input.data = px.acc.data;%lowpass(adis.acc.values',fs_pass,fs);
%     acc_input.t = px.acc.t;%adis.acc.tot;
    acc_input.data = lowpass(adis.acc.data',fs_pass,fs)';
    acc_input.t = adis.acc.t;
    acc_input.tag = 'acc';
    acc_input.t_resample = imu_t;
    acc = Measurement(acc_input);
    %MAG
%     mag_input.data = px.mag.data;%lowpass(adis.mag.values',fs_pass,fs);
%     mag_input.t = px.mag.t;%adis.mag.tot;
    mag_input.data = lowpass(adis.mag.data',fs_pass,fs)';
    mag_input.t = adis.mag.t;
    mag_input.tag = 'mag';
    mag_input.t_resample = imu_t;
    mag = Measurement(mag_input);
elseif use_px
    %ARS
    ars_input.data = px.ars.data;%lowpass(adis.ars.values',fs_pass,fs);
    ars_input.t = px.ars.t;%adis.ars.tot;
    ars_input.tag = 'ars';
    ars = Measurement(ars_input);
    %ACC
    acc_input.data = px.acc.data;%lowpass(adis.acc.values',fs_pass,fs);
    acc_input.t = px.acc.t;%adis.acc.tot;
    acc_input.tag = 'acc';
    acc = Measurement(acc_input);
    %MAG
    mag_input.data = px.mag.data;%lowpass(adis.mag.values',fs_pass,fs);
    mag_input.t = px.mag.t;%adis.mag.tot;
    mag_input.tag = 'mag';
    mag = Measurement(mag_input);
end

if do_plot_measurements 
    %ARS
    figure;
    for i = 1:3
        subplot(3,1,i)
        hold on
        plot(ars.t,ars.data(i,:),clrs(1))
        plot(px.ars.t,px.ars.data(i,:),clrs(3))
%         plot(px.ars2.t,px.ars2.data(i,:),clrs(4))
    end
    suplabel('ARS','t');
    legend('ars','px 1','px 2');
    %ACC
    figure;
    for i = 1:3
        subplot(3,1,i)
        hold on
        plot(acc.t,acc.data(i,:),clrs(1))
        plot(px.acc.t,px.acc.data(i,:),clrs(3))
%         plot(px.acc2.t,px.acc2.data(i,:),clrs(4))
    end
    suplabel('ACC','t')
    legend('acc','px 1','px 2');
%     %MAG
%     figure;
%     for i = 1:3
%         subplot(3,1,i)
%         hold on
%         plot(mag.t,mag.data(i,:),clrs(1))
%         plot(px.mag.t,px.mag.data(i,:),clrs(3))
% %         plot(px.mag2.t,px.mag2.data(i,:),clrs(4))
%     end
%     suplabel('MAG','t')
%     legend('mag f','px 1','px 2');
end

%GPS
t_gps_offset    = 0;
p_arm1          = [  0.347; -0.010; -0.045];
p_arm2          = [ -0.226;  0.904;  0.000];
p_arm3          = [ -0.285; -0.894;  0.005];

if ~exist('gpstimeshift')
    disp('no gpstimeshift exists')
    gpstimeshift = 0;
    idt = 1;
end
    

gps1_input.t                = gps.pos.t-t_gps_offset-gpstimeshift(idt);
gps1_input.data             = gps.pos.data;
gps1_input.tag              = 'gps1';
gps1_input.add_info.p_arm   = p_arm1;
gps1                        = Measurement(gps1_input);

gps2_input.t                = gps.pos2.t-t_gps_offset-gpstimeshift(idt);
gps2_input.data             = gps.pos2.data;
gps2_input.tag              = 'gps2';
gps2_input.add_info.p_arm   = p_arm2;
gps2                        = Measurement(gps2_input);

gps3_input.t                = gps.pos3.t-t_gps_offset-gpstimeshift(idt);
gps3_input.data             = gps.pos3.data;
gps3_input.tag              = 'gps3';
gps3_input.add_info.parm    = p_arm3;
gps3                        = Measurement(gps3_input);

gpsvel1_input.t                = gps.vel.t-gpstimeshift(idt);
gpsvel1_input.data             = gps.vel.data;
gpsvel1_input.tag              = 'gpsvel1';
gpsvel1_input.add_info.p_arm   = p_arm1;
gpsvel1                        = Measurement(gpsvel1_input);

gpsvel2_input.t                = gps.vel2.t-gpstimeshift(idt);
gpsvel2_input.data             = gps.vel2.data;
gpsvel2_input.tag              = 'gpsvel2';
gpsvel2_input.add_info.p_arm   = p_arm2;
gpsvel2                        = Measurement(gpsvel2_input);

%Pitot tube
pitot_input = px.airspeed;
pitot_input.tag = 'pitot';
pitot = Measurement(pitot_input);
%Dynamic pressure
rho = 1.225;
dp_input = px.pd;
dp_input.data = dp_input.data;
dp_input.tag = 'dp';
dp = Measurement(dp_input);
%Aeroprobe
adp_input.data = [ap.aoa.data;ap.ssa.data;ap.airspeed.data];
adp_input.tag = 'aeroprobe';
adp_input.t = ap.aoa.t;
adp = Measurement(adp_input);
%ACC
acc_adp_input.data = [interp1(acc_input.t,acc_input.data(1,:),adp_input.t)';
                      interp1(acc_input.t,acc_input.data(2,:),adp_input.t)';
                      interp1(acc_input.t,acc_input.data(3,:),adp_input.t)'];
acc_adp_input.t = adp_input.t;
acc_adp_input.tag = 'acc_adp';
acc_adp = Measurement(acc_adp_input);
%ARS
ars_adp_input.data = [interp1(ars_input.t,ars_input.data(1,:),adp_input.t)';
                      interp1(ars_input.t,ars_input.data(2,:),adp_input.t)';
                      interp1(ars_input.t,ars_input.data(3,:),adp_input.t)'];
ars_adp_input.t = adp_input.t;
ars_adp_input.tag = 'ars_adp';
ars_adp = Measurement(ars_adp_input);
%Elevator
ttemp = px.ele.t;
datatemp = px.ele.data;
elevator_input.t = ars.t;
elevator_input.data = interp1(ttemp,datatemp,elevator_input.t);
elevator_input.tag = 'elevator';
elevator = Measurement(elevator_input);
%% Inputs
%G_N
g_input.output_names = {'g_n'};
g_input.data = [0;0;9.81];
g_n = Input(g_input);

%Time
time = Time();