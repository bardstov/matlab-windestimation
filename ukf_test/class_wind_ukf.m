classdef class_wind_ukf < handle
    %EKF Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        %state
        x;
        
        %air density
        rho;
        
        %tuning parameters
        alpha=1e-3;                                 %default, tunable
        ki=0;                                       %default, tunable
        beta=2;                                     %default, tunable
        
        %Covariance matrices
        Q;
        P;
        R;
        
        %Stores previous time instance
        prev_time = 0;
        
        %Stores previous acc and ars measururements
        prev_acc=zeros(3,1);
        prev_ars=zeros(3,1);
        
        %Identity matrix of n x n
        I;
        
        %gravity
        g = [0;0;9.81];
        
        %time step
        time_step;
    end
    
    methods
        function kf = class_wind_ukf(input)
            kf.x                           = input.x0;
            kf.P                        = input.P0;
            kf.Q                        = input.Q;
            kf.R                        = input.R;
            if isfield(input,'g')
                kf.g = input.g;
            end
            if isfield(input,'time_step')
                kf.time_step = input.time_step;
            end
%             Rnb = Rquat(input.q0);
%             kf.prev_acc = -[0;0;9.81];
            
            
            if isfield(input,'alpha')
                kf.alpha                    = input.alpha;
            end
            if isfield(input,'beta')
                kf.beta                     = input.beta;
            end
            if isfield(input,'ki')
                kf.ki                       = input.ki;
            end
        end
%         function x = update(kf,measurements,input)
        function [x,P] = update(kf,time,measurements)
            z = [measurements.aeroprobe.data];
            L = numel(kf.x);                                %numer of states
            m = numel(z);                                   %numer of measurements
            kf.alpha = 1e-3;                                %default, tunable
            kf.ki = 0;                                      %default, tunable
            kf.beta = 2;                                    %default, tunable
            lambda = kf.alpha^2*(L+kf.ki) - L;              %scaling factor
            c = L + lambda;                                 %scaling factor
            Wm = [lambda/c, 0.5/c+zeros(1,2*L)];            %weights for means
            Wc = Wm;
            Wc(1) = Wc(1) + (1-kf.alpha^2+kf.beta);         %weights for covariance
            c = sqrt(c);
            X = kf.sigmas(c);                               %sigma points around x
            inps.acc = measurements.acc_adp;
            inps.omg = measurements.ars_adp;
            inps.ea = measurements.ea_inp;
            inps.g = kf.g(3);
            inps.dt = min([0,time-kf.prev_time]);
            fstate = @(x) time_update(x,inps);
            [x1,X1,P1,X2] = kf.ut(fstate,X,Wm,Wc,L,kf.Q);      %unscented transformation of process
            hmeas = @(x) measurement_update(x);
            [z1,Z1,P2,Z2] = kf.ut(hmeas,X1,Wm,Wc,m,kf.R);      %unscented transformation of measurments
            P12 = X2*diag(Wc)*Z2';                          %transformed cross-covariance
            K = P12*pinv(P2);
            x = x1+K*(z-z1);                                %state update
            P = P1-K*P12';                                  %covariance update
            
            kf.prev_time = time;
        end
%         function [x,P] = time_update()
%             L = numel(kf.x);                                 %numer of states
%             m = numel(z);                                 %numer of measurements
%             lambda = kf.alpha^2*(L + kf.ki) - L;                    %scaling factor
%             c = L + lambda;                                 %scaling factor
%             Wm = [lambda/c, 0.5/c+zeros(1,2*L)];           %weights for means
%             Wc = Wm;
%             Wc(1) = Wc(1) + (1-kf.alpha^2+kf.beta);               %weights for covariance
%             c = sqrt(c);
%             X = sigmas(kf.x,kf.P,c);                            %sigma points around x
%             [x1, X1, P1, X2] = ut(fstate,X,Wm,Wc,L,Q);          %unscented transformation of process
%         end
%         function measurement_update()
%             L = numel(x);                                 %numer of states
%             m = numel(z);                                 %numer of measurements
%             lambda = kf.alpha^2 * (L + kf.ki) - L;        %scaling factor
%             c = L + lambda;                               %scaling factor
%             Wm = [lambda/c, 0.5/c+zeros(1,2*L)];          %weights for means
%             Wc = Wm;
%             Wc(1) = Wc(1) + (1-kf.alpha^2+kf.beta);       %weights for covariance
%             c = sqrt(c);
%             X = sigmas(x,P,c);                            %sigma points around x
%             [z1, Z1, P2, Z2] = ut(hmeas,X1,Wm,Wc,m,kf.R); %unscented transformation of measurments
%             P12 = X2*diag(Wc)*Z2';                        %transformed cross-covariance
%             K = P12*pinv(P2);
%             x = x1 + K*(z-z1);                              %state update
%             P = P1 - K*P12';                                %covariance update
%         end
        function [y,Y,P,Y1]=ut(~,f,X,Wm,Wc,n,Cov)
            %Unscented Transformation
            %Input:
            %        f: nonlinear map
            %        X: sigma points
            %       Wm: weights for mean
            %       Wc: weights for covraiance
            %        n: numer of outputs of f
            %        R: additive covariance
            %Output:
            %        y: transformed mean
            %        Y: transformed smapling points
            %        P: transformed covariance
            %       Y1: transformed deviations
            L = size(X,2);
            y = zeros(n,1);
            Y = zeros(n,L);
            for k=1:L                   
                Y(:,k) = f(X(:,k));       
                y = y + Wm(k)*Y(:,k);       
            end
            Y1 = Y - y(:,ones(1,L));
            P = Y1*diag(Wc)*Y1' + Cov;
        end
        function X=sigmas(kf,c)
            %Sigma points around reference point
            %Inputs:
            %       x: reference point
            %       P: covariance
            %       c: coefficient
            %Output:
            %       X: Sigma points

            A = c*chol(kf.P)';
            Y = kf.x(:,ones(1,numel(kf.x)));
            X = [kf.x Y+A Y-A]; 
        end
    end
end

