close all

addpath('../lib')
addpath('..')

load_exp_measurements
Nantennas = 1;

DEG2RAD = pi/180;
RAD2DEG = 1/DEG2RAD;

t_start = px.acc.t(1);
t_end = 2700;

time_vector = sort(unique([ars.t,acc.t]));
ind = find(time_vector > t_start); ind = ind(1);
time_vector = time_vector(ind:end);
t_end = min([time_vector(end),t_end]);
ind = find(time_vector <= t_end); ind = ind(end);
time_vector = time_vector(1:ind);
N = length(time_vector);
t = time_vector;
%% Filters
std_ars     = 5e-3;
std_acc     = 5e-2;
std_mag     = 1e-3;
std_gps     = 5e-2;
std_gpsvel  = 1e-1;
std_pitot   = .5;
std_dp      = 5;

pxind = find(px.ea.t > t_start); pxind = pxind(1);
gpsind = find(gps.pos.t > t_start); gpsind = gpsind(1);
% MEKF
ang                     = px.ea.data(:,pxind);
p0                      = gps.pos.data(:,1);
v0                      = px.vel.data(:,pxind);
b_acc0                  = [0;0;0];
q0                      = euler2q(ang(1),ang(2),ang(3));
b_ars0                  = [0,0,0]';
x0                      = [p0;v0;b_acc0;q0;b_ars0];

%% --- Filters ---
I3 = eye(3);
%R matrices
R_gps    = I3*std_gps^2;
R_gpsvel = I3*std_gpsvel^2;
R_mag    = I3*std_mag^2;
%Q matrices
Q_acc   = I3*std_acc^2;
Q_ars   = I3*std_ars^2;
% if Nantennas == 1
%     Q_bacc  = I3*1e-6;
%     Q_bars  = I3*1e-8;
% elseif Nantennas == 2
%     Q_bacc  = I3*1e-6;
%     Q_bars  = I3*1e-8;
% elseif Nantennas == 3
%     Q_bacc  = I3*1e-8;
%     Q_bars  = I3*1e-10;
% end
Q_bacc  = I3*1e-7;
Q_bars  = I3*1e-9;
Q_bmag   = I3*1e-11;
Q       = blkdiag(Q_acc,Q_bacc,Q_ars,Q_bars,Q_bmag);
%P0 matrices
P0_p    = I3*20;
P0_v    = I3*2;
P0_ub   = I3*5e-1;

P0_bars = I3*1e-9;
P0_bacc = I3*1e-9;
P0_bmag = I3*13-9;
P0      = blkdiag(P0_p,P0_v,P0_bacc,P0_ub,P0_bars,P0_bmag);

%create mekf
mekfinput.P0                    = P0;
mekfinput.x0                    = x0;
mekfinput.Q                     = Q;
mekfinput.R_gps                 = R_gps;
mekfinput.R_gpsvel              = R_gpsvel;
mekfinput.R_mag                 = R_mag;
mekfinput.Nantennas             = Nantennas;
mekfinput.max_P_update_interval = 1;
mekfinput.p_arm1                = p_arm1;
mekfinput.p_arm2                = p_arm2;
mekfinput.p_arm3                = p_arm3;
mekfinput.mag_n                 = mag_n;
mekf                            = class_mekf(mekfinput);

ukf_adp_input.x0                = [0;0;18];
ukf_adp_input.P0                = blkdiag(.1,.1,1);
ukf_adp_input.Q                 = blkdiag(1e-4,1e-4,.1);
ukf_adp_input.R                 = blkdiag(1e-1,1e-1,.3);
ukf_adp                         = class_wind_ukf(ukf_adp_input);

ukf_adp_async_input             = ukf_adp_input;
ukf_adp_async                   = class_wind_ukf_async(ukf_adp_async_input);
%% Which filters to run?
t_adp_inds = find(adp_input.t(find(adp_input.t <= t_end)) >= t_start);
t_adp = adp_input.t(t_adp_inds);

N_adp = length(t_adp);

run_all         = 0;
run_mekf        = 1 || run_all;
run_ukf         = 0 || run_all;
run_ukf_async   = 0 || run_all;

if run_mekf
    es.mekf.data = zeros(19,N);
end
if run_ukf_async
    es.ukf_async.data = zeros(3,N);
    es.ukf_async.P    = zeros(3,3,N);
end
if run_ukf
    es.ukf.data = zeros(3,N_adp);
    es.ukf.P    = zeros(3,3,N_adp);
end
%% --- Run Simulation ---
s='1';
tic
last_percent = 0;
mh = MeasurementHandler({acc,ars,gps1,gps2,gps3,pitot,dp,gpsvel1,gpsvel2,mag,adp},t_start);
for i = 1:N
    if i/(N/100) > last_percent
        s = num2str(last_percent);
        last_percent = last_percent + 1;
        fprintf('Done with %s%s: Time elapsed %.2f%s',s,'%',toc,char(10));
%         if i > 1
%             es.mekf_rvel.data(:,i-1)';
%         end
    end
    t_i = t(i);
    measurements = mh.get_measurements(t_i);
    %Filters
    if run_mekf
        es.mekf.data(:,i) = mekf.update(t_i,measurements);
        measurements.b_acc = es.mekf.data(7:9,i);
        measurements.b_ars = es.mekf.data(14:16,i);
        [a1,a2,a3] = q2euler(es.mekf.data(10:13,i));
        measurements.ea.data = [a1;a2;a3];
    end
    if run_ukf_async
        [es.ukf_async.data(:,i),es.ukf_async.P(:,:,i)] = ukf_adp_async.update(t_i,measurements);
    end
%     measurements.velocity.data = es.mekf.data(4:6,i);
%     [a1,a2,a3] = q2euler(es.mekf.data(10:13,i));
%     measurements.ea.data = [a1;a2;a3];
%     measurements.acc_hat.data = measurements.acc.data - es.mekf.data(7:9,i);
%     measurements.ars_hat.data = measurements.ars.data - es.mekf.data(14:16,i);
end
%%
es.ea.data = qs2eas(es.mekf.data(10:13,:));
es.ea.t = t;
es.ea.name = 'ea';

ea_inp.t = t_adp;
ea_inp.data = interpolateNd(es.ea.t,es.ea.data,ea_inp.t);
ea_inp.name = 'ea_inp';
ea_inp.tag = 'ea_inp';
splot({ea_inp,es.ea},1)

es.mekf.t = t;
es.ukf_async.t = t;
es.ukf.t = t_adp;

es.ukf.tag = 'ukf';
es.ukf_async.tag = 'ukf async';
%%
% mh = MeasurementHandler({acc_adp,ars_adp,adp,ea_inp},t_start);
% s='1';
% tic
% last_percent = 0;
% 
% for i = N_adp%1:N_adp
%     if i/(N_adp/100) > last_percent
%         s = num2str(last_percent);
%         last_percent = last_percent + 1;
%         fprintf('Done with %s%s: Time elapsed %.2f%s',s,'%',toc,char(10));
% %         if i > 1
% %             es.mekf_rvel.data(:,i-1)';
% %         end
%     end
%     t_i = ea_inp.t(i);
%     if run_ukf
%         measurements = mh.get_measurements(t_i);
%         [x,P] = ukf_adp.update(t_i,measurements);
%         es.ukf.data(:,i) = x;    
%         es.ukf.P(:,:,i) = P;
%     end
% end

%%
aoa_ukf.t = t_adp;
aoa_ukf.data = es.ukf.data(1,:);
ssa_ukf.t = t_adp;
ssa_ukf.data = es.ukf.data(2,:);
Vwb_ukf.t = t_adp;
Vwb_ukf.data = es.ukf.data(3,:);
ukf.aoa = aoa_ukf;
ukf.ssa = ssa_ukf;
ukf.Vwb = Vwb_ukf;
ukf.P = es.ukf.P;

es.ukf.Pdiag.data = zeros(3,N_adp);
es.ukf_async.Pdiag.data = zeros(3,N);
es.ukf_async.Pdiag.t = t;
es.ukf.Pdiag.t = t_adp;
for i = 1:N_adp
    es.ukf.Pdiag.data(:,i) = diag(es.ukf.P(:,:,i));
end
for i = 1:N
    es.ukf_async.Pdiag.data(:,i) = diag(es.ukf_async.P(:,:,i));
end
es.ukf.Pdiag.tag = 'ukf';
es.ukf_async.Pdiag.tag = 'ukf async';

csubplotter({es.ukf,es.ukf_async,adp})

csubplotter({es.ukf.Pdiag,es.ukf_async.Pdiag});


plot(diff(es.ukf_async.Pdiag.data(1,:)))

subplotter(es.mekf.t,es.mekf.data(7:9,:))
subplotter(es.mekf.t,es.mekf.data(14:16,:))
subplotter(es.mekf.t,es.mekf.data(17:19,:))


%%
figure; plot3(es.mekf.data(1,:),es.mekf.data(2,:),es.mekf.data(3,:))
inds1 = find(t < 1700);
inds2 = find(t > 1700);
figure; hold on; plot3(es.mekf.data(1,inds1),es.mekf.data(2,inds1),es.mekf.data(3,inds1),'r')
hold on; plot3(es.mekf.data(1,inds2),es.mekf.data(2,inds2),es.mekf.data(3,inds2),'g')
