%% Wind estimation script
% Author Bård Nagy Stovner
load_exp_measurements
close all
do_save = 0;

addlib
fprintf('Running WIND estimation with data set number %s with estimators:\n',XXX);
% if exist('gpstimeshift')
%     mekf = importdata(sprintf('../data/mekf_%s_%.2f.mat',XXX,gpstimeshift(idt)));
% else
    mekf = importdata(sprintf('../data/mekf_%s.mat',XXX));
% end
x8 = importdata('../../simulator/x8_param.mat');
t = mekf.t;
t_start = t(1);
t_end = t(end);
N = length(t);
I3 = eye(3);

inds = find(ars.t >= t_start & ars.t <= t_end);

if strcmp(XXX,'005')
    GTw = [-1.3760;2.1381;-0.6576];
    GTg = 0.7192;
elseif strcmp(XXX,'007')
    GTw = [-1.3554;1.8022;0.1937];
    GTg = 0.7045;
elseif strcmp(XXX,'010')
    GTw = [-1.3554;1.8022;0.1937];
    GTg = 0.7045;
end
%% Initialize filters
std_pitot   = .5;
std_dp      = 15;

std_ars     = 5e-3;
std_acc     = 5e-2;
Q_acc       = I3*std_acc^2;
Q_ars       = I3*std_ars^2;

Rnb0 = Rquat(mekf.q(:,1));

%Measurement covariance
R_speed     = std_pitot^2; % airspeed measurements from Pitot_static tube
R_dp        = std_dp^2; %dynamic pressure measurements from Pitot-static tube
%Process covariance
Q_v         = blkdiag(1e-4,1e-4,1e-4);
Q_gamma     = 1e-6;
Q           = blkdiag(Q_v,Q_gamma)*1e3;
%Initial covariance
P0_wind     = blkdiag(1e-3,1e-3,1e-5);
P0_gamma    = blkdiag(1e-7);
%Initial estiamtes
v_nw0       = [0;0;0];
gamma0      = 1;
x0          = [v_nw0;gamma0];
%standard input to classes
input.x0    = x0;
input.Q     = Q;
input.rho   = rho;
input.P0    = blkdiag(P0_wind,P0_gamma);
input.R     = R_dp;

%wind 3sf1
wind_3sf1_input                 = input;
wind_3sf1_input.window_length   = 1000;
wind_3sf1                       = class_wind_3sf1(wind_3sf1_input);

%wind 3sf2
wind_3sf2_input     = input;
wind_3sf2_input.R   = R_speed*10;
wind_3sf2_input.Q   = Q;
wind_3sf2           = class_wind_3sf2(wind_3sf2_input);

%wind 3sf3
wind_3sf3_input     = input;
wind_3sf3_input.R   = R_dp;
wind_3sf3           = class_wind_3sf3(wind_3sf3_input);

%wind ekf
wind_ekf        = class_wind_ekf(input);

%wind ta
wind_ta_input   = input;
wind_ta_input.R = R_speed;
wind_ta         = class_wind_ta(wind_ta_input);

%% Run filters
run_all         = 1;
run_wind_3sf    = 0 || run_all;
run_wind_ekf    = 0 || run_all;
run_wind_ta     = 0 || run_all;

if run_wind_3sf
    es.wind_3sf1.data = zeros(4,N);
    es.wind_3sf2.data = zeros(4,N);
    es.wind_3sf3.data = zeros(4,N);
    fprintf('%c - Three-stage filter\n',char(9))
end
if run_wind_ekf
    es.wind_ekf.data = zeros(12,N);
    fprintf('%c - Extended Kalman filter\n',char(9))
end
if run_wind_ta
    es.wind_ta.data = zeros(4,N);
    fprintf('%c - Tor Arnes filter\n',char(9))
end

f = waitbar(0,'Processing');
s='1';
tic
last_percent = 0;
mh = MeasurementHandler({acc,ars,gps1,gps2,gps3,pitot,dp,gpsvel1,gpsvel2,mag,adp,elevator},t_start);
beta = zeros(N,1);
saveairspeeds = zeros(2,N);
for i = 1:N
%     if i/(N/100) > last_percent
%         s = num2str(last_percent);
%         last_percent = last_percent + 1;
%         fprintf('Done with %s%s: Time elapsed %.2f%s',s,'%',toc,char(10));
% %         if i > 1
% %             es.mekf_rvel.data(:,i-1)'
% %         end
%     end
    waitbar(i/N,f)
    t_i = t(i);
    measurements = mh.get_measurements(t_i);
    %Filters
    measurements.b_acc_hat.data = mekf.bacc(:,i);
    measurements.b_ars_hat.data = mekf.bars(:,i);
    measurements.velocity.data = mekf.vel(:,i);
    measurements.quaternion.data = mekf.q(:,i);
    if run_wind_3sf
        ret_3sf1 = wind_3sf1.update(t_i,measurements);
        es.wind_3sf1.data(:,i) = ret_3sf1.x;
        measurements.x_3sf1 = ret_3sf1.x;
        measurements.C_3sf1 = ret_3sf1.C;
        measurements.y_3sf1 = ret_3sf1.y;
        x_3sf2 = wind_3sf2.update(t_i,measurements);
        es.wind_3sf2.data(:,i) = x_3sf2;
        measurements.x_3sf2 = x_3sf2;
        es.wind_3sf3.data(:,i) = wind_3sf3.update(t_i,measurements);
    end
    if run_wind_ekf
        es.wind_ekf.data(:,i) = wind_ekf.update(t_i,measurements);
    end
    rvel = Rquat(measurements.quaternion.data)'*(measurements.velocity.data-es.wind_ekf.data(1:3,i));
    measurements.airspeed_hat2.data = sqrt(2*measurements.dp.data/(0.71*1.225));
    measurements.airspeed_hat.data = norm(rvel);
    measurements.beta_hat.data = asin(rvel(2)/norm(rvel));
    measurements.alpha_hat.data = atan2(rvel(3),rvel(1));
    beta(i) = measurements.beta_hat.data;
    if run_wind_ta
        es.wind_ta.data(:,i) = wind_ta.update(t_i,measurements);
    end
    saveairspeeds(:,i) = [measurements.airspeed_hat.data;measurements.airspeed_hat2.data];
    if isempty(measurements.airspeed_hat2.data)
        saveairspeeds(2,i) = 0;
    end
end


%%

%Wind GT
gt_vnw.t = [t(1),t(end)];
gt_vnw.data = GTw*[1,1];
gt_gamma.t = [t(1),t(end)];
gt_gamma.data = GTg*[1,1];

%Wind EKF
rvel = zeros(3,N);
aoa = zeros(1,N);
ssa = zeros(1,N);
Vwb = zeros(1,N);

wind_vnw.t = t;
wind_vnw.data = es.wind_ekf.data(1:3,:);
wind_gamma.t = t;
wind_gamma.data = es.wind_ekf.data(4,:); 
wind_vwb.data = zeros(3,N);   wind_vwb.t = t;
wind_aoa.data = zeros(1,N);    wind_aoa.t = t;
wind_ssa.data = zeros(1,N);    wind_ssa.t = t;
wind_Vwb.data = zeros(1,N);    wind_Vwb.t = t;

%Wind TA
wind_ta_vnw.t = t;
wind_ta_vnw.data = es.wind_ta.data(1:3,:);
wind_ta_gamma.t = t;
wind_ta_gamma.data = 1./es.wind_ta.data(4,:);
wind_ta_vwb.data = zeros(3,N);   wind_ta_vwb.t = t;
wind_ta_aoa.data = zeros(1,N);    wind_ta_aoa.t = t;
wind_ta_ssa.data = zeros(1,N);    wind_ta_ssa.t = t;
wind_ta_Vwb.data = zeros(1,N);    wind_ta_Vwb.t = t;

%Wind 3SF1
wind_3sf1_vnw.t = t;
wind_3sf1_vnw.data = es.wind_3sf1.data(1:3,:);
wind_3sf1_gamma.t = t;
wind_3sf1_gamma.data = es.wind_3sf1.data(4,:);

%Wind 3SF2
wind_3sf2_vnw.t = t;
wind_3sf2_vnw.data = es.wind_3sf2.data(1:3,:);
wind_3sf2_gamma.t = t;
wind_3sf2_gamma.data = es.wind_3sf2.data(4,:);

%Wind 3SF3
wind_3sf3_vnw.t = t;
wind_3sf3_vnw.data = es.wind_3sf3.data(1:3,:);
wind_3sf3_gamma.t = t;
wind_3sf3_gamma.data = es.wind_3sf3.data(4,:);
wind_3sf3_vwb.data = zeros(3,N);   wind_3sf3_vwb.t = t;
wind_3sf3_aoa.data = zeros(1,N);    wind_3sf3_aoa.t = t;
wind_3sf3_ssa.data = zeros(1,N);    wind_3sf3_ssa.t = t;
wind_3sf3_Vwb.data = zeros(1,N);    wind_3sf3_Vwb.t = t;

for i = 1:N
    Rnb = Rquat(mekf.q(:,i));
    
    rvel(:,i) = Rnb'*(mekf.vel(:,i)-es.wind_ekf.data(1:3,i));
    wind_vwb.data(:,i) = Rnb'*(mekf.vel(:,i)-es.wind_ekf.data(1:3,i));
    wind_Vwb.data(i) = norm(rvel(:,i));
    wind_aoa.data(i) = atan2(rvel(3,i),rvel(1,i))*RAD2DEG;
    wind_ssa.data(i) = asin(rvel(2,i)/norm(rvel(:,i)))*RAD2DEG;
    
    wind_3sf3_vwb.data(:,i) = Rnb'*(mekf.vel(:,i)-wind_3sf3_vnw.data(1:3,i));
    wind_3sf3_Vwb.data(i) = norm(wind_3sf3_vwb.data(:,i));
    wind_3sf3_aoa.data(i) = atan2(wind_3sf3_vwb.data(3,i),wind_3sf3_vwb.data(1,i))*RAD2DEG;
    wind_3sf3_ssa.data(i) = asin(wind_3sf3_vwb.data(2,i)/norm(wind_3sf3_vwb.data(:,i)))*RAD2DEG;
    
    wind_ta_vwb.data(:,i) = Rnb'*(mekf.vel(:,i)-wind_ta_vnw.data(1:3,i));
    wind_ta_Vwb.data(i) = norm(wind_ta_vwb.data(:,i));
    wind_ta_aoa.data(i) = atan2(wind_ta_vwb.data(3,i),wind_ta_vwb.data(1,i))*RAD2DEG;
    wind_ta_ssa.data(i) = asin(wind_ta_vwb.data(2,i)/norm(wind_ta_vwb.data(:,i)))*RAD2DEG;
end

%Aeroprobe
%Interpolate
ap_aoa_inp = interp1(ap.aoa.t,ap.aoa.data,wind_aoa.t);
cal_aoa = 0*mean(aoa-ap_aoa_inp);

cal = [cal_aoa,0];
ap_aoa.t = ap.aoa.t;
ap_aoa.data = (ap.aoa.data+cal(1))*RAD2DEG;
ap_ssa.t = ap.ssa.t;
ap_ssa.data = (ap.ssa.data+cal(2))*RAD2DEG;
ap_rvel.t = ap.rvel.t;
ap_rvel.data = zeros(3,length(ap_rvel.t));
ap_vnw.t = t;
ap_vnw.data = zeros(3,N);
for i = 1:length(ap_rvel.t)
    a = ap.aoa.data(i)+cal(1);
    b = ap.ssa.data(i)+cal(2);
    ap_rvel.data(:,i) = ap.airspeed.data(i)*[...
                            cos(a)*cos(b);
                                   sin(b);
                            sin(a)*cos(b)];
end
ap_rvel_inp = interpolateNd(ap_rvel.t,ap_rvel.data,t);
for i = 1:N
    Rnb = Rquat(mekf.q(:,i));
    ap_vnw.data(:,i) = mekf.vel(:,i) - Rnb*ap_rvel_inp(:,i);
end


%% Plot wind velocity
pd_wind.v_nw =  {gt_vnw, px.wvel, wind_vnw, wind_ta_vnw, wind_3sf1_vnw, wind_3sf2_vnw, wind_3sf3_vnw};
plotinput_wind = make_plotinput();
plotinput_wind.colors = plotinput_wind.colors([1,2,3,7,4,5,6]);
plotinput_wind.ylabels = {{'N [$m/s$]','E [$m/s$]','D [$m/s$]'}};
plotinput_wind.labels  = {'Wind Velocity'};
plotinput_wind.figpos = [0,0,700,600];
if strcmp(XXX,'005')
    plotinput_wind.xlims = {[300,2400],[300,2400],[1500,2400],[2200,2400],[2000,2400],[0,2400]};
elseif strcmp(XXX,'010')
    plotinput_wind.xlims = {[450,2400],[450,1960],[1760,1960],[1760,1960],[1760,1960],[500,1960]};
end
plotinput_wind.do_save = 1;
plotinput_wind.savefolder = sprintf('~/work/postdoc/latex/papers/windestimation/plots%s/',XXX);
plotinput_wind.savetype = '.eps';
plotinput_wind.savesuffix = '-Qlarge';
plotinput_wind.selected_estimators = [1,1,1,1,0,1,1];
plotinput_wind = rmfield(plotinput_wind,'legends');
% plotinput_wind.ylims = {[-3,1;
%                          -0,4;
%                          -1.5,0]};
plotinput_wind.ylims = {[-10,10;
                         -10,10;
                         -10,10]};

plotinput_wind.legends = {'Opt.', 'Pixhawk','EKF','KF[4]','3SF1','3SF2','3SF3'};
plotinput_wind.where_legends = {[1,0,0]};
plotter(pd_wind,plotinput_wind);

%% Plot gamma
pd_gamma.gamma = {gt_gamma, 0, wind_gamma, wind_ta_gamma, wind_3sf1_gamma, wind_3sf2_gamma, wind_3sf3_gamma};
plotinput_gamma = plotinput_wind;
plotinput_gamma.legends = {'Opt.', 'Pixhawk','EKF','KF[4]','3SF1','3SF2','3SF3'};
plotinput_gamma.ylabels = {{'Correction factor'}};
plotinput_gamma.labels  = {'Gamma'};
plotinput_gamma.figpos = [0,0,700,300];
% plotinput_gamma.ylims = {[0.6,1]};
plotinput_gamma.ylims = {[0,2]};
plotter(pd_gamma,plotinput_gamma);
%% --- Plotting states of MEKF ---
% % %Integrating ars to euler angles
% % eaint = zeros(3,N);
% % eaint(:,1) = mekf_ea.data(:,1);
% % ars_excerpt = ars.data(:,inds);
% % for i = 1:N-1
% %     [~,~,T] = eulerang(eaint(1,i),eaint(2,i),eaint(3,i));
% %     eaint(:,i+1) = eaint(:,i) + dt*T*(ars_excerpt(:,i)-mekf.bars(:,i));
% % end
% % eaint_dr.t = t;
% % eaint_dr.data = wrapToPi(eaint);
% 
% mekf_pos.data    = mekf.pos;     mekf_pos.t = t;
% mekf_vel.data    = mekf.vel;     mekf_vel.t = t;
% mekf_bacc.data   = mekf.bacc;    mekf_bacc.t = t;
% mekf_ea.data     = mekf.ea;      mekf_ea.t = t;
% mekf_bars.data   = mekf.bars;    mekf_bars.t = t;
% mekf_bmag.data   = mekf.bmag;    mekf_bmag.t = t;
% 
% pd_mekf.pos = {mekf.px.pos,mekf_pos};
% pd_mekf.vel = {mekf.px.vel,mekf_vel};
% pd_mekf.b_acc = {0,mekf_bacc};
% pd_mekf.ea = {mekf.px.ea,mekf_ea};
% pd_mekf.b_ars = {0,mekf_bars};
% 
% plotinput_mekf = make_plotinput();
% plotinput_mekf.legends = {'PX','MEKF'};
% plotter(pd_mekf,plotinput_mekf);
% 
% if do_save
% %     save(sprintf('~/work/statuses/2019-02-21-tor-arne/mekf%s.mat',XXX),'pd_mekf')
% %     save('~/work/statuses/2019-02-21-tor-arne/plotinput_mekf.mat','plotinput_mekf')
% %     save(sprintf('../data/wind_%s_%.2f.mat',XXX,gpstimeshift(idt)),'mekf');
% end

%% --- Plotting observability
pin.xlims = [t(1),t(end)];
subplotter(wind_eigs.t,wind_eigs.data,pin);
figure;
subplotter(wind_Peigs.t,wind_Peigs.data,pin);



%%
% fig=figure('position',[0,0,1000,600],'Color',[1,1,1]);
% ax = plot(mekf.pos(2,:),mekf.pos(1,:),'b'); 
% ax.LineWidth = 1.3
% xlabel('East [m]','FontSize',15,'interpreter','latex'); 
% ylabel('North [m]','FontSize',15,'interpreter','latex');
% export_fig('/home/bard/work/postdoc/latex/papers/windestimation/plots005/XYMap.eps')





