classdef class_wind_aug < handle
    %EKF Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        %states
        v_nw;
        gamma;
        v_sqnorm;
        
        %Covariance matrices
        Q;
        P;
        R;
        
        %Density of air
        rho
        
        %Identity matrix of appropriate size
        I;
        
        %in order to calculate time step
        prev_time;
        
        %enables delayed starting
        start_time = 0;
        
        %previous reset of v_sqnorm
        prev_reset = 0;
    end
    
    methods
        function kf = class_wind_aug(input)
            x0                          = input.x0;
            kf.v_nw                     = x0(1:3); 
            kf.gamma                    = x0(4);
            kf.v_sqnorm                 = norm(kf.v_nw)^2;
            kf.P                        = input.P0;
            kf.Q                        = input.Q;
            kf.I                        = eye(size(input.P0));
            kf.R                        = input.R;
            if isfield(input,'start_time')
                kf.start_time = input.start_time;
            end
        end
        function x = update(kf,time,measurements)
            dp      = measurements.dp.data;
%             v_abs   = measurements.pitot.data;
            v_nb    = measurements.velocity.data;
            if isempty(dp)
                x = [kf.v_nw;1/kf.gamma;kf.v_sqnorm];
                return
            end
            
            if kf.start_time ~= -Inf && time < kf.start_time
                x = [kf.v_nw;kf.gamma];
                return
            else
                kf.start_time = -Inf;
            end
            
%             if time - kf.prev_reset > 10
%                 kf.v_sqnorm = norm(kf.v_nw)^2;
%                 kf.prev_reset = time;
%             end
            
            %find dt
            dt = min([.1,time-kf.prev_time]);
            kf.prev_time=time;
            
            %cov time update
            kf.P = kf.P + kf.Q*dt;
            kf.P = .5*(kf.P+kf.P');
            
            %no state update
            
            %%
            % --- KF update ---
            H = [2*v_nb', v_abs^2, -1; 2*kf.v_nw',0,-1];
%             H = [1;0].*H;
            y_hat = [2*v_nb'*kf.v_nw + dp*2/1.225*kf.gamma - kf.v_sqnorm;norm(kf.v_nw)^2-kf.v_sqnorm];
            y = [norm(v_nb)^2;0];
            K = kf.P*H'/(H*kf.P*H'+kf.R);
            kf.P = (kf.I-K*H)*kf.P*(kf.I-K*H)'+K*kf.R*K';
            kf.P = (kf.P+kf.P')/2;
            ytilde = y-y_hat;
            xtilde = K*ytilde;
            %%%
            
            % --- KF update ---
            H = [2*v_nb', dp*2\1.225, -1; 2*kf.v_nw',0,-1];
%             H = [1;0].*H;
            y_hat = [2*v_nb'*kf.v_nw + dp*2/1.225*kf.gamma - kf.v_sqnorm;norm(kf.v_nw)^2-kf.v_sqnorm];
            y = [norm(v_nb)^2;0];
            K = kf.P*H'/(H*kf.P*H'+kf.R);
            kf.P = (kf.I-K*H)*kf.P*(kf.I-K*H)'+K*kf.R*K';
            kf.P = (kf.P+kf.P')/2;
            ytilde = y-y_hat;
            xtilde = K*ytilde;

            kf.v_nw = kf.v_nw + xtilde(1:3);
            kf.gamma = kf.gamma + xtilde(4);
            kf.v_sqnorm = kf.v_sqnorm + xtilde(5);
            
            

            x = [kf.v_nw;1/kf.gamma;kf.v_sqnorm];
        end
    end
end

