%% Wind estimation script
% Author Bård Nagy Stovner
close all

RAD2DEG = 180/pi;
DEG2RAD = 1/RAD2DEG;

% mekf = importdata('../../simulator/data.mat');
% t = mekf.t;
% mekf.vel = mekf.vel_n;
% N = length(t);

std_dp      = 10;
std_pitot   = .5*std_dp/20;
std_ea      = 0;

t_end = 600;
last_index = find(ss.pos.t<=t_end); last_index = length(last_index);

dp_freq = 10;
h = mean(diff(ss.pos.t));
inds = 1:round(1/(h*dp_freq)):last_index;
t = ss.pos.t(inds);

N = length(t);
mekf.vel = ss.vel.data(:,inds);
mekf.ea = ss.ea.data(:,inds) + std_ea*randn(3,N);
mekf.q = zeros(4,N);
for i = 1:N
    mekf.q(:,i) = euler2q(mekf.ea(1,i),mekf.ea(2,i),mekf.ea(3,i)); 
end
rho = 1.225;

do_save = 0;
do_plot = 0;
I3 = eye(3);

t_start = t(1);

GTw = [3*randn(2,1);.3*randn];
GTg = 1-.15*randn;

true_vnw.data = GTw*ones(1,2);
true_vnw.t = [t(1),t(end)];
true_gamma.data = GTg*ones(1,2);
true_gamma.t = [t(1),t(end)];

dp.t = t;
dp.data = GTg*.5*rho*sum((mekf.vel-GTw).^2,1) + std_dp*randn(1,length(dp.t));
dp.tag = 'dp';

%% Initialize filters
mistuning = 40;
Rnb0 = Rquat(mekf.q(:,1));

%Measurement covariance
R_speed     = std_pitot^2; % airspeed measurements from Pitot_static tube
R_dp        = std_dp^2; %dynamic pressure measurements from Pitot-static tube
%Process covariance
Q_v         = blkdiag(1e-5,1e-5,5e-5);
Q_gamma     = 1e-7;
Q           = blkdiag(Q_v,Q_gamma)*mistuning; %remove 1e2
%Initial covariance
P0_wind     = blkdiag(1e-1,1e-1,1e-2);
P0_gamma    = 1e-2;
%Initial estiamtes
v_nw0       = [0;0;0];
gamma0      = 1;
x0          = [v_nw0;gamma0];
%standard input to classes
input.x0    = x0;
input.Q     = Q;
input.rho   = rho;
input.P0    = blkdiag(P0_wind,P0_gamma);
input.R     = R_dp/mistuning; %remove /10

%wind 3sf1
wind_3sf1_input                 = input;
wind_3sf1_input.window_length   = 300;
wind_3sf1                       = class_wind_3sf1(wind_3sf1_input);

%wind 3sf2
wind_3sf2_input     = input;
wind_3sf2_input.R   = R_dp*(2/rho)^2/mistuning; %remove/10
wind_3sf2_input.Q   = Q;
wind_3sf2           = class_wind_3sf2(wind_3sf2_input);

%wind 3sf3
wind_3sf3_input     = input;
wind_3sf3_input.R   = R_dp/mistuning;
wind_3sf3           = class_wind_3sf3(wind_3sf3_input);

%wind ekf
wind_ekf            = class_wind_ekf(input);

%wind ta
wind_ta_input   = input;
wind_ta_input.R = R_speed/sqrt(mistuning);
wind_ta         = class_wind_ta(wind_ta_input);

%% Run filters
run_all         = 1;
run_wind_3sf    = 0 || run_all;
run_wind_ekf    = 0 || run_all;
run_wind_ta     = 0 || run_all;

if run_wind_3sf
    es.wind_3sf1.data = zeros(4,N);
    es.wind_3sf2.data = zeros(4,N);
    es.wind_3sf3.data = zeros(4,N);
%     fprintf('%c - Three-stage filter\n',char(9))
end
if run_wind_ekf
    es.wind_ekf.data = zeros(12,N);
%     fprintf('%c - Extended Kalman filter\n',char(9))
end
if run_wind_ta
    es.wind_ta.data = zeros(4,N);
%     fprintf('%c - Tor Arnes filter\n',char(9))
end

s='1';
tic
last_percent = 0;
mh = MeasurementHandler({dp},t_start);
beta = zeros(N,1);
saveairspeeds = zeros(2,N);
for i = 1:N
%     if i/(N/100) > last_percent
%         s = num2str(last_percent);
%         last_percent = last_percent + 1;
%         fprintf('Done with %s%s: Time elapsed %.2f%s',s,'%',toc,char(10));
% %         if i > 1
% %             es.mekf_rvel.data(:,i-1)'
% %         end
%     end
    t_i = t(i);
    measurements = mh.get_measurements(t_i);
    %Filters
    measurements.velocity.data = mekf.vel(:,i);
    measurements.quaternion.data = mekf.q(:,i);
    if run_wind_3sf
        ret_3sf1 = wind_3sf1.update(t_i,measurements);
        es.wind_3sf1.data(:,i) = ret_3sf1.x;
        measurements.x_3sf1 = ret_3sf1.x;
        measurements.C_3sf1 = ret_3sf1.C;
        measurements.y_3sf1 = ret_3sf1.y;
        x_3sf2 = wind_3sf2.update(t_i,measurements);
        es.wind_3sf2.data(:,i) = x_3sf2;
        measurements.x_3sf2 = x_3sf2;
        es.wind_3sf3.data(:,i) = wind_3sf3.update(t_i,measurements);
    end
    if run_wind_ekf
        es.wind_ekf.data(:,i) = wind_ekf.update(t_i,measurements);
    end
    if run_wind_ta
        es.wind_ta.data(:,i) = wind_ta.update(t_i,measurements);
    end
end


%%
wind_vnw.t = t;
wind_vnw.data = es.wind_ekf.data(1:3,:);
wind_gamma.t = t;
wind_gamma.data = es.wind_ekf.data(4,:); 

%Wind TA
wind_ta_vnw.t = t;
wind_ta_vnw.data = es.wind_ta.data(1:3,:);
wind_ta_gamma.t = t;
wind_ta_gamma.data = 1./es.wind_ta.data(4,:);

%Wind 3SF1
wind_3sf1_vnw.t = t;
wind_3sf1_vnw.data = es.wind_3sf1.data(1:3,:);
wind_3sf1_gamma.t = t;
wind_3sf1_gamma.data = es.wind_3sf1.data(4,:);

%Wind 3SF2
wind_3sf2_vnw.t = t;
wind_3sf2_vnw.data = es.wind_3sf2.data(1:3,:);
wind_3sf2_gamma.t = t;
wind_3sf2_gamma.data = es.wind_3sf2.data(4,:);

%Wind 3SF3
wind_3sf3_vnw.t = t;
wind_3sf3_vnw.data = es.wind_3sf3.data(1:3,:);
wind_3sf3_gamma.t = t;
wind_3sf3_gamma.data = es.wind_3sf3.data(4,:);

pd_wind.v_nw =  {true_vnw  ,wind_vnw  ,  wind_ta_vnw,  wind_3sf1_vnw,  wind_3sf2_vnw, wind_3sf3_vnw  };
pd_wind.gamma = {true_gamma,wind_gamma,wind_ta_gamma,wind_3sf1_gamma,wind_3sf2_gamma, wind_3sf3_gamma};

%% 
if do_plot
    plotinput = make_plotinput();
    plotinput.colors = plotinput.colors([1,3,7,4,5,6]);
    plotinput.legends = {'True', 'Wind EKF','Johansen [8]','Wind 3SF1','Wind 3SF2','Wind 3SF3'};
    plotinput.ylabels = {{'N [$m/s$]','E [$m/s$]','D [$m/s$]'},{'Gamma'},{'AOA [deg]'},{'SSA [deg]'},{'$\|V_wb\|_2$'},{'X [$m/s$]','Y [$m/s$]','Z [$m/s$]'}};
    plotinput.labels  = {'Wind Velocity','Gamma','AOA','SSA','Airspeed','Relative Velocity'};
    plotinput.figpos = [0,0,1000,600];
    plotinput.do_save = 0;
    plotinput.savefolder = sprintf('~/work/postdoc/latex/papers/windestimation/simplots/');
    plotinput.savetype = '.eps';
%     plotinput.savesuffix = '-Qlarge';
    % plotinput.selected_fields = [1,1,1,0,0,0];
    plotinput.selected_estimators = [1,1,1,1,1,1,1,1,1];
    plotter(pd_wind,plotinput);
end
if do_save
    save('../data/wind_sim.mat','pd_wind');
end


%%
% fig=figure('position',[0,0,1000,600],'Color',[1,1,1]);
% ax = plot(mekf.pos(2,:),mekf.pos(1,:),'b'); 
% ax.LineWidth = 1.3
% xlabel('East [m]','FontSize',15,'interpreter','latex'); 
% ylabel('North [m]','FontSize',15,'interpreter','latex');
% export_fig('/home/bard/work/postdoc/latex/papers/windestimation/plots005/XYMap.eps')




