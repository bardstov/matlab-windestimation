% clear all;
% close all;

if ~exist('XXX')
    error(sprintf('Error: Specify variable XXX first! Alternatives: \n\t 005 \n\t 007 \n\t 010\n'))
end
set(groot,'defaulttextinterpreter','latex');  
set(groot, 'defaultAxesTickLabelInterpreter','latex');  
set(groot, 'defaultLegendInterpreter','latex'); 

% data_loaded = 1;

%addpath('/home/bard/work/flight-2018-april/estimation/lib/estimators/')
%addpath('/home/bard/work/flight-2018-april/estimation/lib/support_classes/')
%addpath('/home/bard/work/flight-2018-april/estimation/lib/support_functions/')
% XXX = '007';
folder = replace('~/work/data/Uddovoll_020518/flight_XXX/sentiboard/combined/matlab_01/','XXX',XXX);
if XXX == '005'
    plotxlim = [322.5,2521.8];
elseif XXX == '007'
    plotxlim = [457.4,2606];
elseif XXX == '010'
    plotxlim = [498.7,2121.6];    
end
RAD2DEG = 180/pi;
DEG2RAD = 1/RAD2DEG;
%% ADIS
adis_file = [folder,'sensor_6_rejected.mat'];
radis = importdata(adis_file);
R_correct_adis = Rzyx(pi,0,0);
adis.ars.t = radis.ars.tot;
adis.ars.data = R_correct_adis*radis.ars.values';
adis.acc.t = radis.acc.tot;
adis.acc.data = R_correct_adis*radis.acc.values';
R_correct_mag = [0,1,0;-1,0,0;0,0,-1];
adis.mag.t = radis.mag.tot;
adis.mag.data = R_correct_mag*radis.mag.values';
% tmp = -adis.mag.data(1:2,:);
% adis.mag.data(1,:) = tmp(2,:);
% adis.mag.data(2,:) = tmp(1,:);
%% STIM
stim_file = [folder,'sensor_2_rejected.mat'];
rstim = importdata(stim_file);
R_correct_stim = Rzyx(pi,0,pi/2);
stim.acc.t = rstim.acc.tot;
stim.acc.data = R_correct_stim*rstim.acc.values';
stim.ars.t = rstim.ars.tot;
stim.ars.data = R_correct_stim*rstim.ars.values';
%% AEROPROBE
rap = importdata([folder,'sensor_1.mat']);
ap.aoa.t = fix_time(rap.tot,1)*1e-8;
ap.aoa.data = rap.AoA*DEG2RAD;
ap.ssa.t = fix_time(rap.tot,1)*1e-8;
ap.ssa.data = -rap.AoS*DEG2RAD;
ap.airspeed.t = fix_time(rap.tot,1)*1e-8;
ap.airspeed.data = rap.V;
ap.rvel.t = ap.aoa.t;
ap.rvel.data = zeros(3,length(ap.rvel.t));
taoa = ap.aoa.data;
tssa = ap.ssa.data;
for i = 1:length(ap.rvel.t)
    ap.rvel.data(:,i) = [cos(tssa(i))*cos(taoa(i));
                         sin(tssa(i));
                         cos(tssa(i))*sin(taoa(i))]*ap.airspeed.data(i);
end
%% PIXHAWK
pxfolder = [folder,'../../../pixhawk/'];
rpx = importdata([pxfolder,getfield(dir([pxfolder,'*.mat']),'name')]);
t_timescale = (1+3.5e-5);
t_offset = find_offset(stim.acc.t,stim.acc.data(3,:),double(rpx.IMU_TimeUS)*1e-6/t_timescale,rpx.IMU_AccZ,0);
% t_offset =    43;
%ADP
px.aoa.t = double(rpx.AOA_TimeUS)*1e-6/t_timescale-t_offset;
px.aoa.data = rpx.AOA_AOA+3;
px.ssa.t = double(rpx.AOA_TimeUS)*1e-6/t_timescale-t_offset;
px.ssa.data = rpx.AOA_SSA-5;
%Pitot tube
px.airspeed.t = double(rpx.ARSP_TimeUS)*1e-6/t_timescale-t_offset;
px.airspeed.data = rpx.ARSP_Airspeed;
px.pd.t = double(rpx.ARSP_TimeUS)*1e-6/t_timescale-t_offset;
px.pd.data = rpx.ARSP_DiffPress;

airspeed_inp = interp1(px.airspeed.t,px.airspeed.data,px.aoa.t);
px.rvel.t = px.aoa.t;
px.rvel.data = zeros(3,length(px.aoa.t));
taoa = px.aoa.data*DEG2RAD;
tssa = px.ssa.data*DEG2RAD;
R_b_px = Rzyx(-0.0047,-0.0127,-0.0320);
for i = 1:length(px.aoa.t)
    px.rvel.data(:,i) = R_b_px*[...
                        cos(tssa(i))*cos(taoa(i));
                        sin(tssa(i));
                        cos(tssa(i))*sin(taoa(i))]*airspeed_inp(i);
end
%NKF
px.ea.t = double(rpx.NKF1_TimeUS)*1e-6/t_timescale-t_offset;
px.ea.data = wrapToPi([rpx.NKF1_Roll;rpx.NKF1_Pitch;rpx.NKF1_Yaw]*pi/180);
px.pos.t = double(rpx.NKF1_TimeUS)*1e-6/t_timescale-t_offset;
px.pos.data = [rpx.NKF1_PN;rpx.NKF1_PE;rpx.NKF1_PD];
px.vel.t = double(rpx.NKF1_TimeUS)*1e-6/t_timescale-t_offset;
px.vel.data = [rpx.NKF1_VN;rpx.NKF1_VE;rpx.NKF1_VD];
px.wvel.t = double(rpx.NKF2_TimeUS)*1e-6/t_timescale-t_offset;
px.wvel.data = [rpx.NKF2_VWN;rpx.NKF2_VWE;0*rpx.NKF2_VWE];
px.bars.t = double(rpx.NKF1_TimeUS)*1e-6/t_timescale-t_offset;
px.bars.data = [rpx.NKF1_GX;rpx.NKF1_GY;rpx.NKF1_GZ];
px.bmag.t = double(rpx.NKF2_TimeUS)*1e-6/t_timescale-t_offset;
px.bmag.data = double([rpx.NKF2_MX;rpx.NKF2_MY;rpx.NKF2_MZ])*1e-3;
px.magn.t = double(rpx.NKF2_TimeUS)*1e-6/t_timescale-t_offset;
px.magn.data = double([rpx.NKF2_MN;rpx.NKF2_ME;rpx.NKF2_MD])*1e-3;
%IMU
px.acc.t = double(rpx.IMU_TimeUS)*1e-6/t_timescale-t_offset;
px.acc.data = [rpx.IMU_AccX;rpx.IMU_AccY;rpx.IMU_AccZ];
px.ars.t = double(rpx.IMU_TimeUS)*1e-6/t_timescale-t_offset;
px.ars.data = [rpx.IMU_GyrX;rpx.IMU_GyrY;rpx.IMU_GyrZ];
px.mag.t = double(rpx.MAG_TimeUS)*1e-6/t_timescale-t_offset;
px.mag.data = double([rpx.MAG_MagX;rpx.MAG_MagY;rpx.MAG_MagZ])*1e-3;
%IMU2
px.ars2.t = double(rpx.IMU2_TimeUS)*1e-6/t_timescale-t_offset;
px.ars2.data = [rpx.IMU2_GyrX;rpx.IMU2_GyrY;rpx.IMU2_GyrZ];
px.acc2.t = double(rpx.IMU2_TimeUS)*1e-6/t_timescale-t_offset;
px.acc2.data = [rpx.IMU2_AccX;rpx.IMU2_AccY;rpx.IMU2_AccZ];
px.mag2.t = double(rpx.MAG2_TimeUS)*1e-6/t_timescale-t_offset;
px.mag2.data = double([rpx.MAG2_MagX;rpx.MAG2_MagY;rpx.MAG2_MagZ])*1e-3;

%Control signals
% px.ail.t = double(rpx.AETR_TimeUS)*1e-6/t_timescale-t_offset;
% px.ail.data = rpx.AETR_Ail;
% px.ele.t = double(rpx.AETR_TimeUS)*1e-6/t_timescale-t_offset;
% px.ele.data = rpx.AETR_Elev;
px.rpm.t = double(rpx.RPM_TimeUS)*1e-6/t_timescale-t_offset;
px.rpm.data = rpx.RPM_rpm1;

a_right = 8.589e-4; b_right = -1.339;
a_left = -7.334e-4; b_left = 1.122;
px.leftelevon.data = double(rpx.RCOU_C1)*a_left+b_left;
px.leftelevon.t = double(rpx.RCOU_TimeUS)*1e-6/t_timescale-t_offset;
px.rightelevon.data = double(rpx.RCOU_C2)*a_right+b_right;
px.rightelevon.t = double(rpx.RCOU_TimeUS)*1e-6/t_timescale-t_offset;
px.ele.t = double(rpx.RCOU_TimeUS)*1e-6/t_timescale-t_offset;
px.ele.data = .5*(px.leftelevon.data+px.rightelevon.data);
px.ail.t = double(rpx.RCOU_TimeUS)*1e-6/t_timescale-t_offset;
px.ail.data = .5*(px.leftelevon.data-px.rightelevon.data);

%% UBLOX
rtkfile = [folder,'RTK/sensor_XXXX.pos'];
rgps = importdata([folder,'../../../RTK/gps.mat']);
rgpsvel = importdata([folder,'gpsvel.mat']);
pb_gps1 = [ 0.355,     0, -.2]';
pb_gps2 = [-0.245, 0.905,   0]';
pb_gps3 = [-0.245,-0.905,   0]';
inds = find(isnan(rgps{1}.tot) == 0);
gps.pos.t = rgps{1}.tot(inds);
gps.pos.data = rgps{1}.pos_ned(:,inds);
gps.pos.rcv = pb_gps1;
gps.vel = rgpsvel{1};
gps.vel.t = gps.vel.tot;
inds = find(isnan(rgps{2}.tot) == 0);
gps.pos2.t = rgps{2}.tot(inds);
gps.pos2.data = rgps{2}.pos_ned(:,inds);
gps.pos2.rcv = pb_gps2;
gps.vel2 = rgpsvel{2};
gps.vel2.t = gps.vel2.tot;
inds = find(isnan(rgps{3}.tot) == 0);
gps.pos3.t = rgps{3}.tot(inds);
gps.pos3.data = rgps{3}.pos_ned(:,inds);
gps.pos3.rcv = pb_gps3;

%interpolate and find ea
gps_inp.pos = gps.pos;
gps_inp.pos2.t = gps.pos.t; gps_inp.pos3.t = gps.pos.t; 

gps_inp.pos2.data = zeros(3,length(gps.pos.t));
gps_inp.pos3.data = zeros(3,length(gps.pos.t));
for i = 1:3
    gps_inp.pos2.data(i,:) = interp1(gps.pos2.t,gps.pos2.data(i,:),gps.pos.t);
    gps_inp.pos3.data(i,:) = interp1(gps.pos3.t,gps.pos3.data(i,:),gps.pos.t);
end
while 1
    stop = 1;
    if sum(isnan([gps_inp.pos.data(1),gps_inp.pos2.data(1),gps_inp.pos3.data(1)]))
        gps_inp.pos.t = gps_inp.pos.t(2:end);gps_inp.pos.data = gps_inp.pos.data(:,2:end);
        gps_inp.pos2.t = gps_inp.pos.t(2:end);gps_inp.pos2.data = gps_inp.pos2.data(:,2:end);
        gps_inp.pos3.t = gps_inp.pos.t(2:end);gps_inp.pos3.data = gps_inp.pos3.data(:,2:end);
    elseif sum(isnan([gps_inp.pos.data(end),gps_inp.pos2.data(end),gps_inp.pos3.data(end)]))
        gps_inp.pos.t = gps_inp.pos.t(1:end-1);gps_inp.pos.data = gps_inp.pos.data(:,1:end-1);
        gps_inp.pos2.t = gps_inp.pos.t(1:end-1);gps_inp.pos2.data = gps_inp.pos2.data(:,1:end-1);
        gps_inp.pos3.t = gps_inp.pos.t(1:end-1);gps_inp.pos3.data = gps_inp.pos3.data(:,1:end-1);
    else
        break;
    end
end

gps_inp.ea.t = gps_inp.pos.t;
gps_inp.ea.data = zeros(3,length(gps_inp.pos.t));
p1 = gps.pos.rcv;
p2 = gps.pos2.rcv;
p3 = gps.pos3.rcv;
dp1_b = p3-p2;
dp2_b = p1-(p3+p2)/2;
Ab = find_triad(dp1_b,dp2_b);
Ab_inv = pinv(Ab);
for i = 1:length(gps_inp.ea.t)
    p1 = gps_inp.pos.data(:,i);
    p2 = gps_inp.pos2.data(:,i);
    p3 = gps_inp.pos3.data(:,i);
    dp1_n = p3-p2;
    dp2_n = p1-(p3+p2)/2;
    An = find_triad(dp1_n,dp2_n);
    Rnb_gps = An*Ab_inv;
    [a1,a2,a3] = R2euler(Rnb_gps);
    gps_inp.ea.data(:,i) = [a1;a2;a3];
end

%%
px = add_tags(px);
adis = add_tags(adis);
stim = add_tags(stim);
ap = add_tags(ap);
gps = add_tags(gps);
%%
ss.px = px;
ss.adis = adis;
ss.stim = stim;
ss.ap = ap;
ss.gps = gps;
% save ss010.mat ss
%%
clrs = 'rgbkcm';
% figure;
% for i = 1:3
%     subplot(3,1,i);
%     splot({px.mag2,px.mag,adis.mag},i);
% end
% %AOA
% pxaoa = px.aoa; pxaoa.data = lowpass(pxaoa.data',1,1/mean(diff(pxaoa.t)))';
% apaoa = ap.aoa; apaoa.data = lowpass(apaoa.data',1,1/mean(diff(apaoa.t)))';
% figure;splot({px.aoa,ap.aoa},1);
% %SSA
% figure;splot({px.ssa,ap.ssa},1);

% %%
% %CALIBRATION
% cal = importdata([folder,'cal.mat']);
% b_acc = zeros(3,1);%cal.b_acc;
% b_omg = zeros(3,1);%cal.b_omg;
% b_mag = zeros(3,1);%cal.b_mag;
% d = zeros(6,1);%cal.d_mag;
% D_mag = eye(3)+[d(1),d(4),d(5);d(4),d(2),d(6);d(5),d(6),d(3)];
% mag_n = [13583.6,790.0,50056.9]*1e-5;%cal.m_n;
% % % g = [0;0;9.81];%cal.g;
% acc_std = 20e-3 / 10;%cal.acc_std/10;
% omg_std = 1e-3;%cal.omg_std;
% mag_std = 5.2e-3;%cal.mag_std;

%SAVING

