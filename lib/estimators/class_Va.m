classdef class_Va < handle
    %EKF Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        %states
        x;
        
        %Covariance matrices
        Q;
        P;
        R_f;
        R_V
        
        %acceleration of gravity
        g = [0;0;9.81];
        
        %air density
        rho;
        
        %UAV parameters
        m; %mass
        S; %wing area
        
        %Identity matrix of appropriate size
        I;
        
        %in order to calculate time step
        prev_time;
        
        %
        started = false;
        
        %
        acc = zeros(3,1);
        ars = zeros(3,1);
        f = zeros(3,1);
    end
    
    methods
        function kf = class_Va(input)
            kf.x        = input.x0;
            kf.P        = input.P0;
            kf.Q        = input.Q;
            kf.I        = eye(size(input.P0));
            kf.R_f      = input.R_f;
            kf.R_V      = input.R_V;
            kf.S        = input.S;
            kf.m        = input.mass;
            kf.rho      = input.rho;
            if isfield(input,'g')
                kf.g = input.g;
            end
            
        end
        function x = update(kf,time,measurements)
            airspeed = measurements.pitot.data;
            
            if ~kf.started
                if ~isempty(airspeed) && airspeed < 5
                    x = kf.x;
                    return
                else
                    kf.started = true;
                end
            end
            
            if ~isempty(measurements.ars.data)
                kf.ars = measurements.ars.data;
            end
            if ~isempty(measurements.acc.data)
                kf.f = measurements.acc.data - measurements.b_acc_hat.data;
                kf.acc = kf.f + Rquat(measurements.quaternion.data)'*kf.g;
            end
            ax = kf.acc(1); az = kf.acc(3);
            fz = kf.f(3);
            q = kf.ars(2);
            
            V           = kf.x(1);
            alpha       = kf.x(2);
            C_L_0       = kf.x(3);
            C_L_alpha   = kf.x(4);
            gamma       = kf.x(5);
            
            %find dt
            dt = min([.1,time-kf.prev_time]);
            kf.prev_time=time;
            
            ca = cos(alpha);
            sa = sin(alpha);
            
            % --- KF covariance time update --- 
            F = [0,ca*ax - sa*az,0,0,0;
                 0,0,0,0,0;
                 0,0,0,0,0;
                 0,0,0,0,0;
                 0,0,0,0,0];
            G_V = [sa,0,ca];
            G_a = [0,1,0];
            G = blkdiag(G_V,G_a,eye(3));
            
            
            [Phi,Qd] = van_loan(F,G*kf.Q*G',dt);
            kf.P = Phi*kf.P*Phi'+Qd;
            kf.P = .5*(kf.P+kf.P');
            
            % --- KF state time update ---
            V = V + dt*(sa*ax + ca*az);
            alpha = alpha + dt*q;
            
            % --- KF covariance measurement update
            y = [];
            H = [];
            y_hat = [];
            pre = .5*kf.rho*V^2*kf.S;
            R = [];
            if ~isempty(airspeed)
                y = [y;airspeed];
                y_hat = [y_hat;gamma*V];
                H = [H;
                     gamma,... % dh/dV
                     0,0,0,... %dh/d[alpha,C_L_0,C_L_alpha]
                     V]; %dh/dgamma
                R = blkdiag(R,kf.R_V);
            end
            if ~isempty(measurements.acc.data)
                y = [y;kf.m*fz];
                y_hat = [y_hat;pre*(C_L_0 + C_L_alpha*alpha)];
                H = [H;
                     kf.rho*V*kf.S*(C_L_0 + C_L_alpha*alpha),... % dh/dV
                     pre*C_L_alpha,... %dh/dalpha
                     pre, pre*alpha,0]; %dh/d[C_L_0,C_L_alpha,gamma]
                R = blkdiag(R,kf.R_f);
            end
%             size(kf.P)
%             size(H)
%             size(kf.R)
            K = kf.P*H'/(H*kf.P*H'+R);
            kf.P = (kf.I-K*H)*kf.P*(kf.I-K*H)'+K*R*K';
            kf.P = (kf.P+kf.P')/2;
            
            % --- KF state measurement update ---
            ytilde = y-y_hat;
            xtilde = K*ytilde;
            kf.x = [V;
                    alpha;
                    C_L_0;
                    C_L_alpha;
                    gamma] + xtilde;
            x = kf.x;
        end
    end
end

