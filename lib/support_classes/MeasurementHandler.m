classdef MeasurementHandler < handle
    %MEASUREMENTHANDLER Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        counter;
        N_measurements;
        m;
    end
    
    methods
        function mh = MeasurementHandler(cell_of_measurements,t0)
            mh.m = cell_of_measurements;
            mh.N_measurements = length(cell_of_measurements);
            mh.counter = ones(mh.N_measurements,1);
            
            %spooling
            for i = 1:mh.N_measurements
                t1 = mh.m{i}.t(mh.counter(i));
                while t0 > t1
                    mh.counter(i) = mh.counter(i) + 1;
                    t1 = mh.m{i}.t(mh.counter(i));
                end
            end
        end
        
        function measurements = get_measurements(mh,t)
            measurements = [];
            for i = 1:mh.N_measurements
                tag = mh.m{i}.tag;
                if mh.counter(i) < length(mh.m{i}.t) && t >= mh.m{i}.t(mh.counter(i))
                    measurements.(tag).data = mh.m{i}.data(:,mh.counter(i));
                    mh.counter(i) = mh.counter(i) + 1;
                else
                    measurements.(tag).data = [];
                end
            end
        end
    end
end

