clear all; close all;
addlib
Nmc = 300;
dt = datetime;
dt.Format = 'uuuu-MM-dd--HH:mm:ss';
dt = char(dt);
folder = ['~/data/scitech/' dt '/'];
mkdir(folder);

ss = importdata('../../ecc2019/data_slow.mat');

pds = cell(Nmc,1);
tim = tic;
for i_mc = 1:Nmc
    
    runner_wind_estimation_sim
    pds{i_mc} = pd_wind;
    clearvars -except pds Nmc i_mc folder ss tim
    
    str = sprintf(':::: \t MC run %d/%d \t %.2f \t Remaning %.1f seconds ::::',i_mc,Nmc,toc(tim),toc(tim)/i_mc*(Nmc-i_mc));
    disp(str);
end
save([folder 'pds.mat'],'pds');


