classdef class_apkf < handle
    %EKF Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        %states
        x
        
        %Covariance matrices
        Q;
        P;
        R;
        
        %Identity matrix of appropriate size
        I;
        
        %gravitational acceleration
        g = [0;0;9.81];
        
        %in order to calculate time step
        prev_time;
        
        %enables delayed starting
        start_time = 0;
    end
    
    methods
        function kf = class_apkf(input)
            kf.x                           = input.x0;
            kf.P                        = input.P0;
            kf.Q                        = input.Q;
            kf.I                        = eye(size(input.P0));
            kf.R                        = input.R;
            if isfield(input,'start_time')
                kf.start_time = input.start_time;
            end
            kf.g = [0;0;9.81];
        end
        function x = update(kf,time,measurements)
            ap      = measurements.aeroprobe.data;
            q       = measurements.quaternion.data;
            ars_hat = measurements.ars.data-measurements.b_ars_hat.data;
            acc_hat = measurements.acc.data-measurements.b_acc_hat.data;
            
            if isempty(ap)
                x = kf.x;
                return
            end
            
            Rnb = Rquat(q); 
            
            %find dt
            dt = min([.1,time-kf.prev_time]);
            kf.prev_time=time;
            
            %cov time update
            A = Rnb'*Smtrx(ars_hat);
            G = [-Rnb'*Smtrx(kf.x),eye(3)];
            [Phi,Qd] = van_loan(A,G*kf.Q*G',dt);
            kf.P = Phi*kf.P*Phi'+Qd;
            kf.P = .5*(kf.P+kf.P');
            
            %state update
            kf.x = Phi*kf.x+dt*(acc_hat+Rnb'*kf.g);
            
            % --- KF update ---
            a = ap(1);
            b = ap(2);
            V = ap(3);
            y = V*[cos(a)*cos(b);sin(b);sin(a)*cos(b)];
            y_hat = kf.x;
            H = eye(3);
            K = kf.P*H'/(H*kf.P*H'+kf.R);
            kf.P = (kf.I-K*H)*kf.P*(kf.I-K*H)'+K*kf.R*K';
            kf.P = (kf.P+kf.P')/2;
            ytilde = y-y_hat;
            xtilde = K*ytilde;

            kf.x = kf.x + xtilde(1:3);
            x = kf.x;
        end
    end
end

