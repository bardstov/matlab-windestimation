close all
clear all

DEG2RAD = pi/180;
RAD2DEG = 1/DEG2RAD;

t_end = 1800;
h = 0.01;
t = 0:h:t_end-h;
N = length(t);

vb = [30;0;0];
ab = zeros(3,N);

g = 9.81;
gn = [0;0;9.81];


%% --- Make EA and ARS ---
% t_roll0 = 0:h:10-h;
% t_roll = 0:h:20-h;
% roll = pi/12*(1-cos(pi*t_roll0/10));
% while length(roll) < N
%     roll = [roll,...
%             pi/6*ones(1,40/h),pi/6*cos(pi*t_roll/20),-pi/6*ones(1,40/h),-pi/6*cos(pi*t_roll/20)];
% end
%%
%[pi/12*(1-cos(pi*t_roll0*h/10)),pi/6*ones(1,40/h),pi/6*cos(pi*t_roll*h/20),-pi/6*ones(1,40/h),-pi/6*cos(pi*t_roll*h/20)];
roll = pi/12*sin(2*pi*t/15) + pi/10*sin(2*pi*t/120);
pitch = pi/9*sin(2*pi*t/15);
% roll = roll(1:N);
yaw = zeros(1,N);
dyaw = roll*9.81/vb(1);
for i = 1:N
    yaw(i+1) = yaw(i) + h*dyaw(i);
end
ea = [roll;pitch;yaw(1:N)];
% dea = diff(ea')'/h;
% yaw_fig8 = sin((0:h:t_fig8-h)*2*pi/t_fig8)*3*pi/4;
% yaw = 2*pi/4*sin(2*pi*t/80);
% dyaw = diff(yaw)/h;
% dyaw = [dyaw,dyaw(end)];
% roll = dyaw*vb(1)/g;
% ea = [roll;pitch;yaw];
dea = diff(ea')'/h;


ars_out = zeros(3,N);
for i = 1:N-1
    [~,Rnb,Tea] = eulerang(ea(1,i),ea(2,i),ea(3,i));
    ars_out(:,i) = pinv(Tea)*dea(:,i);
end
ars_out(:,end) = ars_out(:,end-1);

ea_dr = zeros(3,N);
ea_dr(:,1) = ea(:,1);
for i = 1:N-1
    [~,~,Tea] = eulerang(ea_dr(1,i),ea_dr(2,i),ea_dr(3,i));
    ea_dr(:,i+1) = ea_dr(:,i) + h*Tea*ars_out(:,i);
end
figure
for i = 1:3
    subplot(3,1,i)
    hold on
    plot((ea(i,:)-ea_dr(i,:))*RAD2DEG,'r');
end
ea_out = ea_dr;
%%% !!!!! Best result by letting ea = ea_dr. Then, the integration of ars is
%% --- Make position and ACC ---
v2 = zeros(3,N);
p2 = zeros(3,N);
for i = 1:N
    Rnb = Rzyx(ea_out(1,i),ea_out(2,i),ea_out(3,i));
    v2(:,i) = Rnb*vb;
    p2(:,i+1) = p2(:,i) + h*v2(:,i);
end
p2 = p2(:,1:N);

an = diff(v2')'/h;
fb2 = zeros(3,N);
for i = 1:N-1
    Rnb = Rzyx(ea_out(1,i),ea_out(2,i),ea_out(3,i));
    fb2(:,i) = Rnb'*(an(:,i) - [0;0;9.81]);
end
fb2(:,end) = fb2(:,end-1);

Rnb0 = Rzyx(ea_out(1,1),ea_out(2,1),ea_out(3,1));
p_out = zeros(3,N);
v_out = zeros(3,N);
v_out(:,1) = Rnb0*vb;
fb_out = zeros(3,N);
for i = 1:N-1
    Rnb = Rzyx(ea_out(1,i),ea_out(2,i),ea_out(3,i));
    
    dvb = Smtrx(ars_out(:,i))*vb + ab(:,i);
    fb_out(:,i) =  dvb - Rnb'*[0;0;9.81];
    v_out(:,i+1) = v_out(:,i) + h*Rnb*dvb;
    p_out(:,i+1) = p_out(:,i) + h*v_out(:,i);
end

posfig = figure;
velfig = figure;
fbfig = figure;
velfig.Position(1) = sum(posfig.Position([1,3]))+10;
fbfig.Position(1) = sum(velfig.Position([1,3]))+10;
for i = 1:3
    figure(posfig)
    subplot(3,1,i)
    plot(p_out(i,:))
    
    figure(velfig)
    subplot(3,1,i)
    plot(v_out(i,:))

    figure(fbfig)
    subplot(3,1,i)
    hold on
    plot(fb2(i,:)-fb_out(i,:),'r')
end

pos3fig = figure;
pos3fig.Position(2) = posfig.Position(2)-posfig.Position(4) - 100;
plot3(p_out(1,:),p_out(2,:),p_out(3,:));





%% Store data in savestruct
% h_out = 0.01;
% inds = 1:h_out/h:t_end/h;
ss.pos.data = p_out;
ss.pos.t = t;
ss.vel.data = v_out;
ss.vel.t = t;

ss.acc.data = fb_out;
ss.acc.t = t;

ss.ars.data = ars_out;
ss.ars.t = t;

ss.ea.data = ea_out;
ss.ea.t = t;

save data.mat ss