function ed = pd2ed(pd)
    f = fields(pd);
    for j = 1:length(f)
        fieldname = f{j};
        datacells = pd.(fieldname);
        K = length(datacells);
        tr = datacells{1};
        for k = 2:K
            if isstruct(datacells{k})
                est = datacells{k};
                if strcmp(fieldname,'ea')
                    a = zeros(size(est.data));
                    for i = 1:length(est.t)
                        Rtr = Rzyx(tr.data(1,i),tr.data(2,i),tr.data(3,i));
                        Rest = Rzyx(est.data(1,i),est.data(2,i),est.data(3,i));
                        Rerror = Rest'*Rtr;
                        [a(1,i),a(2,i),a(3,i)] = R2euler(Rerror);
                    end
                    error = a;
                else
                    error = est.data - tr.data;
                end
                ed.(fieldname){k}.data = error;
                ed.(fieldname){k}.t = est.t;
            else
                ed.(fieldname){k} = 0;
            end
        end
    end
end