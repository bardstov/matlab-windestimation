%% Wind estimation script
% Author Bård Nagy Stovner
XXX = '005';
if exist(XXX) && exist(XXX_prev) && strcmp(XXX,XXX_prev)
    %do nothing
else
    clearvars -except XXX
    XXX_prev = XXX;
    load_exp_measurements
    mekf = importdata(sprintf('../data/mekf_%s.mat',XXX));
    x8 = importdata('../../simulator/x8_param.mat');
    t = mekf.t;
    t_start = t(1);
    t_end = t(end);
    N = length(t);
end

fprintf('Running WIND estimation with data set number %s with LSQ:\n',XXX);

if strcmp('005',XXX)
    ts = [500,2400];
elseif strcmp('007',XXX)
    
elseif strcmp('010',XXX)
    
else
    disp('Invalid input')
end

inds_lsq = find(t>=ts(1) & t <= ts(2));

