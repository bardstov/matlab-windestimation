classdef class_wind_opt < handle
    %EKF Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        %States
        x1 = zeros(3,1);
        x2 = 1;
        
        %Window length
        L;
        
        %Save vectors
        a = [];
        b = [];
        
        %Gradient and Hessian arrays
        H_gradient = [];
        H_hessian = [];
        
        %air density
        rho
    end
    
    methods
        function kf = class_wind_opt(input)
            kf.L        = input.window_length;
            kf.x1       = input.x0(1:3);
            kf.x2       = sqrt(input.x0(4));
            kf.rho      = input.rho;
        end
        function x = update(kf,time,measurements)
            dp      = measurements.dp.data;
            v_nb    = measurements.velocity.data;
            x = [];
            if isempty(dp)
                return
            end
            % --- KF update ---
            
            if length(kf.b) < kf.L
                b = 2*dp/kf.rho;
                kf.H_gradient = ...
                    [kf.H_gradient;
                     2*(kf.x1-v_nb);-b];%2*dp*kf.x2/kf.rho];
                kf.H_hessian = ...
                    [kf.H_hessian;
                     eye(3),zeros(3,1);
                     zeros(1,3),0];
                kf.a = [kf.a,v_nb];
                kf.b = [kf.b,dp*2/kf.rho];
            end
%             kf.a = [kf.a,v_nb];
%             kf.b = [kf.b,dp*2/kf.rho];
            if length(kf.b) == kf.L
                C = [-2*kf.a',-kf.b'];
                l = ones(kf.L,1);
                d = zeros(kf.L,1);
                for i = 1:kf.L
                    d(i) = kf.a(:,i)'*kf.a(:,i);
                end
                C_inv = pinv(C);
                c = C_inv*l;
                w = C_inv*d;
                M = eye(4); M(4,4) = 0;
                % c'*M*c*r^2 - h*r + w'*w = 0
                h = (2*w'*M*c+1);
                root = sqrt(h^2-4*(c'*M*c)*(w'*w+3^2*trace(C_inv'*M*C_inv)));                
                
                r1 = (h + root)/2*c'*M*c;
                r2 = (h - root)/2*c'*M*c;
%                 root
%                 c
%                 w
                x_1 = c*r1-w;
                x_2 = c*r2-w;
%                 x_1
%                 x_2
                x = [x_1(1:3);1/sqrt(x_1(4))];
%                 
%                 c.a = kf.a;
%                 c.b = kf.b;
%                 fun = @(x) nlsq_fun(x,c);
%                 x = lsqnonlin(fun,[kf.x1;kf.x2]);
%                 x
%                 
%                 dx = -kf.H_hessian\kf.H_gradient;
%                 dx2 = -[2*(kf.x1-sum(kf.a,2)/kf.L);
%                        -2*kf.x2*sum(kf.b)/(kf.b*kf.b')];
% %                 '----------'
% %                 dx'
% %                 dx2'
%                 Q = blkdiag(kf.L*eye(3),-sum(kf.b));
%                 B = [2*(kf.x1-sum(kf.a,2))',-2*sum(kf.b)*kf.x2];
%                 pinv(Q)*B'
%                 %TODO PRØV MED NLSQ
%                 kf.x1 = kf.x1 ;%+ dx(1:3);
%                 kf.x2 = kf.x2 ;%+ dx(4);
%                 x = [kf.x1;kf.x2^2];
%                 x'
%                 kf.H_hessian = [];
%                 kf.H_gradient = [];
                kf.a = [];
                kf.b = [];
            end
        end
    end
end

