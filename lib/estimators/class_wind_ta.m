classdef class_wind_ta < handle
    %EKF Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        %states
        v_nw;
        gamma;
        
        %Covariance matrices
        Q;
        P;
        R;
        
        %Identity matrix of appropriate size
        I;
        
        %in order to calculate time step
        prev_time;
        
        %enables delayed starting
        start_time = 0;
        
        %air density
        rho;
        
        %remember array for NEW
        v_rem
        p_rem
    end
    
    methods
        function kf = class_wind_ta(input)
            x0          = input.x0;
            kf.v_nw     = x0(1:3); 
            kf.gamma    = x0(4);
            kf.P        = input.P0;
            kf.Q        = input.Q;
            kf.I        = eye(size(input.P0));
            kf.R        = input.R;
            kf.rho      = input.rho;
            if isfield(input,'start_time')
                kf.start_time = input.start_time;
            end
        end
        function x = update(kf,time,measurements)
%             v_abs   = measurements.pitot.data;
            v_nb    = measurements.velocity.data;
            dp      = measurements.dp.data;
            q       = measurements.quaternion.data;
            Rnb     = Rquat(q);
            x = [kf.v_nw;kf.gamma^2];
            if isempty(dp)
                return
            end
            
            v_abs = sqrt(2*dp/1.225);
            
            if kf.start_time ~= -Inf && time < kf.start_time
                return
            else
                kf.start_time = -Inf;
            end
            
            %find dt
            dt = min([.1,time-kf.prev_time]);
            kf.prev_time=time;
            
            %cov time update
            [Phi,Qd] = van_loan(zeros(4),kf.Q,dt);
%             Phi
%             Gamma
            kf.P = Phi*kf.P*Phi' + Qd;
            kf.P = .5*(kf.P+kf.P');
            
            %no state update
            
            % --- KF update ---
            d1 = Rnb*[1;0;0];
            y_hat = d1'*kf.v_nw + v_abs*kf.gamma;
            y = d1'*v_nb;
            H = [d1',v_abs];
            K = kf.P*H'/(H*kf.P*H'+kf.R);
            kf.P = (kf.I-K*H)*kf.P*(kf.I-K*H)'+K*kf.R*K';
            kf.P = (kf.P+kf.P')/2;
            ytilde = y-y_hat;
            xtilde = K*ytilde;            
            kf.v_nw = kf.v_nw + xtilde(1:3);
            kf.gamma = kf.gamma + xtilde(4);

            x = [kf.v_nw;kf.gamma^2];
        end
    end
end

