%% calibrate px to adis


inds = find(acc.t(acc.t < px.acc.t(end)) > px.acc.t(1));
% figure;plot(px.acc.t,px.acc.data(1,:),'r',acc.t(inds),acc.data(1,inds),'b--')

acc_inp = interpolateNd(acc.t(inds),acc.data(:,inds),px.acc.t);
ars_inp = interpolateNd(ars.t(inds),ars.data(:,inds),px.ars.t);
inds_acc = find(~isnan(acc_inp(1,:)));
inds_ars = find(~isnan(ars_inp(1,:)));
c.b = [acc_inp(:,inds_acc),ars_inp(:,inds_ars)*20];
c.x = [px.acc.data(:,inds_acc),px.ars.data(:,inds_ars)*20];

fun = @(x) cal_px(x,c);
x = lsqnonlin(fun,[0,0,0]');

figure
for i = 1:3
    subplot(3,1,i)
    hold on
    plot(acc.t,acc.data(i,:),'r');
    plot(px.acc.t,acc_inp(i,:),'b--')
    plot(px.acc.t,px.acc.data(i,:),'g--')
end

figure
for i = 1:3
    subplot(3,1,i)
    hold on
    plot(ars.t,ars.data(i,:),'r');
    plot(px.ars.t,ars_inp(i,:),'b--')
    plot(px.ars.t,px.ars.data(i,:),'g--')
end