

function diff = cal_px(x,c)
    N = length(c.x);
    R = Rzyx(x(1),x(2),x(3));
    diff = c.b-R*c.x;
    diff = reshape(diff,3*N,1);
end