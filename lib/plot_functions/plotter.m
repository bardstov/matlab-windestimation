function Plotter(pd,input)
    DEG2RAD = pi/180;
    RAD2DEG = 1/DEG2RAD;
    
    do_legends  = 0;
    do_ylabels  = 0;
    do_labels   = 0;
    do_colors   = 0;
    do_save     = 0;
    do_figpos   = 0;
    
    if isfield(input,'legends')
        do_legends = 1;
        legends = input.legends;
    end
    if isfield(input,'ylabels')
        do_ylabels = 1;
        ylabels = input.ylabels;
    end
    if isfield(input,'labels')
        do_labels = 1;
        labels = input.labels;
    end
    if isfield(input,'colors')
        do_colors = 1;
        colors = input.colors;
    end
    if isfield(input,'do_save') && input.do_save
        do_save = input.do_save;
        savefolder = input.savefolder;
        savenames = input.labels;
        savetype = input.savetype;
    end
    if isfield(input,'figpos')
        do_figpos = 1;
        figpos = input.figpos;
    end
    if isfield(input,'savesuffix')
        savesuffix = input.savesuffix;
    else
        savesuffix = '';
    end
    if isfield(input,'linewidth')
        linewidth = input.linewidth;
    else
        linewidth=1.2;
    end
    if isfield(input,'xlims')
        xlims = input.xlims;
    end
    if isfield(input,'ylims')
        ylims = input.ylims;
    end
    if isfield(input,'selected_fields')
        selected_fields = input.selected_fields;
    else
        selected_fields = 1:length(fields(pd));
    end
    if isfield(input,'selected_estimators')
        selected_estimators = input.selected_estimators;
    else
        selected_estimators = 1:length(legends);
    end
    f = fields(pd);
    for j = 1:length(f)
        if ~selected_fields(j)
            continue
        end
        fieldname = f{j};
        datacells = pd.(fieldname);
        K = length(datacells);
        M = 0;
        counter = 1;
        while M == 0 && counter <= K
            try
                M = size(datacells{counter}.data,1);
            catch
                counter = counter + 1;
            end
        end
        if do_figpos
            fig=figure('position',figpos,'Color',[1,1,1]);
        else
            fig=figure;
        end
        for i = 1:M
            ax = subplot(M,1,i);
            hold on;
            if do_legends
                l = legends;
            end
            for k = 1:K
                data = datacells{k};
                if isstruct(data) && selected_estimators(k)
                    if (length(fieldname) >= 2 && strcmp(fieldname(1:2),'ea')) ||...
                       (length(fieldname) >= 5 && strcmp(fieldname(1:5),'b_ars')) ||...
                       (length(fieldname) >= 3 && strcmp(fieldname(1:3),'ang'))
                        data.data = data.data*RAD2DEG;
                    end
                    if do_colors
                        plot(data.t,data.data(i,:),colors{k},'LineWidth',linewidth);
                    else
                        plot(data.t,data.data(i,:),'LineWidth',linewidth);
                    end
                elseif do_legends
                    l(k-(length(legends)-length(l))) = []; %delete cell item
                end
            end
            ax.FontSize = 14;
            if exist('xlims') && ~isempty(xlims{j})
                ax.XLim = xlims{j};
            end
            if exist('ylims') && ~isempty(ylims{j})
                ax.YLim = ylims{j}(i,:);
            end
            if do_ylabels
                ylabel(ylabels{j}{i},'interpreter','latex','fontsize',15,'Interpreter','latex')
            end
            if do_legends
                if isfield(input,'where_legends') && ~input.where_legends{j}(i)
                else
                    legend(l,'interpreter','latex','fontsize',12,'Interpreter','latex');
                end
            end
        end
        xlabel('Time [$s$]','interpreter','latex','fontsize',15,'Interpreter','latex')
        if do_save
            replace(sprintf('%s%s%s%s',savefolder,savenames{j},savesuffix,savetype),' ','-')
            export_fig(replace(sprintf('%s%s%s%s',savefolder,savenames{j},savesuffix,savetype),' ','-'));
        elseif do_labels
            suplabel(labels{j},'t');
        end
    end
end