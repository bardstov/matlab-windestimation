classdef Time < handle
    properties
        output_names = {'time'};
        size_measurement;
    end
    
    methods
        function t = Time()
            t.size_measurement = 1;
        end
        
        function x = update(~,sim)
            x = {sim.time};
        end
        
        function x = get_full_run(~,sim)
            x = cell(1,sim.N);
            for n=1:sim.N
                x{n} = {sim.save_struct.t(n)};
            end
        end
    end
    
end