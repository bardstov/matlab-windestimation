function y = cal_fun(x,c)
    N = size(c.ea,2);
    y = zeros(3*N,1);
    for i = 1:N
        Rnb = Rzyx(c.ea(1,i),c.ea(2,i),c.ea(3,i));
        y(3*i-2:3*i) = c.vel(:,i) - x(1:3) - Rnb*Rzyx(0,x(4),0)*c.rvel(:,i);
    end
end