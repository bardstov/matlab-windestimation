syms a b V p q r fx fy fz 'real'
u = cos(a)*cos(b)*V;
v = sin(b)*V;
w = sin(a)*cos(b)*V;
drelvel = -Smtrx([p,q,r])*[u,v,w]' + [fx;fy;fz];
du = drelvel(1);
dv = drelvel(2);
dw = drelvel(3);
da = simplify((u*dw - du*w)/(u^2+w^2));
