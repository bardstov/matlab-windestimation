function x = time_update(x,inps)
%TIME_UPDATE Summary of this function goes here
%   Detailed explanation goes here
a = x(1);
b = x(2);
V = x(3);
ax = inps.acc.data(1);
ay = inps.acc.data(2);
az = inps.acc.data(3);

p = inps.omg.data(1);
q = inps.omg.data(2);
r = inps.omg.data(3);

phi = inps.ea.data(1);
theta = inps.ea.data(2);

g = inps.g;

ca = cos(a);
sa = sin(a);
cb = cos(b);
sb = sin(b);
tb = sb/cb;
cphi = cos(phi);
sphi = sin(phi);
ctheta = cos(theta);
stheta = sin(theta);


acc = inps.acc.data + [-stheta;ctheta*sphi;ctheta*cphi]*g;
rvel = [ca*cb;sb;sa*cb]*V;
da = q - (p*ca+r*sa)*tb + (g*(cphi*ctheta*ca+stheta*sa)-ax*sa+az*ca)/(V*cb);
db = (-ax*ca*sb + ay*cb - az*sa*sb + g*(stheta*ca*sb + ctheta*sphi*cb - ctheta*cphi*sa*sb))/V + p*sa - r*ca;
dV = rvel'*acc/V;
dx = [da;db;dV];
x = x + inps.dt*dx;
end

