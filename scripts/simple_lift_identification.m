fz = acc.data(3,:);
aoa = ap.aoa.data;
V = px.airspeed.data;
x8 = importdata('../../simulator/x8_param.mat');
h = 1/50;

if XXX == '005'
    tt = 800:h:2500;
    deg_offset = 4.15;
elseif XXX == '007'
    tt = 600:h:1950;
    deg_offset = 4.15;
elseif XXX == '010'
    tt = 550:h:1950;
    deg_offset = 4.55;
end
% px.airspeed.t(1):h:px.airspeed.t(end);

c.fz = interp1(acc.t,fz,tt);
c.aoa = interp1(ap.aoa.t-.05,aoa,tt)-deg_offset*DEG2RAD;
c.V = interp1(px.airspeed.t,V,tt);

lsqfun = @(x) my_fun(x,c);
x = lsqnonlin(lsqfun,[0,4])
%%
x_alt = [x8.C_L_0    2.8306];
figure
hold on;
plot(tt,x(1) + x(2)*c.aoa,'b');
plot(tt,-2*x8.mass*c.fz./(rho*c.V.^2*x8.S_wing),'g');
legend('$-mf_z/\rho V^2 S_{wing}$','$C_{L0} + C_{L\alpha} \alpha$','Interpreter','latex');
suplabel(sprintf('$C_{L0} = %.4f, C_{La}$ = %.2f',x(1),x(2)),'t');

figure;
hold on;
plot(tt,c.aoa*RAD2DEG,'r');
plot(tt,(-2*x8.mass*c.fz./(rho*c.V.^2*x8.S_wing)-x(1))/x(2)*RAD2DEG,'b');
plot(tt,(-2*x8.mass*c.fz./(rho*c.V.^2*x8.S_wing)-x_alt(1))/x_alt(2)*RAD2DEG,'g');
suplabel(sprintf('$C_{L0} = %.4f, C_{La}$ = %.2f',x(1),x(2)),'t');
legend('Aeroprobe $\alpha$\newline lol','$(-mf_z/\rho V^2 S_{wing}-C_{L0})/C_{L\alpha}$');

% x010 = [0.11    2.8306];
% close all
f=figure('Position',[0,0,800,450]);
hold on
scatter(c.aoa*RAD2DEG,-2*x8.mass*c.fz./(rho*c.V.^2*x8.S_wing),'.');
plot([min(c.aoa),max(c.aoa)]*RAD2DEG,x(1) + [min(c.aoa),max(c.aoa)]*x(2),'r');
plot([min(c.aoa),max(c.aoa)]*RAD2DEG,x_alt(1) + [min(c.aoa),max(c.aoa)]*x_alt(2),'g');
legend({'Data points','$C_{L0},C_{L\alpha}$','$\bar{C}_{L0},\bar{C}_{L\alpha}$'},'FontSize',14,'Location','NW');
xlabel(sprintf('Aeroprobe AOA [deg]: Corrected %.2f deg',deg_offset),'FontSize',14);
ylabel('$(-mf_z/\rho V^2 S_{wing}-C_{L0})/C_{L\alpha}$','FontSize',14);
export_fig(f,sprintf('/home/bard/Documents/papersuggestions/modelbased/scatter%s.eps',XXX));
% suplabel(XXX,'t');

function y = my_fun(x,c)
    m = 3.3640;
    S = .75;
    rho = 1.225;
    y =  c.aoa + (x(1) + 2*m*c.fz./(rho*c.V.^2*S))/x(2);
end
