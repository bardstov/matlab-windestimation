close all
addpath('../lib')

DEG2RAD = pi/180;
RAD2DEG = 1/DEG2RAD;

init_noise = 1;
meas_noise = 1;
do_bias = 0;

t_end = Inf; % Inf == all
tstart = 0;

%% --- LOAD DATA ---
tic
ss = importdata('../data.mat');

t_end = min([ss.ea.t(end),t_end]);
lol = find(ss.ea.t >= t_end);
N = lol(1);
t = ss.pos.t(1:N);
h = t(2)-t(1);

%create wind velocity
v_nw.t = t;
v_nw.data = [5;-5;2]*ones(1,N);

%% --- NOISE AND BIAS ---
std_gps     = meas_noise*1e-2;
std_gpsvel  = meas_noise*1e-2;
std_acc     = meas_noise*1e-3;
std_ars     = meas_noise*1e-3;
std_dp      = meas_noise*3;
std_pitot   = meas_noise*.06;
std_mag     = meas_noise*.05;

bias_acc = [.05,-.04,.06]'*do_bias;
bias_ars = [.08,-.06,-.1]'*do_bias;

%% --- SENSORS ---
%ARS
ars_input = ss.ars;
ars_input.data = ss.ars.data + bias_ars*ones(1,size(ss.ars.data,2)) + std_ars*randn(size(ss.ars.data));
ars_input.tag = 'ars';
ars = Measurement(ars_input);
%ACC
acc_input = ss.acc;
acc_input.data = acc_input.data + bias_acc*ones(1,size(acc_input.data,2)) + std_acc*randn(size(acc_input.data));
acc_input.tag = 'acc';
acc = Measurement(acc_input);
%MAG
mag_n = [.134, 9e-3,.478]';
bias_mag = [.1,.2,.3]';
mag_input.t = t;
mag_input.data = zeros(3,N);
mag_input.tag = 'mag';
for i = 1:N
    Rnb = Rzyx(ss.ea.data(1,i),ss.ea.data(2,i),ss.ea.data(3,i));
    mag_input.data(:,i) = Rnb'*mag_n + bias_mag + std_mag*randn(3,1);
end
mag = Measurement(mag_input);
%GPS
f_gps = 1;
t_gps = 0:1/f_gps:t(end)-1/f_gps;

Nantennas = 2;
p_arm1 = [  0.347; -0.010; -0.045];
p_arm2 = [ -0.226;  0.904;  0.000];
p_arm3 = [ -0.285; -0.894;  0.005];
gps1_input.t = t_gps;
gps2_input.t = t_gps;
gps3_input.t = t_gps;
gps1_input.data = zeros(3,length(t_gps));
gps2_input.data = zeros(3,length(t_gps));
gps3_input.data = zeros(3,length(t_gps));

j = 1;
for i = 1:length(t_gps)
    while t(j) < t_gps(i)
        j = j+1;
    end
    Rnb = Rzyx(ss.ea.data(1,j),ss.ea.data(2,j),ss.ea.data(3,j));
    gps1_input.data(:,i) = ss.pos.data(:,j) + Rnb*p_arm1 + std_gps*randn(3,1);
    gps2_input.data(:,i) = ss.pos.data(:,j) + Rnb*p_arm2 + std_gps*randn(3,1);
    gps3_input.data(:,i) = ss.pos.data(:,j) + Rnb*p_arm3 + std_gps*randn(3,1);
end

gps1_input.tag  = 'gps1';
gps1            = Measurement(gps1_input);

gps2_input.tag  = 'gps2';
gps2            = Measurement(gps2_input);

gps3_input.tag  = 'gps3';
gps3    = Measurement(gps3_input);

%Aeroprobe
rvel_n = ss.vel.data - v_nw.data;
aoa.data = zeros(1,N);
ssa.data = zeros(1,N);
Vwb.data  = zeros(1,N);

aoa_std = 1e-2;
ssa_std = 1e-2;
Vwb_std = 1;
for i = 1:N
    Rnb = Rzyx(ss.ea.data(1,i),ss.ea.data(2,i),ss.ea.data(3,i));
    rvel_b = Rnb'*rvel_n(:,i);
    aoa.data(i) = atan2(rvel_b(3),rvel_b(1))+aoa_std*randn;
    Vwb.data(i) = norm(rvel_b)+ssa_std*randn;
    ssa.data(i) = asin(rvel_b(2)/Vwb.data(i))+Vwb_std*randn;
end

%Pitot tube
gamma = 1.1;
f_pitot = 5;
t_pitot = 0:1/f_pitot:t(end)-1/f_pitot;
pitot_input.t = t_pitot;
pitot_input.data = zeros(1,length(t_pitot));
dp_input.data = zeros(1,length(t_pitot));
dp_input.t = t_pitot;
j = 1;
rho = 1.225;
for i = 1:length(t_pitot)
    while t(j) < t_pitot(i)
        j = j+1;
    end
    dp_input.data(i) = gamma*(norm(ss.vel.data(:,j)-v_nw.data(:,j)))^2*rho/2 + std_dp*randn;
    truedp(i) = gamma*(norm(ss.vel.data(:,j)-v_nw.data(:,j)))^2*rho/2;
    truepitot(i) = sqrt(truedp(i)*2/rho);
    pitot_input.data(i) = sqrt(dp_input.data(i)*2/rho);
end
pitot_input.tag = 'pitot';
pitot = Measurement(pitot_input);

% figure;
% hist(dp_input.data-truedp,20);
% figure
% hist(pitot_input.data-truepitot,20);

dp_input.tag = 'dp';
dp = Measurement(dp_input);

%% --- Inputs ---
%G_N
g_input.output_names = {'g_n'};
g_input.data = [0;0;9.81];
g_n = Input(g_input);
%Time
time = Time();
%ea
ea_input = ss.ea;
ea_input.output_names = {'ea'};
ea = Input(ea_input);

%g
inps.g = 9.81;

%% 
x = zeros(3,N);
last_percent = 0;
P = zeros(3,3,N);
x(:,1) = [0;0;18];
P(:,:,1) = eye(3);

Q = blkdiag(1e-2*eye(2),1);
R = blkdiag(1e-2*eye(2),1);
t_i = 0;
for i = 2:N
    if i/(N/100) > last_percent
        s = num2str(last_percent);
        last_percent = last_percent + 1;
        fprintf('Done with %s%s: Time elapsed %.2f%s',s,'%',toc,char(10));
    end
    inps.dt = t(i)-t_i;
    t_i = t(i);
    inps.ea = ea.data(:,i);
    inps.omg = ars.data(:,i);
    inps.acc = acc.data(:,i);
    
    fstate = @(x) time_update(x,inps);
    hmeas = @(x) measurement_update(x);
    
    z = [aoa.data(i);ssa.data(i);Vwb.data(i)];
    [x(:,i),P(:,:,i)] = ukf_update(fstate,x(:,i-1),P(:,:,i-1),hmeas,z,Q,R);
end

%% Plotting
%states
figure
subplot(3,1,1)
hold on
plot(t,aoa.data,'r')
plot(t,x(1,:),'b')
subplot(3,1,2)
hold on
plot(t,ssa.data,'r')
plot(t,x(2,:),'b')
subplot(3,1,3)
hold on
plot(t,Vwb.data,'r')
plot(t,x(3,:),'b')

%covariances
figure
for i = 1:3
    subplot(3,1,i)
    hold on
    Ptemp = reshape(P(i,i,:),[1,N]);
    plot(Ptemp,'r')
end