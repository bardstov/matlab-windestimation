clear
XXXs = {'005','007','010'};
x8 = importdata('../../simulator/x8_param.mat');
h = 1/50;
M = length(XXXs);
xs = cell(M,1);
cs = cell(M,1);
tt{1} = 800:h:2500;
deg_offset{1} = 4.15;
tt{2} = 600:h:1950;
deg_offset{2} = 4.15;
tt{3} = 550:h:1950;
deg_offset{3} = 4.55;
for ix = 1:M
    clearvars -except XXXs x8 h M xs cs ix tt deg_offset
    XXX = XXXs{ix};
    fprintf('Dataset %s\n',XXX);
    load_exp_measurements;
    fz = acc.data(3,:);
    aoa = ap.aoa.data;
    V = px.airspeed.data;
    
    c.fz = interp1(acc.t,fz,tt{ix});
    c.aoa = interp1(ap.aoa.t-.05,aoa,tt{ix})-deg_offset{ix}*DEG2RAD;
    c.V = interp1(px.airspeed.t,V,tt{ix});
    cs{ix} = c;
    lsqfun = @(x) my_fun(x,c);
    xs{ix} = lsqnonlin(lsqfun,[0,4]);
    fprintf('\t C_L_0 = %.4f \t C_L_a = %.4f\n\n',xs{ix}(1),xs{ix}(2));
end

%%
ix = 3;
% figure
% hold on;
% plot(tt{ix},xs{ix}(1) + xs{ix}(2)*cs{ix}.aoa,'b');
% plot(tt{ix},-2*x8.mass*cs{ix}.fz./(rho*cs{ix}.V.^2*x8.S_wing),'g');
% legend('$-mf_z/\rho V^2 S_{wing}$','$C_{L0} + C_{L\alpha} \alpha$','Interpreter','latex');
% suplabel(sprintf('$C_{L0} = %.4f, C_{La}$ = %.2f',xs{ix}(1),xs{ix}(2)),'t');
% 
% figure;
% hold on;
% plot(tt{ix},cs{ix}.aoa*RAD2DEG,'r');
% plot(tt{ix},(-2*x8.mass*cs{ix}.fz./(rho*cs{ix}.V.^2*x8.S_wing)-xs{ix}(1))/xs{ix}(2)*RAD2DEG,'b');
% % plot(tt,(-2*x8.mass*cs{ix}.fz./(rho*cs{ix}.V.^2*x8.S_wing)-x_alt(1))/x_alt(2)*RAD2DEG,'g');
% suplabel(sprintf('$C_{L0} = %.4f, C_{La}$ = %.2f',xs{ix}(1),xs{ix}(2)),'t');
% legend('Aeroprobe $\alpha$\newline lol','$(-mf_z/\rho V^2 S_{wing}-C_{L0})/C_{L\alpha}$');

% x010 = [0.11    2.8306];
% close all
f=figure('Position',[0,0,800,450]);
hold on
scatter(cs{ix}.aoa*RAD2DEG,-2*x8.mass*cs{ix}.fz./(rho*cs{ix}.V.^2*x8.S_wing),'.');
plot([min(cs{ix}.aoa),max(cs{ix}.aoa)]*RAD2DEG,xs{ix}(1) + [min(cs{ix}.aoa),max(cs{ix}.aoa)]*xs{ix}(2),'r');
% plot([min(cs{ix}.aoa),max(cs{ix}.aoa)]*RAD2DEG,x_alt(1) + [min(cs{ix}.aoa),max(cs{ix}.aoa)]*x_alt(2),'g');
legend({'Data points','$C_{L0},C_{L\alpha}$','$\bar{C}_{L0},\bar{C}_{L\alpha}$'},'FontSize',14,'Location','NW');
xlabel(sprintf('Aeroprobe AOA [deg]: Corrected %.2f deg',deg_offset{ix}),'FontSize',14);
ylabel('$(-mf_z/\rho V^2 S_{wing}-C_{L0})/C_{L\alpha}$','FontSize',14);
xlim([-2,10])
ylim([-.1,.6])
% export_fig(f,sprintf('/home/bard/Documents/papersuggestions/modelbased/scatter%s.eps',XXX));
% suplabel(XXX,'t');
f=figure('Position',[0,0,800,450]);
hold on
clrs = 'rgb';
for ix = [3,2,1]
    scatter(cs{ix}.aoa*RAD2DEG,-2*x8.mass*cs{ix}.fz./(rho*cs{ix}.V.^2*x8.S_wing),'MarkerEdgeColor',clrs(ix),'Marker','.');
    plot([min(cs{ix}.aoa),max(cs{ix}.aoa)]*RAD2DEG,xs{ix}(1) + [min(cs{ix}.aoa),max(cs{ix}.aoa)]*xs{ix}(2),'color',clrs(ix));
    xlabel(sprintf('Aeroprobe AOA [deg]: Corrected %.2f deg',deg_offset{ix}),'FontSize',14);
    ylabel('$(-mf_z/\rho V^2 S_{wing}-C_{L0})/C_{L\alpha}$','FontSize',14);
    xlim([-2,10])
    ylim([-.1,.6])
end

%%



function y = my_fun(x,c)
    m = 3.3640;
    S = .75;
    rho = 1.225;
    y =  c.aoa + (x(1) + 2*m*c.fz./(rho*c.V.^2*S))/x(2);
end
