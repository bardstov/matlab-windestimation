function csubplotter(cellarray)
%SUBPLOTTER Summary of this function goes here
%   Detailed explanation goes here
    Nrows = size(cellarray{1}.data,1);
    Ncells = length(cellarray);
    figure;
    clrs = 'rgb';
    for j = 1:Nrows
        ax = subplot(Nrows,1,j);
        hold on
        tags = {};
        for i = 1:Ncells
            plot(cellarray{i}.t,cellarray{i}.data(j,:),clrs(i));
            tags{i} = cellarray{i}.tag;
        end
        legend(tags);
    end
end

