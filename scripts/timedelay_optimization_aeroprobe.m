%%
load_exp_measurements

%%

mekf = importdata(sprintf('../data/mekf_%s.mat',XXX));

%%
% tv = gps.vel.t;
% v_nb = gps.vel.data;
tv = mekf.t;
v_nb = mekf.vel;
if strcmp(XXX,'005')
    tstart = 325;
    tend = 2500;
elseif strcmp(XXX,'007')
    tstart = 460;
    tend = 2400;
elseif strcmp(XXX,'010')
    tstart = 550;
    tend = 1930;
end
vinds = find(tv>tstart & tv < tend);
tv = tv(vinds);
v_nb = mekf.vel(:,vinds);
q_nb = mekf.q(:,vinds);
N = length(tv);

fun = @(x)(lsqfun(x,ap.rvel.t,ap.rvel.data,tv,v_nb,q_nb));
x = lsqnonlin(fun,0);
%%
% figure;
% subplotter(ap.rvel.t,ap.rvel.data,[])
% subplotter(tv,v_wb,[])
% figure;
% subplotter(mekf.t,mekf.q,[])
% subplotter(tv,q_nb,[])
v_wb_orig = interpolateNd(ap.rvel.t,ap.rvel.data,tv);
v_nw_orig = zeros(3,N);
for i = 1:N
    R_nb = Rquat(q_nb(:,i));
    v_nw_orig(:,i) = v_nb(:,i) - R_nb*v_wb_orig(:,i);
end
for j = -1:.1:1
    v_wb = interpolateNd(ap.rvel.t-j,ap.rvel.data,tv);
    v_nw = zeros(3,N);

    for i = 1:N
        R_nb = Rquat(q_nb(:,i));
        v_nw(:,i) = v_nb(:,i) - R_nb*v_wb(:,i);
    end
    pin.legends = {sprintf('original %.1f',j),sprintf('corrected %.1f',j)};
    figure
    subplotter(tv,v_nw_orig,pin);
    subplotter(tv,v_nw,pin);
    j
end


%%
function y = lsqfun(x,tap,v_wb,tmekf,v_nb,q_nb)
    fprintf('x: %f\n',x);
    v_wb = interpolateNd(tap-x,v_wb,tmekf);
%     for i = 1:N
%         q_nb(:,i) = q_nb(:,i)/norm(q_nb(:,i));
%     end
    v_nw = zeros(size(v_nb));
    for i = 1:size(v_nw,2)
        R_nb = Rquat(q_nb(:,i)/norm(q_nb(:,i)));
        v_nw(:,i) = v_nb(:,i) - R_nb*v_wb(:,i);
    end
    y = var(v_nw(1,:)) + var(v_nw(2,:)) + var(v_nw(3,:));
    fprintf('y: %f\n',y)
end