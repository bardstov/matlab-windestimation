classdef class_wind_opt < handle
    %EKF Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        %States
        x = [0;0;0;1];
        
        %Window length
        L;
        
        %Save vectors
        a = [];
        b = [];
        
        %air density
        rho
        
        %lsqnonlin options
        options = optimset('display','off','TolFun',1e-10);
    end
    
    methods
        function kf = class_wind_opt(input)
            kf.L        = input.window_length;
            kf.x        = input.x0;
            kf.rho      = input.rho;
        end
        function ret = update(kf,time,measurements)
            dp      = measurements.dp.data;
            v_nb    = measurements.velocity.data;
            if isempty(dp)
                ret = kf.x;
                return
            end
            % --- KF update ---
            
            if length(kf.b) < kf.L
                kf.a = [kf.a,v_nb];
                kf.b = [kf.b,dp*2/kf.rho];
                ret = kf.x;
            end
%             bi = dp*2/kf.rho;
%             dh = [2*(kf.x(1:3)-v_nb); -2*bi*kf.x(4)];
%             H  = blkdiag(2*eye(3),-2*bi);
%             kf.x = kf.x - pinv(H'*H)*H'*dh;
%             x = kf.x;
            if length(kf.b) == kf.L
%                 dh = [];
%                 H = [];
%                 for i = 1:kf.L
%                     dh = [dh;2*(kf.x(1:3)-kf.a(:,i)); -2*kf.b(i)*x(4)];
%                     H  = [H;blkdiag(2*eye(3),-2*kf.b(i))];
%                 end
%                 dh = [2*sum(kf.a,2);2*sum(kf.b)];
%                 H = blkdiag(2*kf.L*eye(3),2*sum(kf.b));
%                 '----------'
%                 x = -pinv(H'*H)*H'*dh
                c.a = kf.a;
                c.b = kf.b;
                fun = @(x) nlsq_fun(x,c);
                x = lsqnonlin(fun,kf.x,[],[],kf.options);
                kf.x = [x(1:3);x(4)];
                ret = kf.x;
                kf.a = [];
                kf.b = [];
            end
        end
    end
end

