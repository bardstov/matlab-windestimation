function y = nlsq_fun(x,c)
    a = c.a;
    b = c.b;
    L = length(b);
    y = zeros(1,L);
    for i = 1:L
        y(i) = x(4)*norm(x(1:3)-a(:,i))^2 - b(i);
    end
end