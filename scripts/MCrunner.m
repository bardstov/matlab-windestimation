clear all; close all;
Nmc = 50;
dt = datetime;
dt.Format = 'uuuu-MM-dd--HH:mm:ss';
dt = char(dt);
folder = ['~/data/ecc2019/' dt '/'];
mkdir(folder);
for i_antennas = 1:3
    pds = cell(Nmc,1);
    eds = cell(Nmc,1);
    for i_mc = 1:Nmc
        str = sprintf(':::: \t MC run %d/%d \t Nantennas = %d ::::',i_mc,Nmc,i_antennas);
        disp(str);
        Nantennas = i_antennas;
        runner
        pds{i_mc} = pd;
        eds{i_mc} = ed;
        clearvars -except pds eds Nmc i_mc i_antennas folder 
    end
    save(sprintf([folder 'pds-%d.mat'],i_antennas),'pds');
    save(sprintf([folder 'eds-%d.mat'],i_antennas),'eds');
    clearvars pds eds
end