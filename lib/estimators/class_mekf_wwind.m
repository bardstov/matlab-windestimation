classdef class_mekf_wwind < handle
    %EKF Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        %states
        p;
        v;
        b_acc;
        q;
        b_ars;
        v_nw;
        gamma;
        
        %air density
        rho;
        
        %Covariance matrices
        Q;
        P;
        R_gps;
        R_gpsvel;
        R_pitot;
        %Arms
        Nantennas;
        p_arm1;
        p_arm2;
        p_arm3;
        
        %Sets maximal update interval for P
        max_P_update_interval;
        
        %Stores previous P update time
        P_prev_time = 0;
        
        %Stores previous time instance
        prev_time = 0;
        
        %Stores previous acc and ars measururements
        prev_acc=zeros(3,1);
        prev_ars=zeros(3,1);
        
        %Identity matrix of n x n
        I;
        
        %gravity
        g = [0;0;9.81];
        
        %time step
        time_step;
    end
    
    methods
        function kf = class_mekf_wwind(input)
            x0                          = input.x0;
            kf.p                        = x0(1:3); 
            kf.v                        = x0(4:6);
            kf.b_acc                    = x0(7:9);
            kf.q                        = x0(10:13);
            kf.b_ars                    = x0(14:16);
            kf.v_nw                     = x0(17:19);
            kf.gamma                    = x0(20);
            kf.p_arm1                   = input.p_arm1;
            kf.p_arm2                   = input.p_arm2;
            kf.p_arm3                   = input.p_arm3;
            kf.Nantennas                = input.Nantennas;
            kf.P                        = input.P0;
            kf.Q                        = input.Q;
            kf.I                        = eye(size(input.P0));
            kf.R_gps                    = input.R_gps;
            kf.R_gpsvel                 = input.R_gpsvel;
            kf.R_pitot                  = input.R_pitot;
            kf.rho                      = input.rho;
            kf.max_P_update_interval    = input.max_P_update_interval;
            if isfield(input,'g')
                kf.g = input.g;
            end
            if isfield(input,'time_step')
                kf.time_step = input.time_step;
            end
            Rnb = Rquat(kf.q);
            kf.prev_acc = -Rnb'*[0;0;9.81];
        end
%         function x = update(kf,measurements,input)
        function x = update(kf,time,measurements)
            new_gps = [kf.Nantennas >= 1 && ~isempty(measurements.gps1.data);
                       kf.Nantennas >= 2 && ~isempty(measurements.gps2.data);
                       kf.Nantennas >= 3 && ~isempty(measurements.gps3.data)];
            new_gpsvel = [kf.Nantennas >= 1 && ~isempty(measurements.gpsvel1.data);
                          kf.Nantennas >= 2 && ~isempty(measurements.gpsvel2.data);
                          0];
%             new_pitot = ~isempty(measurements.pitot.data);
            new_pitot = ~isempty(measurements.dp.data);
            I3 = eye(3);
            z3 = zeros(3);
            z31 = zeros(3,1);
            z13 = z31';
            
            H = [];
            y = [];
            y_hat = [];
            R = [];
            
            %find dt
            if kf.time_step > 0
                dt = kf.time_step;
            else
                dt = min([.1,time-kf.prev_time]);
                kf.prev_time=time;
            end
            %cov time update
            Rnb = Rquat(kf.q);
            ars_hat = kf.prev_ars - kf.b_ars;
            fb_hat = kf.prev_acc - kf.b_acc;
            F_p_v  = I3;
            F_v_bacc = -Rnb;
            F_v_u = -Rnb*Smtrx(fb_hat);
            F_u_u = -Smtrx(ars_hat);
            F_u_bars = -I3;
            F = [ z3, F_p_v,       z3,    z3,       z3;
                  z3,    z3, F_v_bacc, F_v_u,       z3;
                  z3,    z3,       z3,    z3,       z3;
                  z3,    z3,       z3, F_u_u, F_u_bars;
                  z3,    z3,       z3,    z3,       z3];
            F = blkdiag(F,zeros(4));
            G_v = -Rnb;
            G_bacc = I3;
            G_ars = -I3;
            G_bars = I3;
            G_wind = I3;
            G_gamma = 1;
            G = [   z3,     z3,    z3,     z3;
                   G_v,     z3,    z3,     z3;
                    z3, G_bacc,    z3,     z3;
                    z3,     z3, G_ars,     z3;
                    z3,     z3,    z3, G_bars];
            G = blkdiag(G,G_wind,G_gamma);
%             kf.F_next = F;
%             kf.G_next = G;
%             size(F)
%             size(G)
%             size(kf.Q)
            [Phi,Qd] = van_loan(F,G*kf.Q*G',dt);
            kf.P = Phi*kf.P*Phi'+Qd;
            kf.P = .5*(kf.P+kf.P');
            
            %state time update
            dvn = Rnb*fb_hat + kf.g;
            kf.p = kf.p + dt*kf.v;
            kf.v = kf.v + dt*dvn;
            kf.q = kf.q + dt*.5*qmult(kf.q,[0;ars_hat]);
            kf.q = kf.q/norm(kf.q);
            Rnb = Rquat(kf.q);
            
            %measurement update
            if ~isempty(measurements.acc.data)
                kf.prev_acc = measurements.acc.data;
            end
            if ~isempty(measurements.ars.data)
                kf.prev_ars = measurements.ars.data;
            end
            for i = 1:kf.Nantennas
                if new_gps(i)
                    meas = eval(sprintf('measurements.gps%i.data',i));
                    p_b_arm = eval(sprintf('kf.p_arm%i',i));
                    H_p = I3;
                    H_ub = -Rnb*Smtrx(p_b_arm);
                    H = [H;H_p,z3,z3,H_ub,z3,z3,z31];
                    y = [y;meas];
                    y_hat = [y_hat;kf.p+Rnb*p_b_arm];
                    R = blkdiag(R,kf.R_gps);
                end
            end
            for i = 1:kf.Nantennas
                if new_gpsvel(i)
                    meas = eval(sprintf('measurements.gpsvel%i.data',i));
                    p_b_arm = eval(sprintf('kf.p_arm%i',i));
                    H_v = I3;
                    H_ub = Rnb*Smtrx(Smtrx(p_b_arm)*ars_hat);
                    H_b = Rnb*Smtrx(p_b_arm);
                    H = [H;z3,H_v,z3,H_ub,H_b,z3,z31];
                    y = [y;meas];
                    y_hat = [y_hat;kf.v - Rnb*Smtrx(p_b_arm)*ars_hat];
                    R = blkdiag(R,kf.R_gpsvel);                    
                end
            end
            if new_pitot
                %<<< OLD
%                 meas = measurements.pitot.data;
%                 relvel = (kf.v-kf.v_nw);
%                 relvel_n = norm(relvel);
%                 pitot_hat = kf.gamma*relvel_n;
%                 H_pitot = [-kf.gamma*(relvel)'/relvel_n, relvel_n];
                % OLD >>>
                %<<< NEW
                meas = measurements.dp.data;
                relvel = kf.v-kf.v_nw;
                pitot_hat = .5*kf.rho*kf.gamma*(relvel'*relvel);
                H_pitot = kf.rho*[-kf.gamma*(relvel)', .5*(relvel'*relvel)];
                % NEW >>>
                H = [H;z13,z13,z13,z13,z13,H_pitot];
                y = [y;meas];
                y_hat = [y_hat;pitot_hat];
                R = blkdiag(R,kf.R_pitot);
            end
            if sum([new_gps;new_gpsvel;new_pitot])
                % --- KF update ---
%                 size(kf.P)
%                 size(H)
%                 size(R)
                K = kf.P*H'/(H*kf.P*H'+R);
                kf.P = (kf.I-K*H)*kf.P*(kf.I-K*H)'+K*R*K';
                kf.P = (kf.P+kf.P')/2;
                ytilde = y-y_hat;
                xtilde = K*ytilde;

                kf.p = kf.p + xtilde(1:3);
                kf.v = kf.v + xtilde(4:6);
                kf.b_acc = kf.b_acc + xtilde(7:9);
                kf.b_ars = kf.b_ars + xtilde(13:15);
                kf.v_nw = kf.v_nw + xtilde(16:18);
                kf.gamma = kf.gamma + xtilde(19);
                %attitude update
                du = xtilde(10:12);
                dq = [16-du'*du; 8*du]/(16+du'*du);
                kf.q = qmult(kf.q,dq); 
                kf.q = kf.q/norm(kf.q);
            end
            x = [kf.p;kf.v;kf.b_acc;kf.q;kf.b_ars;kf.v_nw;kf.gamma];
        end
    end
end

