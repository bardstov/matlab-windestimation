classdef class_mekf < handle
    %EKF Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        %states
        p;
        v;
        b_acc;
        q;
        b_ars;
        b_mag;
        
        %magnet field NED
        mag_n;
        
        %Covariance matrices
        Q;
        P;
        R_gps;
        R_gpsvel;
        R_mag;
        %Arms
        Nantennas;
        p_arm1;
        p_arm2;
        p_arm3;
        
        %Sets maximal update interval for P
        max_P_update_interval;
        
        %Stores previous P update time
        P_prev_time = 0;
        
        %Stores previous time instance
        prev_time = 0;
        
        %Stores previous acc and ars measururements
        prev_acc=zeros(3,1);
        prev_ars=zeros(3,1);
        
        %Identity matrix of n x n
        I;
        
        %gravity
        g = [0;0;9.81];
        
        %time step
        time_step;
    end
    
    methods
        function kf = class_mekf(input)
            x0                          = input.x0;
            kf.p                        = x0(1:3); 
            kf.v                        = x0(4:6);
            kf.b_acc                    = x0(7:9);
            kf.q                        = x0(10:13);
            kf.b_ars                    = x0(14:16);
            kf.b_mag                    = zeros(3,1);
            kf.p_arm1                   = input.p_arm1;
            kf.p_arm2                   = input.p_arm2;
            kf.p_arm3                   = input.p_arm3;
            kf.Nantennas                = input.Nantennas;
            kf.mag_n                    = input.mag_n;
            kf.P                        = input.P0;
            kf.Q                        = input.Q;
            kf.I                        = eye(size(input.P0));
            kf.R_gps                    = input.R_gps;
            kf.R_gpsvel                 = input.R_gpsvel;
            kf.R_mag                    = input.R_mag;
            kf.max_P_update_interval    = input.max_P_update_interval;
            if isfield(input,'g')
                kf.g = input.g;
            end
            if isfield(input,'time_step')
                kf.time_step = input.time_step;
            end
            Rnb = Rquat(kf.q);
            kf.prev_acc = -Rnb'*[0;0;9.81];
        end
%         function x = update(kf,measurements,input)
        function x = update(kf,time,measurements)
            new_gps = [kf.Nantennas >= 1 && ~isempty(measurements.gps1.data);
                       kf.Nantennas >= 2 && ~isempty(measurements.gps2.data);
                       kf.Nantennas >= 3 && ~isempty(measurements.gps3.data)];
            new_gpsvel = [kf.Nantennas >= 1 && isfield(measurements,'gpsvel1') && ~isempty(measurements.gpsvel1.data);
                          kf.Nantennas >= 2 && isfield(measurements,'gpsvel2') && ~isempty(measurements.gpsvel2.data);
                          0];
            new_mag = ~isempty(measurements.mag.data);
            I3 = eye(3);
            z3 = zeros(3);
            
            H = [];
            y = [];
            y_hat = [];
            R = [];
            
            %find dt
            if kf.time_step > 0
                dt = kf.time_step;
            else
                dt = min([.1,time-kf.prev_time]);
                kf.prev_time=time;
            end
            %cov time update
            Rnb = Rquat(kf.q);
            ars_hat = kf.prev_ars - kf.b_ars;
            fb_hat = kf.prev_acc - kf.b_acc;
            F_p_v  = I3;
            F_v_bacc = -Rnb;
            F_v_u = -Rnb*Smtrx(fb_hat);
            F_u_u = -Smtrx(ars_hat);
            F_u_bars = -I3;
            F = [ z3, F_p_v,       z3,    z3,       z3;
                  z3,    z3, F_v_bacc, F_v_u,       z3;
                  z3,    z3,       z3,    z3,       z3;
                  z3,    z3,       z3, F_u_u, F_u_bars;
                  z3,    z3,       z3,    z3,       z3];
            F = blkdiag(F,z3);
            G_v = -Rnb;
            G_bacc = I3;
            G_ars = -I3;
            G_bars = I3;
            G = [   z3,     z3,    z3,     z3;
                   G_v,     z3,    z3,     z3;
                    z3, G_bacc,    z3,     z3;
                    z3,     z3, G_ars,     z3;
                    z3,     z3,    z3, G_bars];
            G = blkdiag(G,I3);
%             size(F)
%             size(G)
%             size(kf.Q)
            [Phi,Qd] = van_loan(F,G*kf.Q*G',dt);
            kf.P = Phi*kf.P*Phi'+Qd;
            kf.P = .5*(kf.P+kf.P');
            
            %state time update
            dvn = Rnb*fb_hat + kf.g;
            kf.p = kf.p + dt*kf.v;
            kf.v = kf.v + dt*dvn;
            kf.q = kf.q + dt*.5*qmult(kf.q,[0;ars_hat]);
            kf.q = kf.q/norm(kf.q);
            Rnb = Rquat(kf.q);
            
            %measurement update
            if ~isempty(measurements.acc.data)
                kf.prev_acc = measurements.acc.data;
            end
            if ~isempty(measurements.ars.data)
                kf.prev_ars = measurements.ars.data;
            end
            for i = 1:kf.Nantennas
                if new_gps(i)
                    meas = eval(sprintf('measurements.gps%i.data',i));
                    p_b_arm = eval(sprintf('kf.p_arm%i',i));
                    H_p = I3;
                    H_ub = -Rnb*Smtrx(p_b_arm);
                    H = [H;H_p,z3,z3,H_ub,z3,z3];
                    y = [y;meas];
                    y_hat = [y_hat;kf.p+Rnb*p_b_arm];
                    R = blkdiag(R,kf.R_gps);
                end
            end
            for i = 1:kf.Nantennas
                if new_gpsvel(i)
                    meas = eval(sprintf('measurements.gpsvel%i.data',i));
                    p_b_arm = eval(sprintf('kf.p_arm%i',i));
                    H_v = I3;
                    H_ub = Rnb*Smtrx(Smtrx(p_b_arm)*ars_hat);
                    H_b = Rnb*Smtrx(p_b_arm);
                    H = [H;z3,H_v,z3,H_ub,H_b,z3];
                    y = [y;meas];
                    y_hat = [y_hat;kf.v+Rnb*Smtrx(ars_hat)*p_b_arm];
                    R = blkdiag(R,kf.R_gpsvel);                    
                end
            end
            if new_mag*0
                y = [y;measurements.mag.data];
                mb_hat = Rnb'*kf.mag_n+kf.b_mag;
                y_hat = [y_hat;mb_hat];
                H_ub = Smtrx(mb_hat);
                H = [H;z3,z3,z3,H_ub,z3,I3];
                R = blkdiag(R,kf.R_mag);
            end
            if sum([new_gps;0*new_mag])
                % --- KF update ---
%                 size(kf.P)
%                 size(H)
%                 size(R)
                K = kf.P*H'/(H*kf.P*H'+R);
                kf.P = (kf.I-K*H)*kf.P*(kf.I-K*H)'+K*R*K';
                kf.P = (kf.P+kf.P')/2;
%                 y
%                 y_hat
                ytilde = y-y_hat;
                xtilde = K*ytilde;

                kf.p = kf.p + xtilde(1:3);
                kf.v = kf.v + xtilde(4:6);
                kf.b_acc = kf.b_acc + xtilde(7:9);
                kf.b_ars = kf.b_ars + xtilde(13:15);
                kf.b_mag = kf.b_mag + xtilde(16:18);
                %attitude update
                du = xtilde(10:12);
                dq = [16-du'*du; 8*du]/(16+du'*du);
                kf.q = qmult(kf.q,dq); 
                kf.q = kf.q/norm(kf.q);
            end
            x = [kf.p;kf.v;kf.b_acc;kf.q;kf.b_ars;kf.b_mag];
        end
    end
end

