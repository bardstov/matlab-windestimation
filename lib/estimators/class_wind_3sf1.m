classdef class_wind_3sf1 < handle
    %EKF Summary of this class goes here
    %   Detailed explanation goes here
    
    properties  
        %State
        x
        
        %Window length
        L;
        
        %Save vectors
        a = [];
        b = [];
        
        %Gradient and Hessian arrays
        
        %air density
        rho
        
        %
        R;
        
        %
        O = zeros(4,4);
        O_counter = 0;
        e_prev = zeros(4,1);
    end
    
    methods
        function kf = class_wind_3sf1(input)
            kf.L        = input.window_length;
            kf.rho      = input.rho;
            kf.x        = input.x0;
        end
        function ret = update(kf,time,measurements)
            dp      = measurements.dp.data;
            v_nb    = measurements.velocity.data;
            ret.x   = [kf.x];
            ret.C   = [];
            ret.y   = [];
            if isempty(dp)
                return
            end
            % --- KF update ---
            kf.a = [kf.a,v_nb];
            kf.b = [kf.b,dp*2/kf.rho];
            
%             go_on = 0;
%             kf.O_counter = kf.O_counter + 1;
%             kf.O = kf.O + [kf.a(:,end);kf.b(end)]*[kf.a(:,end)',kf.b(end)];
%             e = eig(kf.O/sqrt(kf.O_counter));
%             if min(e) > 50 && sum(e>kf.e_prev) > 0 ...
%                     || length(kf.b) == kf.L
%                 e
%                 if length(kf.b) == kf.L
%                     disp('lol: ')
%                 end
%                 go_on = 1;
%                 kf.O_counter = 0;
%                 kf.O = 0*kf.O;
%             end
%             kf.e_prev = e;
            
            if length(kf.b) == kf.L
                Lx = length(kf.b);
                kf.a = [kf.a(:,2:end),v_nb];
                kf.b = [kf.b(2:end),dp*2/kf.rho];
                C = [2*kf.a',kf.b'];
                l = ones(Lx,1);
                d = zeros(Lx,1);
                for i = 1:Lx
                    d(i) = kf.a(:,i)'*kf.a(:,i);
                end
                [v,i] = min(eig(C'*C));
                C_inv = pinv(C);
                c = C_inv*l;
                w = C_inv*d;
                M = eye(4); M(4,4) = 0;
                % c'*M*c*r^2 - h*r + w'*w = 0
                q1 = c'*M*c;
                q2 = 2*w'*M*c-1;
                q3 = w'*w + kf.L*3^2*trace(C_inv'*M*C_inv);
%                 q2^2-4*q1*q3
                r1 = (-q2+sqrt(q2^2-4*q1*q3))/(2*q1);
                r2 = (-q2-sqrt(q2^2-4*q1*q3))/(2*q1);
                x1 = c*r1+w;
                x2 = c*r2+w;
                if abs(x1(4)-1) < abs(x2(4)-1)
                    r_correct = r1;
                    x_correct = x1;
                else
                    r_correct = r2;
                    x_correct = x2;
                end
%                 if q2^2-4*q1*q3 < 0
%                     '3sf1 < 0'
%                     kf.a = [];
%                     kf.b = [];
%                     ret.x = kf.x;
%                     ret.C = C*0;
%                     ret.y = l*0;
%                     return 
%                 end
                kf.x = [x_correct(1:3);1/x_correct(4)];
                ret.x   = kf.x;
                X=[x1,x2,x_correct];
%                 X= [X(1:3,:);1./X(4,:)]
                ret.C = C;
                ret.y = r_correct*l+d;
                kf.a = [];
                kf.b = [];
            end
        end
    end
end

