%% Wind estimation script
% Author Bård Nagy Stovner
load_exp_measurements
close all
do_save = 0;

addlib
fprintf('Running WIND estimation with data set number %s with estimators:\n',XXX);
% if exist('gpstimeshift')
%     mekf = importdata(sprintf('../data/mekf_%s_%.2f.mat',XXX,gpstimeshift(idt)));
% else
    mekf = importdata(sprintf('../data/mekf_%s.mat',XXX));
% end
x8 = importdata('../../simulator/x8_param.mat');
t = mekf.t;
t_start = t(1);
t_end = t(end);
N = length(t);
I3 = eye(3);

inds = find(ars.t >= t_start & ars.t <= t_end);

if strcmp(XXX,'005')
    GTw = [-1.3760;2.1381;-0.6576];
    GTg = 0.7192;
elseif strcmp(XXX,'007')
    GTw = [-1.3554;1.8022;0.1937];
    GTg = 0.7045;
elseif strcmp(XXX,'010')
    GTw = [-1.3554;1.8022;0.1937];
    GTg = 0.7045;
end
%% Initialize filters
std_pitot   = .5;
std_dp      = 15;

std_ars     = 5e-3;
std_acc     = 5e-2;
Q_acc       = I3*std_acc^2;
Q_ars       = I3*std_ars^2;

Rnb0 = Rquat(mekf.q(:,1));

%Measurement covariance
R_speed     = std_pitot^2; % airspeed measurements from Pitot_static tube
R_dp        = std_dp^2; %dynamic pressure measurements from Pitot-static tube
%Process covariance
Q_v         = blkdiag(1e-5,1e-5,1e-6);
Q_gamma     = 1e-7;
Q           = blkdiag(Q_v,Q_gamma);
%Initial covariance
P0_wind     = blkdiag(1e-3,1e-3,1e-5);
P0_gamma    = blkdiag(1e-7);
%Initial estiamtes
v_nw0       = [0;0;0];
gamma0      = 1;
x0          = [v_nw0;gamma0];
%standard input to classes
input.x0    = x0;
input.Q     = Q;
input.rho   = rho;
input.P0    = blkdiag(P0_wind,P0_gamma);
input.R     = R_dp;

%wind 3sf1
wind_3sf1_input                 = input;
wind_3sf1_input.window_length   = 2000;
wind_3sf1                       = class_wind_3sf1(wind_3sf1_input);

%wind 3sf2
wind_3sf2_input     = input;
wind_3sf2_input.R   = R_dp/10;
wind_3sf2_input.Q   = 10*Q;
wind_3sf2           = class_wind_3sf2(wind_3sf2_input);

%wind 3sf3
wind_3sf3_input     = input;
wind_3sf3_input.R   = R_dp;
wind_3sf3           = class_wind_3sf3(wind_3sf3_input);

%wind ekf
wind_ekf        = class_wind_ekf(input);

%wind ta
wind_ta_input   = input;
wind_ta_input.R = R_speed;
wind_ta         = class_wind_ta(wind_ta_input);

% %wind opt
% wind_opt_input = wind_3sf1_input;
% wind_opt       = class_wind_opt(wind_opt_input);

% %wind aug
% wind_aug_input      = input;
% wind_aug_input.P0   = blkdiag(I3*3,1e-2,9);
% wind_aug_input.Q    = blkdiag(Q,1000);
% wind_aug_input.R    = blkdiag((2/rho)^2*R_dp,1e-8)*1000;
% wind_aug            = class_wind_aug(wind_aug_input);
% %wind aug
% wind_aug_input      = input;
% wind_aug_input.P0   = blkdiag(I3*3,1e-2,9);
% wind_aug_input.Q    = blkdiag(Q,1000);
% wind_aug_input.R    = blkdiag((2/rho)^2*R_dp,1e-8);
% wind_aug            = class_wind_aug(wind_aug_input);

% %wind adp
% wind_adp_input      = input;
% wind_adp_input.x0   = [0;0;18;1];
% wind_adp_input.P0   = blkdiag(.1,.1,2,.1e-4);
% wind_adp_input.Q    = blkdiag(1e-4,1e-4,.1,1e-8);
% wind_adp_input.R    = R_speed;
% wind_adp_input.q0   = mekf.q(:,1);
% wind_adp            = class_wind_adp(wind_adp_input);

% %ukf adp 
% ukf_input.x0    = [0;0;18];
% ukf_input.P0    = blkdiag(.1,.1,1);
% ukf_input.Q     = blkdiag(1e-1,1e-1,.1);
% ukf_input.R     = blkdiag(1e-1,1e-1,.3);
% ukf             = class_wind_ukf(ukf_input);

%aeroprobe kf
% apkf_input.x0       = mekf.vel(:,1);
% apkf_input.Q        = blkdiag(Q_ars,Q_acc);
% apkf_input.P0       = 2*eye(3);
% apkf_input.R        = 1e-1*eye(3);
% apkf                = class_apkf(apkf_input);

% %wind model based estimator
% Q_CL0               = 1e-5;
% Q_CLalpha           = 1e-4;
% Q_CLq               = 1e-2;
% Q_CLe               = 1e-2;
% mod_input.x0        = [0;x8.C_L_0;x8.C_L_alpha;x8.C_L_q;0];
% % mod_input.x0        = [0;-0.07;x8.C_L_alpha;x8.C_L_q;0];
% mod_input.Q         = blkdiag(1e-2,Q_CL0,Q_CLalpha,Q_CLq,Q_CLe);
% mod_input.P0        = blkdiag(1,1e-7,1e-7,1e-8,1e-7);
% mod_input.R_f       = 1e-3;
% mod_input.R_aoa_lf  = 5e-1;
% mod_input.c         = x8.c;
% mod_input.mass      = x8.mass;
% mod_input.S         = x8.S_wing;
% mod_input.rho       = rho;
% mod                 = class_model_based(mod_input);

% %wind model based estimator exogenous
% Q_CL0               = 1e-5;
% Q_CLalpha           = 1e-3;
% Q_CLq               = 1e-3;
% Q_CLe               = 1e-4;
% % mod_input.x0        = [0;x8.C_L_0;x8.C_L_alpha;x8.C_L_q;0];
% modx_input.x0        = [0;x8.C_L_0;x8.C_L_alpha;0;0];
% modx_input.Q         = blkdiag(1e-2,Q_CL0,Q_CLalpha,Q_CLq,Q_CLe);
% modx_input.P0        = blkdiag(1e-8,1e-8,1e-8,1e-8,1e-8);
% modx_input.R_f       = 1e-3;
% % mod_input.R_aoa_hf  = 1e-4;
% % mod_input.R_aoa_lf  = 1e-2;
% modx_input.c         = x8.c;
% modx_input.mass      = x8.mass;
% modx_input.S         = x8.S_wing;
% modx_input.rho       = rho;
% modx                 = class_model_based_x(modx_input);

% %wind model based estimator exogenous
% Q_CL0               = 1e-5;
% Q_CLalpha           = 1e-3;
% Q_gamma             = 1e-8;
% Va_input.x0         = [20;0;x8.C_L_0;x8.C_L_alpha;1];
% Va_input.Q          = blkdiag(eye(3)*1e-2,eye(3)*1e-2,Q_CL0,Q_CLalpha,Q_gamma);
% Va_input.P0         = blkdiag(1,1e-2,1e-8,1e-8,1e-6);
% Va_input.R_f        = 1e-2;
% Va_input.R_V        = 1e-1;
% Va_input.mass       = x8.mass;
% Va_input.S          = x8.S_wing;
% Va_input.rho        = rho;
% Va                  = class_Va(Va_input);

%wind ekf
Q_vnw = 1e-3*eye(3);
Q_gamma = 1e-8;
Q_CL0 = 1e-8;
Q_CLalpha = 1e-11;
model_ekf_input.Q = blkdiag(Q_vnw,Q_gamma,Q_CL0,Q_CLalpha);
P0_vnw = 1*eye(3);
P0_gamma = 1e-5;
P0_CL0 = 1e-12;
P0_CLalpha = 1e-10;
model_ekf_input.P0 = blkdiag(P0_vnw,P0_gamma,P0_CL0,P0_CLalpha);
model_ekf_input.R_dp = R_dp;
model_ekf_input.R_f = 5e-1;
model_ekf_input.x0 = [0;0;0;.72;x8.C_L_0;x8.C_L_alpha];
model_ekf_input.rho = 1.225;
model_ekf_input.S_wing = x8.S_wing;
model_ekf_input.c = x8.c;
model_ekf_input.mass = x8.mass;
wind_model_ekf        = class_model_ekf(model_ekf_input);
%% Run filters
run_all         = 1;
run_wind_3sf    = 0 || run_all;
run_wind_ekf    = 0 || run_all;
% run_wind_opt    = 0 || run_all;
run_wind_ta     = 0 || run_all;
% run_wind_aug    = 0 || run_all;     
% run_wind_adp    = 0 || run_all;
run_model_ekf   = 1 || run_all;
% run_apkf        = 0 || run_all;
% run_mod         = 0 || run_all;
% run_modx        = 0 || run_all;
% run_nlsq        = 0 || run_all;
% run_Va          = 0 || run_all;

if run_wind_3sf
    es.wind_3sf1.data = zeros(4,N);
    es.wind_3sf2.data = zeros(4,N);
    es.wind_3sf3.data = zeros(4,N);
    fprintf('%c - Three-stage filter\n',char(9))
end
if run_wind_ekf
    es.wind_ekf.data = zeros(12,N);
    fprintf('%c - Extended Kalman filter\n',char(9))
end
if run_wind_ta
    es.wind_ta.data = zeros(4,N);
    fprintf('%c - Tor Arnes filter\n',char(9))
end
% if run_wind_aug
%     es.wind_aug.data = zeros(5,N);
%     fprintf('%c - Augmented filter\n',char(9))
% end
% if run_wind_opt
%     es.wind_opt.data = zeros(4,N);
%     es.wind_opt.received = zeros(1,N);
% end
% if run_wind_adp
%     es.wind_adp.data = zeros(4,N);
% end
% if run_mekf_rvel
%     es.mekf_rvel.data = zeros(23,N);
% end
if run_model_ekf
    es.model_ekf.data = zeros(6,N);
%     es.model_ekf.P    = zeros(6,6,N);
    fprintf('%c - Model=based Wind EKF\n',char(9))
end
% if run_apkf
%     es.apkf.data = zeros(3,N);
%     fprintf('%c - Aeroprobe Kalman filter filter\n',char(9))
% end
% if run_mod
%     es.mod.data = zeros(5,N);
%     fprintf('%c - Model-based Kalman filter\n',char(9))
% end
% if run_modx
%     es.modx.data = zeros(5,N);
%     fprintf('%c - Model-based eXogenous Kalman filter\n',char(9))
% end
% if run_Va
%     es.Va.data = zeros(5,N);
%     fprintf('%c - V and alpha Kalman filter\n',char(9))
% end

s='1';
tic
last_percent = 0;
mh = MeasurementHandler({acc,ars,gps1,gps2,gps3,pitot,dp,gpsvel1,gpsvel2,mag,adp,elevator},t_start);
beta = zeros(N,1);
saveairspeeds = zeros(2,N);
for i = 1:N
    if i/(N/100) > last_percent
        s = num2str(last_percent);
        last_percent = last_percent + 1;
        fprintf('Done with %s%s: Time elapsed %.2f%s',s,'%',toc,char(10));
%         if i > 1
%             es.mekf_rvel.data(:,i-1)'
%         end
    end
    t_i = t(i);
    measurements = mh.get_measurements(t_i);
    %Filters
    measurements.b_acc_hat.data = mekf.bacc(:,i);
    measurements.b_ars_hat.data = mekf.bars(:,i);
    measurements.velocity.data = mekf.vel(:,i);
    measurements.quaternion.data = mekf.q(:,i);
    if run_wind_3sf
        ret_3sf1 = wind_3sf1.update(t_i,measurements);
        es.wind_3sf1.data(:,i) = ret_3sf1.x;
        measurements.x_3sf1 = ret_3sf1.x;
        measurements.C_3sf1 = ret_3sf1.C;
        measurements.y_3sf1 = ret_3sf1.y;
        x_3sf2 = wind_3sf2.update(t_i,measurements);
        es.wind_3sf2.data(:,i) = x_3sf2;
        measurements.x_3sf2 = x_3sf2;
        es.wind_3sf3.data(:,i) = wind_3sf3.update(t_i,measurements);
    end
    if run_wind_ekf
        es.wind_ekf.data(:,i) = wind_ekf.update(t_i,measurements);
    end
    rvel = Rquat(measurements.quaternion.data)'*(measurements.velocity.data-es.wind_ekf.data(1:3,i));
    measurements.airspeed_hat2.data = sqrt(2*measurements.dp.data/(0.71*1.225));
    measurements.airspeed_hat.data = norm(rvel);
    measurements.beta_hat.data = asin(rvel(2)/norm(rvel));
    measurements.alpha_hat.data = atan2(rvel(3),rvel(1));
    beta(i) = measurements.beta_hat.data;
    if run_wind_ta
        es.wind_ta.data(:,i) = wind_ta.update(t_i,measurements);
    end
%     if run_wind_aug
%         es.wind_aug.data(:,i) = wind_aug.update(t_i,measurements);
%     end
%     if run_wind_opt
%         wind_opt_ret = wind_opt.update(t_i,measurements);
%         if ~isempty(wind_opt_ret)
%     %         wind_opt_ret
%             es.wind_opt.data(:,i) = wind_opt_ret;
%             es.wind_opt.received(i) = 1;
%         end
%     end    
%     if run_wind_adp && ~isempty(measurements.acc.data) && ~isempty(measurements.ars.data)
%         es.wind_adp.data(:,i) = wind_adp.update(t_i,measurements);
%     end
%     if run_apkf
%         es.apkf.data(:,i) = apkf.update(t_i,measurements);
%     end
    if run_model_ekf
%         [es.model_ekf.data(:,i),es.model_ekf.P(:,:,i)] = wind_model_ekf.update(t_i,measurements);
        es.model_ekf.data(:,i) = wind_model_ekf.update(t_i,measurements);
    end
%     if run_mod
%         es.mod.data(:,i) = mod.update(t_i,measurements);
%     end
%     if run_modx
%         es.modx.data(:,i) = modx.update(t_i,measurements);
%     end
%     if run_nlsq
%         es.wind_nlsq.data(:,i) = 
%     end
%     if run_Va
%         es.Va.data(:,i) = Va.update(t_i,measurements);
%     end
    saveairspeeds(:,i) = [measurements.airspeed_hat.data;measurements.airspeed_hat2.data];
    if isempty(measurements.airspeed_hat2.data)
        saveairspeeds(2,i) = 0;
    end
end


%%

%Wind GT
% gt_vnw.t = [t(1),t(end)];
% gt_vnw.data = GTw*[1,1];

% gt_gamma.t = [t(1),t(end)];
% gt_gamma.data = GTg*[1,1];
%Wind EKF
rvel = zeros(3,N);
aoa = zeros(1,N);
ssa = zeros(1,N);
Vwb = zeros(1,N);
% for i = 1:N
%     Rnb = Rquat(mekf.q(:,i));    
%     rvel(:,i) = Rnb'*(mekf.vel(:,i)-es.wind_ekf.data(1:3,i));
%     Vwb(i) = norm(rvel(:,i));
%     aoa(i) = atan2(rvel(3,i),rvel(1,i));
%     ssa(i) = asin(rvel(2,i)/norm(rvel(:,i)));
% end

wind_vnw.t = t;
wind_vnw.data = es.wind_ekf.data(1:3,:);
wind_gamma.t = t;
wind_gamma.data = es.wind_ekf.data(4,:); 
wind_vwb.data = zeros(3,N);   wind_vwb.t = t;
wind_aoa.data = zeros(1,N);    wind_aoa.t = t;
wind_ssa.data = zeros(1,N);    wind_ssa.t = t;
wind_Vwb.data = zeros(1,N);    wind_Vwb.t = t;

wind_eigs.t = t;
wind_eigs.data = es.wind_ekf.data(5:8,:);

wind_Peigs.t = t;
wind_Peigs.data = es.wind_ekf.data(9:12,:);
% %Wind OPT
% inds = find(es.wind_opt.received);
% wind_opt_vnw.t = t(inds);
% wind_opt_vnw.data = es.wind_opt.data(1:3,inds);
% wind_opt_gamma.t = t(inds);
% wind_opt_gamma.data = es.wind_opt.data(4,inds);
% 
%Wind TA
wind_ta_vnw.t = t;
wind_ta_vnw.data = es.wind_ta.data(1:3,:);
wind_ta_gamma.t = t;
wind_ta_gamma.data = 1./es.wind_ta.data(4,:);
wind_ta_vwb.data = zeros(3,N);   wind_ta_vwb.t = t;
wind_ta_aoa.data = zeros(1,N);    wind_ta_aoa.t = t;
wind_ta_ssa.data = zeros(1,N);    wind_ta_ssa.t = t;
wind_ta_Vwb.data = zeros(1,N);    wind_ta_Vwb.t = t;

% %Wind AUG
% wind_aug_vnw.t = t;
% wind_aug_vnw.data = es.wind_aug.data(1:3,:);
% wind_aug_gamma.t = t;
% wind_aug_gamma.data = es.wind_aug.data(4,:);
% wind_aug_vnorm.t = t;
% wind_aug_vnorm.data = es.wind_aug.data(5,:);
% wind_aug_vwb.data = zeros(3,N);   wind_aug_vwb.t = t;
% wind_aug_aoa.data = zeros(1,N);    wind_aug_aoa.t = t;
% wind_aug_ssa.data = zeros(1,N);    wind_aug_ssa.t = t;
% wind_aug_Vwb.data = zeros(1,N);    wind_aug_Vwb.t = t;

%Wind 3SF1
wind_3sf1_vnw.t = t;
wind_3sf1_vnw.data = es.wind_3sf1.data(1:3,:);
wind_3sf1_gamma.t = t;
wind_3sf1_gamma.data = es.wind_3sf1.data(4,:);

%Wind 3SF2
wind_3sf2_vnw.t = t;
wind_3sf2_vnw.data = es.wind_3sf2.data(1:3,:);
wind_3sf2_gamma.t = t;
wind_3sf2_gamma.data = es.wind_3sf2.data(4,:);

%Wind 3SF3
wind_3sf3_vnw.t = t;
wind_3sf3_vnw.data = es.wind_3sf3.data(1:3,:);
wind_3sf3_gamma.t = t;
wind_3sf3_gamma.data = es.wind_3sf3.data(4,:);
wind_3sf3_vwb.data = zeros(3,N);   wind_3sf3_vwb.t = t;
wind_3sf3_aoa.data = zeros(1,N);    wind_3sf3_aoa.t = t;
wind_3sf3_ssa.data = zeros(1,N);    wind_3sf3_ssa.t = t;
wind_3sf3_Vwb.data = zeros(1,N);    wind_3sf3_Vwb.t = t;
% for i = 1:N
%     Rnb = Rquat(mekf.q(:,i));
%     wind_3sf3_vwb.data(:,i) = Rnb'*(mekf.vel(:,i)-wind_3sf3_vnw.data(1:3,i));
%     wind_3sf3_Vwb.data(i) = norm(wind_3sf3_vwb.data(:,i));
%     wind_3sf3_aoa.data(i) = atan2(wind_3sf3_vwb.data(3,i),wind_3sf3_vwb.data(1,i))*RAD2DEG;
%     wind_3sf3_ssa.data(i) = asin(wind_3sf3_vwb.data(2,i)/norm(wind_3sf3_vwb.data(:,i)))*RAD2DEG;
% end

% %Wind ADP
% wind_adp_aoa.t = t;
% wind_adp_aoa.data = wrapToPi(es.wind_adp.data(1,:))*RAD2DEG;
% wind_adp_ssa.t = t;
% wind_adp_ssa.data = wrapToPi(es.wind_adp.data(2,:))*RAD2DEG;
% wind_adp_Vwb.t = t;
% wind_adp_Vwb.data = es.wind_adp.data(3,:);
% % wind_adp_aoa.t = t;
% % wind_adp_aoa.data = es.wind_adp.data(1:3,:);
% wind_adp_gamma.t = t;
% wind_adp_gamma.data = es.wind_adp.data(4,:);

%UKF
model_ekf_vnw.t = t;
model_ekf_vnw.data = es.model_ekf.data(1:3,:);
model_ekf_gamma.t = t;
model_ekf_gamma.data = es.model_ekf.data(4,:);
model_ekf_CL0.t = t;
model_ekf_CL0.data = es.model_ekf.data(5,:);
model_ekf_CLalpha.t = t;
model_ekf_CLalpha.data = es.model_ekf.data(6,:);
model_ekf_aoa.t = t;
model_ekf_aoa.data = zeros(1,N);
model_ekf_ssa.t = t;
model_ekf_ssa.data = zeros(1,N);
model_ekf_vwb.t = t;
model_ekf_vwb.data = zeros(3,N);
model_ekf_Vwb.t = t;
model_ekf_Vwb.data = zeros(3,N);
for i = 1:N
    Rnb = Rquat(mekf.q(:,i));
    model_ekf_vwb.data(:,i) = Rnb'*(mekf.vel(:,i)-model_ekf_vnw.data(1:3,i));
    model_ekf_Vwb.data(i) = norm(model_ekf_vwb.data(:,i));
    model_ekf_aoa.data(i) = atan2(model_ekf_vwb.data(3,i),model_ekf_vwb.data(1,i))*RAD2DEG;
    model_ekf_ssa.data(i) = asin(model_ekf_vwb.data(2,i)/norm(model_ekf_vwb.data(:,i)))*RAD2DEG;
end

% %RVEL KF
% apkf_vwb.t = t;
% apkf_vwb.data = es.apkf.data;
% apkf_Vwb.t = t;
% apkf_Vwb.data = zeros(1,N);
% apkf_aoa.t = t;
% apkf_aoa.data = zeros(1,N);
% apkf_ssa.t = t;
% apkf_ssa.data = zeros(1,N);
% for i = 1:N
%     apkf_Vwb.data(i) = norm(apkf_vwb.data(:,i));
%     apkf_aoa.data(i) = atan2(apkf_vwb.data(3,i),apkf_vwb.data(1,i))*RAD2DEG;
%     apkf_ssa.data(i) = asin(apkf_vwb.data(2,i)/apkf_Vwb.data(i))*RAD2DEG;
% end

% %Wind Va
% Va_Vwb.t = t;
% Va_Vwb.data = es.Va.data(1,:);
% Va_aoa.t = t;
% Va_aoa.data = es.Va.data(2,:)*RAD2DEG;
% Va_C_L_0.t = t;
% Va_C_L_0.data = es.Va.data(3,:);
% Va_C_L_alpha.t = t;
% Va_C_L_alpha.data = es.Va.data(4,:);
% Va_gamma.t = t;
% Va_gamma.data = es.Va.data(5,:);

% %Model based
% mod_aoa.t = t;
% mod_aoa.data = es.mod.data(1,:)*RAD2DEG;%*1.8+1;
% % mod_aoa.data(mod_aoa.data > 30) = 1;

% %Model based XKF
% modx_aoa.t = t;
% modx_aoa.data = es.modx.data(1,:)*RAD2DEG;%*1.8+1;

%AOA APPROXIMATION pitch - flight path angle
%SSA APPROXIMATION course - heading
fpa = zeros(1,N);
course = zeros(1,N);

for i = 1:N
    Rnb = Rquat(mekf.q(:,i));
    
    rvel(:,i) = Rnb'*(mekf.vel(:,i)-es.wind_ekf.data(1:3,i));
    wind_vwb.data(:,i) = Rnb'*(mekf.vel(:,i)-es.wind_ekf.data(1:3,i));
    wind_Vwb.data(i) = norm(rvel(:,i));
    wind_aoa.data(i) = atan2(rvel(3,i),rvel(1,i))*RAD2DEG;
    wind_ssa.data(i) = asin(rvel(2,i)/norm(rvel(:,i)))*RAD2DEG;
    
    
    wind_3sf3_vwb.data(:,i) = Rnb'*(mekf.vel(:,i)-wind_3sf3_vnw.data(1:3,i));
    wind_3sf3_Vwb.data(i) = norm(wind_3sf3_vwb.data(:,i));
    wind_3sf3_aoa.data(i) = atan2(wind_3sf3_vwb.data(3,i),wind_3sf3_vwb.data(1,i))*RAD2DEG;
    wind_3sf3_ssa.data(i) = asin(wind_3sf3_vwb.data(2,i)/norm(wind_3sf3_vwb.data(:,i)))*RAD2DEG;
    
    wind_ta_vwb.data(:,i) = Rnb'*(mekf.vel(:,i)-wind_ta_vnw.data(1:3,i));
    wind_ta_Vwb.data(i) = norm(wind_ta_vwb.data(:,i));
    wind_ta_aoa.data(i) = atan2(wind_ta_vwb.data(3,i),wind_ta_vwb.data(1,i))*RAD2DEG;
    wind_ta_ssa.data(i) = asin(wind_ta_vwb.data(2,i)/norm(wind_ta_vwb.data(:,i)))*RAD2DEG;
    
%     wind_aug_vwb.data(:,i) = Rnb'*(mekf.vel(:,i)-wind_aug_vnw.data(1:3,i));
%     wind_aug_Vwb.data(i) = norm(wind_aug_vwb.data(:,i));
%     wind_aug_aoa.data(i) = atan2(wind_aug_vwb.data(3,i),wind_aug_vwb.data(1,i))*RAD2DEG;
%     wind_aug_ssa.data(i) = asin(wind_aug_vwb.data(2,i)/norm(wind_aug_vwb.data(:,i)))*RAD2DEG;
    
    vel_b = Rnb'*mekf.vel(:,i);
    fpa(i) = asin(-mekf.vel(3,i)/norm(mekf.vel(:,i)));
    course(i) = asin(mekf.vel(2,i)/norm(mekf.vel(1:2,i)));
end
aoa_approx.t = t;
aoa_approx.data = (mekf.ea(2,:)-fpa)*RAD2DEG;
ssa_approx.t = t;
ssa_approx.data = wrapToPi(course - mekf.ea(3,:))*RAD2DEG;

%Aeroprobe
%Interpolate
ap_aoa_inp = interp1(ap.aoa.t,ap.aoa.data,wind_aoa.t);
cal_aoa = 0*mean(aoa-ap_aoa_inp);
% figure;
% hold on;
% plot(ap_aoa_inp+cal_aoa,'r');
% plot(aoa,'b');
cal = [cal_aoa,0];
ap_aoa.t = ap.aoa.t;
ap_aoa.data = (ap.aoa.data+cal(1))*RAD2DEG;
ap_ssa.t = ap.ssa.t;
ap_ssa.data = (ap.ssa.data+cal(2))*RAD2DEG;
ap_rvel.t = ap.rvel.t;
ap_rvel.data = zeros(3,length(ap_rvel.t));
ap_vnw.t = t;
ap_vnw.data = zeros(3,N);
for i = 1:length(ap_rvel.t)
    a = ap.aoa.data(i)+cal(1);
    b = ap.ssa.data(i)+cal(2);
    ap_rvel.data(:,i) = ap.airspeed.data(i)*[...
                            cos(a)*cos(b);
                                   sin(b);
                            sin(a)*cos(b)];
end
ap_rvel_inp = interpolateNd(ap_rvel.t,ap_rvel.data,t);
for i = 1:N
    Rnb = Rquat(mekf.q(:,i));
    ap_vnw.data(:,i) = mekf.vel(:,i) - Rnb*ap_rvel_inp(:,i);
end
 
%LSQ

% adp_inp = interpolateNd(adp.t,adp.data,t);
% airspeed_inp = adp_inp.data(3,:);
% aoa_inp = apd_inp.data(1,:);
% acc_inp = interpolateNd(acc.t,acc.data,t);
% ars_inp = interpolateNd(ars.t,ars.data,t);
% y = -x8.mass*acc_inp(3,:);

pd_wind.v_nw =  {     ap_vnw,     px.wvel, wind_vnw  ,  wind_3sf1_vnw,  wind_3sf2_vnw, wind_3sf3_vnw  ,  wind_ta_vnw, model_ekf_vnw};
pd_wind.gamma = {          0,           0, wind_gamma,wind_3sf1_gamma,wind_3sf2_gamma, wind_3sf3_gamma,wind_ta_gamma, model_ekf_gamma};
pd_wind.aoa =   {     ap_aoa,      px.aoa, wind_aoa  ,              0,              0, wind_3sf3_aoa  ,  wind_ta_aoa, model_ekf_aoa}; 
pd_wind.ssa =   {     ap_ssa,      px.ssa, wind_ssa  ,              0,              0, wind_3sf3_ssa  ,  wind_ta_ssa, model_ekf_ssa};
pd_wind.Vwb =   {ap.airspeed, px.airspeed, wind_Vwb  ,              0,              0, wind_3sf3_Vwb  ,  wind_ta_Vwb, model_ekf_Vwb};

% pd_wind.v_nw =  {          0,     px.wvel, wind_vnw  ,  wind_3sf1_vnw,  wind_3sf2_vnw, wind_3sf3_vnw  ,0      ,0       ,gt_vnw  ,0       , model_ekf_vnw};
% pd_wind.gamma = {          0,           0, wind_gamma,wind_3sf1_gamma,wind_3sf2_gamma, wind_3sf3_gamma,0      ,0       ,gt_gamma,Va_gamma, model_ekf_gamma};
% pd_wind.aoa =   {     ap_aoa,      px.aoa, wind_aoa  ,              0,              0, wind_3sf3_aoa  ,mod_aoa,modx_aoa,0       ,Va_aoa  , model_ekf_aoa}; 
% pd_wind.ssa =   {     ap_ssa,      px.ssa, wind_ssa  ,              0,              0, wind_3sf3_ssa  ,0      ,0       ,0       ,0       , model_ekf_ssa};
% pd_wind.Vwb =   {ap.airspeed, px.airspeed, wind_Vwb  ,              0,              0, wind_3sf3_Vwb  ,0      ,0       ,0       ,Va_Vwb  , model_ekf_Vwb};
% pd_wind.vwb =   {    ap_rvel,     px.rvel, wind_vwb  ,              0,              0, wind_3sf3_vwb  ,0};

%% 
plotinput = make_plotinput();
plotinput.legends = {'Aeroprobe', 'Pixhawk','EKF','3SF1','3SF2','3SF3','KF[7]','Opt.'};
plotinput.ylabels = {{'N [$m/s$]','E [$m/s$]','D [$m/s$]'},{'Gamma'},{'AOA [deg]'},{'SSA [deg]'},{'$\|V_wb\|_2$'},{'X [$m/s$]','Y [$m/s$]','Z [$m/s$]'}};
plotinput.labels  = {'Wind Velocity','Gamma','AOA','SSA','Airspeed','Relative Velocity'};
plotinput.figpos = [0,0,1000,600];
if strcmp(XXX,'005')
    plotinput.xlims = {[300,2400],[300,2400],[1500,2400],[2200,2400],[2000,2400],[0,2400]};
elseif strcmp(XXX,'010')
    plotinput.xlims = {[450,2400],[450,1960],[1760,1960],[1760,1960],[1760,1960],[500,1960]};
end
plotinput.do_save = 0;
plotinput.savefolder = sprintf('~/work/postdoc/latex/papers/windestimation/plots%s/',XXX);
plotinput.savetype = '.eps';
plotinput.savesuffix = '-Qlarge';
plotinput.selected_fields = [1,1,0,0,0,0];
plotinput.selected_estimators = [0,1,1,0,1,1,1,1];
plotter(pd_wind,plotinput);
if do_save
%     save(sprintf('~/work/statuses/2019-02-21-tor-arne/wind%s.mat',XXX),'pd_wind')
%     save('~/work/statuses/2019-02-21-tor-arne/plotinput.mat','plotinput');
    save(sprintf('../data/wind_%s_%.2f.mat',XXX,gpstimeshift(idt)),'pd_wind');
end
%%
% plotinds = es.mod.data(1,:) < 1;
% figure;
% subplotter(t(plotinds),es.mod.data(1:4,plotinds).*[RAD2DEG;1;1;1],[])
% 
% C_L_0.t = t;
% C_L_0.data = es.mod.data(2,:);
% C_L_a.t = t;
% C_L_a.data = es.mod.data(3,:);
% C_L_q.t = t;
% C_L_q.data = es.mod.data(4,:);
% C_L_e.t = t;
% C_L_e.data = es.mod.data(5,:);
% 
% XC_L_0.t = t;
% XC_L_0.data = es.modx.data(2,:);
% XC_L_a.t = t;
% XC_L_a.data = es.modx.data(3,:);
% XC_L_q.t = t;
% XC_L_q.data = es.modx.data(4,:);
% XC_L_e.t = t;
% XC_L_e.data = es.modx.data(5,:);

coeffs.C_L_0 = {model_ekf_CL0};
coeffs.C_L_a = {model_ekf_CLalpha};
% coeffs.C_L_0 = {C_L_0,XC_L_0,Va_C_L_0    , model_ekf_CL0};
% coeffs.C_L_a = {C_L_a,XC_L_a,Va_C_L_alpha, model_ekf_CLalpha};
% coeffs.C_L_q = {C_L_q,XC_L_q};
% coeffs.C_L_e = {C_L_e,XC_L_e};

pic.ylabels = {{'$C_{L0}$'},{'$C_{L{\alpha}}$'},{'$C_{Lq}$'},{'$C_{L\delta e}$'}};
pic.figpos = [0,0,1000,600];
pic.legends = {'mod KF', 'mod XKF', 'Va EKF', 'mod EKF'};
% if strcmp(XXX,'005')
%     pic.xlims = {[300,2400],[300,2400],[2200,2400],[2200,2400],[2000,2400],[0,2400]};
% elseif strcmp(XXX,'010')
%     pic.xlims = {[t_start,2070],[t_start,2070],[t_start,2070]};
% end
% pic.ylims = {[-.5,.5],[-2,7],[-5,10],[-100,35]};
plotter(coeffs,pic);

if do_save
%     save(sprintf('~/work/statuses/2019-02-21-tor-arne/coeffs%s.mat',XXX),'coeffs')
%     save('~/work/statuses/2019-02-21-tor-arne/plotinput_coeffs.mat','pic')
    save(sprintf('../data/coeffs_%s_%.2f.mat',XXX,gpstimeshift(idt)),'coeffs');
    save('../data/plotinput_coeffs.mat','pic')
end

%% --- Plotting states of MEKF ---
% %Integrating ars to euler angles
% eaint = zeros(3,N);
% eaint(:,1) = mekf_ea.data(:,1);
% ars_excerpt = ars.data(:,inds);
% for i = 1:N-1
%     [~,~,T] = eulerang(eaint(1,i),eaint(2,i),eaint(3,i));
%     eaint(:,i+1) = eaint(:,i) + dt*T*(ars_excerpt(:,i)-mekf.bars(:,i));
% end
% eaint_dr.t = t;
% eaint_dr.data = wrapToPi(eaint);

mekf_pos.data    = mekf.pos;     mekf_pos.t = t;
mekf_vel.data    = mekf.vel;     mekf_vel.t = t;
mekf_bacc.data   = mekf.bacc;    mekf_bacc.t = t;
mekf_ea.data     = mekf.ea;      mekf_ea.t = t;
mekf_bars.data   = mekf.bars;    mekf_bars.t = t;
mekf_bmag.data   = mekf.bmag;    mekf_bmag.t = t;

pd_mekf.pos = {mekf.px.pos,mekf_pos};
pd_mekf.vel = {mekf.px.vel,mekf_vel};
pd_mekf.b_acc = {0,mekf_bacc};
pd_mekf.ea = {mekf.px.ea,mekf_ea};
pd_mekf.b_ars = {0,mekf_bars};

plotinput_mekf = make_plotinput();
plotinput_mekf.legends = {'PX','MEKF'};
plotter(pd_mekf,plotinput_mekf);

if do_save
%     save(sprintf('~/work/statuses/2019-02-21-tor-arne/mekf%s.mat',XXX),'pd_mekf')
%     save('~/work/statuses/2019-02-21-tor-arne/plotinput_mekf.mat','plotinput_mekf')
%     save(sprintf('../data/wind_%s_%.2f.mat',XXX,gpstimeshift(idt)),'mekf');
end

%% --- Plotting observability
pin.xlims = [t(1),t(end)];
subplotter(wind_eigs.t,wind_eigs.data,pin);
figure;
subplotter(wind_Peigs.t,wind_Peigs.data,pin);



%%
% fig=figure('position',[0,0,1000,600],'Color',[1,1,1]);
% ax = plot(mekf.pos(2,:),mekf.pos(1,:),'b'); 
% ax.LineWidth = 1.3
% xlabel('East [m]','FontSize',15,'interpreter','latex'); 
% ylabel('North [m]','FontSize',15,'interpreter','latex');
% export_fig('/home/bard/work/postdoc/latex/papers/windestimation/plots005/XYMap.eps')





