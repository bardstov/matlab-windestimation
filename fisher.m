addpath lib

load_exp_measurements

dt = .01;
t = ceil(px.acc.t(1)):dt:floor(px.acc.t(end))-dt;
N = length(t);

x8 = importdata('../simulator/x8_param.mat');

%% interpolate all signals
iea = interpolateNd(px.ea.t,px.ea.data,t);
iacc = interpolateNd(acc.t,acc.data,t);
iars = interpolateNd(ars.t,ars.data,t);
iadp = interpolateNd(adp.t,adp.data,t);
iele = interpolateNd(px.ele.t,px.ele.data,t)/1e4;

%% Estimate angular acceleration
angacc_direct = lowpass(diff(lowpass(iars',1,100))/dt,1,100)';
angacc_kf = zeros(3,N);
I = eye(9);
A = [zeros(9,3),eye(9,6)];
G = [eye(3,6);zeros(3,6);zeros(3),eye(3)];
H = eye(6,9);

Q = blkdiag(1e-2*eye(3),1*eye(3));
R = blkdiag(1e-1*eye(3),1e-2*eye(3));

P = blkdiag(1e-2*eye(3),1e-3*eye(3),1*eye(3));
%%
x = zeros(9,N);
x(:,1) = [iea(:,1);iars(:,i);zeros(3,1)];
s='1';
tic
last_percent = 0;
prev_ars = zeros(3,1);
for i = 2:N
    if i/(N/100) > last_percent
        s = num2str(last_percent);
        last_percent = last_percent + 1;
        fprintf('Done with %s%s: Time elapsed %.2f%s',s,'%',toc,char(10));
    end
    [~,Rnb,T] = eulerang(iea(1,i),iea(2,i),iea(3,i));
    
    dx = [T*iars(:,i-1);zeros(6,1)];
    x(:,i) = x(:,i-1) + dt*dx;
    
    A(1:3,1:3) = T;
    p = iars(1,i);
    q = iars(2,i);
    r = iars(3,i);
    phi = iea(1,i);
    the = iea(2,i);
    psi = iea(3,i);
    
    A(1:3,4:6) = [ (q*cos(phi)*sin(the))/cos(the) - (r*sin(phi)*sin(the))/cos(the), r*cos(phi) + q*sin(phi) + (r*cos(phi)*sin(the)^2)/cos(the)^2 + (q*sin(phi)*sin(the)^2)/cos(the)^2, 0
                                                         - r*cos(phi) - q*sin(phi),                                                                                                 0, 0
                                     (q*cos(phi))/cos(the) - (r*sin(phi))/cos(the),                               (r*cos(phi)*sin(the))/cos(the)^2 + (q*sin(phi)*sin(the))/cos(the)^2, 0];
    [Phi,Qd] = van_loan(A,G*Q*G',dt);
    P = Phi*P*Phi' + Qd;
    P = .5*(P+P');
    
    y = [iea(:,i);iars(:,i)];
    yhat = x(1:6,i);
    K = P*H'/(H*P*H'+R);
    P = (I-K*H)*P*(I-K*H)'+K*R*K';
    P = (P+P')/2;
    
    x(:,i) = x(:,i) + K*(y-yhat);
end
%%
figure
subplotter(t(1:end-1),angacc_direct)
subplotter(t,x(7:9,:))

if strcmp(XXX,'007')
    tstarts = [ 600;
                651.5;
                714;
                775;
                830;
                888;
                950;
                1012;
                1070;
                1130;
                1992;
                2018;
                2041;
                2067;
                2090;
                2145];
    tends   = [ 607
                664.5;
                723;
                783;
                842;
                898.5;
                960;
                1020;
                1078;
                1137.5;
                2010;
                2030.5;
                2058;
                2082;
                2114;
                2169];
end

for i = 1:length(tstarts)
    inds = find(es.t(es.t < tends(i))>tstarts(i));
    figure;
    subplotter(ars.t(inds),ars.data(:,inds)*RAD2DEG)
end
CRLB = sqrt(diag(Fsum));
%% calculate CRLB

allinds = [];
for i = 1:length(tstarts)
    inds = find(t(t < tends(i))>tstarts(i));
    allinds = [allinds,inds];
end

alphas  = iadp(1,allinds);
Vs      = iadp(3,allinds);
qs      = iars(2,allinds);
eles    = iele(allinds);

H = .5*1.225*x8.S_wing*x8.c*(Vs.*Vs)'.*[ones(length(allinds),1), alphas', x8.b*qs'./(2*Vs'), eles'];

F = zeros(4,4,length(allinds));
Fsum = zeros(4);
for i = 1:length(allinds)
    Hi= .5*1.225*x8.S_wing*x8.c*Vs(i)^2*[1, alphas(i), x8.b*qs(i)/(2*Vs(i)), eles(i)];
    H(i,:) = Hi;
    F(:,:,i) = Hi'*Hi;
    Fsum = Fsum + F(:,:,i);
end
Fsum = Fsum/length(allinds);


m = x8.Jy*x(8,allinds)';
x_lsq = pinv(H)*m;
x_sim = [x8.C_m_0;
         x8.C_m_alpha;
         x8.C_m_q;
         x8.C_m_delta_e];
     
figure
plot(t(allinds),m,'r')
hold on
plot(t(allinds),-H*x_sim*x8.Jy,'b')
legend('exp','sim')
%%
N2 = round(N/l)-1;
phi1 = iea(1,1);
r1 = iars(3,1);
l = .5/dt;
tl = t(1:l:N2*l);
clearvars mp mr sp sr
for i = 1:N2
    inds = i/dt:i/dt+l-1;
    t2 = t(inds);
    phi2 = iea(1,inds);
    r2 = iars(3,inds);
    
    mp(i) = mean(phi2);
    mr(i) = mean(r2);
    sp(i) = std(phi2);
    sr(i) = std(r2);
end

figure
plot(tl,mr)
hold on
plot(tl,sr,'r')

figure
plot(tl,mp)
hold on
plot(tl,sp,'r')

