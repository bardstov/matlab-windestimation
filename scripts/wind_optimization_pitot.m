load_exp_measurements

%%

figure;
subplotter( gps.vel.t, gps.vel.data,[])
subplotter(gps.vel2.t,gps.vel2.data,[])
%%

tv = gps.vel.t;
v =  gps.vel.data;

tp = dp.t;
p = dp.data;

if strcmp(XXX,'005')
    tstart = 325;
    tend = 2500;
elseif strcmp(XXX,'007')
    tstart = 460;
    tend = 2400;
elseif strcmp(XXX,'010')
    tstart = 550;
    tend = 1930;
end
vinds = find(tv>tstart & tv < tend);
tv = tv(vinds);
v = v(:,vinds);

p = interp1(tp,p,tv);

figure;plot(tp,dp.data)
hold on
plot([1,1]*tv(1),[0,150],'r')
plot([1,1]*tv(end),[0,150],'r')

figure;
subplotter(tv,v,[]);

x0 = [-1;1;0;1];
fun = @(x) lsqfun(x,p,v);
x = lsqnonlin(fun,x0);
x'
%%
function y = lsqfun(x,p,v)
    y = zeros(size(p));
    for i = 1:length(p)
        y(i) = (p(i)-.5*1.225/.72*norm(v(:,i)-x(1:3))^2)^2;
    end
end