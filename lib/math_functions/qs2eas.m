function eas = qs2eas(qs)
%QS2EAS Summary of this function goes here
%   Detailed explanation goes here
    eas = zeros(3,size(qs,2));
    for i = 1:size(qs,2)
        [a1,a2,a3] = q2euler(qs(:,i));
        eas(:,i) = [a1;a2;a3];
    end
end

