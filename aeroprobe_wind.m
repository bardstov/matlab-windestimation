




%%

figure
hold on
plot(ap.airspeed.t,ap.airspeed.data,'r')
plot(px.airspeed.t,px.airspeed.data,'b')

%%
figure
for i = 1:3
    subplot(3,1,i)
    hold on
    plot(ap.rvel.t,ap.rvel.data(i,:),'r')
    plot(px.rvel.t,px.rvel.data(i,:),'b')
end


%% interpoler ap.rvel til mekf_ea
N_ea = length(mekf_ea.t);
ap_inp.rvel.data = zeros(3,N_ea);
ap_inp.rvel.t = mekf_ea.t;
for i = 1:3
    ap_inp.rvel.data(i,:) = lowpass(interp1(ap.rvel.t,ap.rvel.data(i,:),ap_inp.rvel.t),1,50);
end

ap_inp.wvel.data = zeros(3,N_ea);
ap_inp.wvel.t = ap_inp.rvel.t;
ap_inp.rvel_ned.data = zeros(3,N_ea);
ap_inp.rvel_ned.t = ap_inp.rvel.t;
for i = 1:N_ea
    Rnb = Rzyx(mekf_ea.data(1,i),mekf_ea.data(2,i),mekf_ea.data(3,i));
    rvel_ned_i = Rnb*ap_inp.rvel.data(:,i);
    ap_inp.rvel_ned.data(:,i) = rvel_ned_i;
    wvel_ned_i = mekf_vel.data(:,i) - rvel_ned_i;
    ap_inp.wvel.data(:,i) = wvel_ned_i;
end

%%
opts = optimoptions('lsqnonlin');
opts.Display = 'Off';
N_win = 1;
L = floor(N_ea/N_win);

x0 = [0;0;0;1];
x = zeros(4,N_win);
tx = zeros(1,N_win);
tic
for i = 1:N_win
    c.ea = mekf_ea.data(:,(i-1)*L+1:L*i);
    c.vel = mekf_vel.data(:,(i-1)*L+1:L*i);
    c.rvel = ap_inp.rvel.data(:,(i-1)*L+1:L*i);
    x(:,i) = lsqnonlin( @(x) cal_fun(x,c), x0,[],[],opts);
    tx(i) = mekf_vel.t(i*L);
    disp(sprintf('Time %f',toc));
end

%correct ap_inp
ap_inp.rvel_corr.t = ap_inp.rvel.t;

Rba = Rzyx(0,mean(x(4,:)),0);
ap_inp.rvel_corr.data = Rba*ap_inp.rvel.data;
ap_inp.rvel_ned_corr.t = ap_inp.rvel_corr.t;
ap_inp.rvel_ned_corr.data = zeros(3,N_ea);
for i = 1:length(ap_inp.rvel_corr.t)
    Rnb = Rzyx(mekf_ea.data(1,i),mekf_ea.data(2,i),mekf_ea.data(3,i));
    ap_inp.rvel_ned_corr.data(:,i) = Rnb*ap_inp.rvel_corr.data(:,i);
end


%%
figure
for i = 1:3
    subplot(3,1,i)
    hold on
    plot(ap_inp.wvel.t,ap_inp.wvel.data(i,:),'r')
    plot(px.wvel.t,px.wvel.data(i,:),'b')
    if length(tx) == 1
        plot([px.wvel.t(1),px.wvel.t(end)],x(i,:)*[1,1],'g')
    else
        plot(tx,x(i,:),'g')
    end
    legend('Aeroprobe','PX','NLSQ')
end

figure
for i = 1:3
    subplot(3,1,i)
    hold on
    plot(mekf_vel.t,mekf_vel.data(i,:),'r')
    plot(ap_inp.rvel_ned.t,x(i)+ap_inp.rvel_ned.data(i,:),'b')
    plot(ap_inp.rvel_ned_corr.t,mean(x(i,:))+ap_inp.rvel_ned_corr.data(i,:),'g')
end