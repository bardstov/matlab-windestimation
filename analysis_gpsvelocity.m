


v1.t = gpsvel1.t(1:end-2);
v1.data = gpsvel1.data(:,1:end-2);
N = length(v1.t);

v2.t = gpsvel2.t;
v2.data = gpsvel2.data;

ars_inp.t = v1.t;
ars_inp.data = zeros(3,N);
for i = 1:3
    ars_inp.data(i,:) = interp1(ars.t,ars.data(i,:),ars_inp.t);
end


ea_inp.t = v1.t;
ea_inp.data = zeros(3,N);
for i = 1:3
    ea_inp.data(i,:) = interp1(px.ea.t,px.ea.data(i,:),ea_inp.t);
end

S1 = Smtrx(p_arm1);
S2 = Smtrx(p_arm2);

for i = 1:N
    Rnb = Rzyx(ea_inp.data(1,i),ea_inp.data(2,i),ea_inp.data(3,i));
    v1_c.data(:,i) = v1.data(:,i) - Rnb*S1*ars_inp.data(:,i);
    v2_c.data(:,i) = v2.data(:,i) - Rnb*S2*ars_inp.data(:,i);
end
v1_c.t = v1.t;
v2_c.t = v2.t;


figure
for i = 1:3
    subplot(3,1,i)
    hold on
    plot(v1.t,v1_c.data(i,:)-v1.data(i,:),'r')
    plot(v2.t,v2_c.data(i,:)-v2.data(i,:),'b')
end

figure
for i = 1:3
    subplot(3,1,i)
    hold on
    plot(v1_c.t,v1_c.data(i,:),'r')
    plot(v2_c.t,v2_c.data(i,:),'b')
    plot(v1.t,v1.data(i,:),'m')
    plot(v2.t,v2.data(i,:),'g')
end